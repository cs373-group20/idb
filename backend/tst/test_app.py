import sys
import json

sys.path.append('../src')

from app import app

# Test the homepage
def test_index():
    response = app.test_client().get("/")
    assert response.status_code == 200

    data = json.loads(response.data.decode('utf-8'))
    assert data == "API_HOME"

# Test the facility call with 541
def test_facility():
    response = app.test_client().get("/facility?name=541")
    assert response.status_code == 200

    data = json.loads(response.data.decode('utf-8'))

    assert data["name"] == "541"
    assert data["facility_status"] == "Active"
    assert data["status_note"] == None
    assert data["cad_name"] == "HIG 541"
    assert data["aka"] == "Highland"
    assert data["type"] == "FSA"
    assert data["zip_code"] == "92346"
    assert data["phone_num"] == "909-862-3031"
    assert str(data["lat"]) == "34.12142498"
    assert str(data["lon"]) == "-117.21607808"
    assert len(data["nearby_fires"]) > 0
    assert len(data["nearby_counties"]) > 0

# Test the wildfire call with pilot
def test_fire():
    response = app.test_client().get("/wildfire?name=pilot")
    assert response.status_code == 200

    data = json.loads(response.data.decode('utf-8'))

    assert data["name"] == "Pilot"
    assert data["city"] == "Willow Creek"
    assert data["county"] == "Humboldt"
    assert data["dispatch_center"] == "CANCIC"
    assert data["primary_fuel"] == "Timber (Grass and Understory)"
    assert data["secondary_fuel"] == "Brush (2 feet)"
    assert data["total_personnel"] == "98"
    assert data["identifier"] == "2023-CASRF-000932"
    assert data["final_cost"] == None
    assert data["severity"] == "null"
    assert data["percent_to_be_contained"] == 100
    assert data["containment_date"] == None
    assert data["size"] == 1055
    assert data["discovery_size"] == 35
    assert data["cost_to_date"] == 816081
    assert data["final_acres"] == None
    assert data["behavior0"] == "Moderate"
    assert data["behavior1"] == "Flanking"
    assert data["behavior2"] == "Backing"
    assert data["behavior3"] == "Torching"
    assert data["cause"] == "Undetermined"
    assert data["discover_date"] == 1692086640000
    assert data["category"] == "WF"
    assert data["type"] == "FI"
    assert data["response_date"] == None
    assert data["percent_contained"] == 100
    assert str(data["lat"]) == "40.6953"
    assert str(data["lon"]) == "-123.695"
    assert len(data["nearby_counties"]) > 0
    assert len(data["nearby_facilities"]) > 0

# Test the county call with alameda
def test_county():
    response = app.test_client().get("/county?name=alameda")
    assert response.status_code == 200

    data = json.loads(response.data.decode('utf-8'))
    
    assert data["name"] == "Alameda"
    assert data["pop"] == 1671329
    assert str(data["pop_density"]) == "2266.8038359"
    assert data["region"] == "Coastal"
    assert data["fireMAR"] == 2
    assert data["area"] == 821
    assert str(data["lat"]) == "37.6504930265"
    assert str(data["lon"]) == "-121.917998966"
    assert len(data["nearby_fires"]) > 0
    assert len(data["nearby_facilities"]) > 0

# Test the allFacilities call
def test_all_facilities():
    response = app.test_client().get("/allFacilities")
    assert response.status_code == 200

    data = json.loads(response.data.decode('utf-8'))

    facility_attrs = ["name", "facility_status", "status_note", "cad_name", "aka", 
                      "type", "zip_code", "phone_num", "lat", "lon"]
    for attr in facility_attrs:
        assert attr in data[0]
    assert len(data) == 1498

# Test the allWildfires call
def test_all_wildfires():
    response = app.test_client().get("/allWildfires")
    assert response.status_code == 200

    data = json.loads(response.data.decode('utf-8'))

    wildfire_attrs = ["name", "city", "county", "dispatch_center", "primary_fuel", "secondary_fuel", "total_personnel",
                    "identifier", "final_cost", "severity", "percent_to_be_contained", "containment_date",
                    "size", "discovery_size", "cost_to_date", "final_acres", "behavior0", "behavior1", "behavior2",
                    "behavior3", "cause", "discover_date", "category", "type", "response_date", "percent_contained",
                    "lat", "lon"]
    for attr in wildfire_attrs:
        assert attr in data[0]
    assert len(data) == 125

# Test the allCounties call
def test_all_counties():
    response = app.test_client().get("/allCounties")
    assert response.status_code == 200

    data = json.loads(response.data.decode('utf-8'))

    county_attrs = ["name", "pop", "pop_density", "region", "fireMAR", "area", "lat", "lon"]
    for attr in county_attrs:
        assert attr in data[0]
    assert len(data) == 58

# Test the recentWildfire call
def test_recentWildfire():
    response = app.test_client().get("/recentWildfire")
    assert response.status_code == 200
    
    data = json.loads(response.data.decode('utf-8'))

    assert data[0]["name"] == "Horse"

# Test the allFacilities call for sorting
def test_sorting_facilities():
    response = app.test_client().get("/allFacilities?order=name")
    assert response.status_code == 200
    data = json.loads(response.data.decode('utf-8'))
    assert data[0]["name"] == "541"
    assert data[10]["name"] == "Airport FS"
    assert data[50]["name"] == "Baker FS"
    assert data[100]["name"] == "Big Bend FS"
    assert data[200]["name"] == "City Walk/Universal City"
    assert data[300]["name"] == "Elk Creek FS"
    assert data[1400]["name"] == "USAR Warehouse"
    assert len(data) == 1498

# Test the allWildfires call for sorting
def test_sorting_wildfires():
    response = app.test_client().get("/allWildfires?order=name")
    assert response.status_code == 200
    data = json.loads(response.data.decode('utf-8'))
    assert data[0]["name"] == "2023 Srf Lightning Complex"
    assert data[10]["name"] == "Burlington 3 B Rx"
    assert data[50]["name"] == "Lac-353801"
    assert data[70]["name"] == "Lac-356637"
    assert data[100]["name"] == "Rx Johnson Vmp"
    assert data[124]["name"] == "Wolverine"
    assert len(data) == 125

# Test the allCounties call for sorting
def test_sorting_counties():
    response = app.test_client().get("/allCounties?order=name")
    assert response.status_code == 200
    data = json.loads(response.data.decode('utf-8'))
    assert data[0]["name"] == "Alameda"
    assert data[5]["name"] == "Colusa"
    assert data[10]["name"] == "Glenn"
    assert data[20]["name"] == "Marin"
    assert data[30]["name"] == "Placer"
    assert data[40]["name"] == "San Mateo"
    assert data[50]["name"] == "Sutter"
    assert data[57]["name"] == "Yuba"
    assert len(data) == 58

# Test the allFacilities call for filtering
def test_filtering_facilities():
    response = app.test_client().get("/allFacilities?county=Alameda")
    assert response.status_code == 200
    data = json.loads(response.data.decode('utf-8'))
    assert data[0]["name"] == "Fremont FLS"
    assert data[1]["name"] == "Fremont PL"
    assert data[2]["name"] == "Mt. Allison Repeater"
    assert data[3]["name"] == "Sunol FS"
    assert len(data) == 4

# Test the allWildfires call for filtering
def test_filtering_wildfires():
    response = app.test_client().get("/allWildfires?county=Del Norte")
    assert response.status_code == 200
    data = json.loads(response.data.decode('utf-8'))
    assert data[0]["name"] == "Blue Creek"
    assert data[5]["name"] == "Corral"
    assert data[10]["name"] == "Island"
    assert data[15]["name"] == "Smith River Complex"
    assert data[16]["name"] == "Wimer"
    assert len(data) == 17

# Test the allCounties call for filtering
def test_filtering_counties():
    response = app.test_client().get("/allCounties?region=Coastal")
    assert response.status_code == 200
    data = json.loads(response.data.decode('utf-8'))
    assert data[0]["name"] == "Alameda"
    assert data[1]["name"] == "Contra Costa"
    assert data[5]["name"] == "Marin"
    assert data[10]["name"] == "San Francisco"
    assert data[15]["name"] == "Sonoma"
    assert len(data) == 16
