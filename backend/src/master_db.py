import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import create_database, database_exists
from sqlalchemy.orm import sessionmaker

# A class inherited by all "db" classes and provides functions that simply need to access the aws server
class MasterDB():
    def __init__(self):
        self.url = "mysql://admin:adminpassword@californiawildfiredb.crnzrax60aum.us-east-2.rds.amazonaws.com:3306/californiawildfiredb"
        self.metadata = db.MetaData()
        self.base = declarative_base(metadata=self.metadata)
        self.table_name = ""
        self.string_args = []
        self.int_args = []
        self.order_args = []
        self.cache = []
        self.DEGREE_DIFF = 0

        # Create the database if it doesn't already exist
        self.engine = db.create_engine(self.url)
        if not database_exists(self.engine.url):
            create_database(self.engine.url)
            print("Creating a New Database")

    # Get instances of the given class nearby the coordinates provided
    def nearby(self, lat, lon):
        max_lat = lat + self.DEGREE_DIFF
        min_lat = lat - self.DEGREE_DIFF

        max_lon = lon + self.DEGREE_DIFF
        min_lon = lon - self.DEGREE_DIFF

        query_string = "SELECT * FROM %s WHERE lat < %f AND lat > %f AND lon < %f AND lon >  %f" % (self.table_name, max_lat, min_lat, max_lon, min_lon)
        with self.engine.connect() as connection:
            result = connection.execute(db.text(query_string))
        return self.data_as_dict(result)

    # Deletes the table
    def delete_table(self):
        table = self.metadata.tables.get(self.table_name)
        self.base.metadata.drop_all(self.engine, [table], checkfirst=True)
    
    # Uploads all rows within the cache
    def upload_data(self):
        # Ensure that this table_name has been set
        assert self.table_name != ""

        # Connect with the SQL server
        with self.engine.connect() as connection:
            
            # See if the table exists in the current Database
            if db.inspect(self.engine).has_table(self.table_name):
                print("Table Already Exists")
            else:
                # Create the tables in our sql base obj
                self.base.metadata.create_all(self.engine)

            # Start a session and upload the data
            session_obj = sessionmaker(bind=self.engine)
            session = session_obj(bind = connection)
            session.bulk_save_objects(self.cache)
            session.commit()
    
    # Read a subset from the table 
    def read_subset(self, attr, value):
        with self.engine.connect() as connection:
            # When looking for string values, we have to add single quotes (') on either side
            val_as_str = "'" + value + "'"
            query_string = "SELECT * FROM %s WHERE %s = %s" % (self.table_name, attr, val_as_str)
            result = connection.execute(db.text(query_string))
            return self.data_as_dict(result)

    # Read all entries from the table
    def read_all(self, order, string_data = {}, int_data = {}):
        with self.engine.connect() as connection:
            query_string = "SELECT * FROM %s" % self.table_name

            # Add general filtering
            if len(int_data) > 0 or len(string_data) > 0:
                query_string += " WHERE "

            # Add string comparison
            i = 0
            for attr, val in string_data.items():
                query_string += '{attr} =  "{val}"'.format(attr=attr,val=val)
                if i != len(string_data)-1 or len(int_data) != 0:
                    query_string += " AND "
                i += 1

            # Add value comparison
            i = 0
            for attr, val in int_data:
                cmp = val[0]
                query_string += '{attr} {cmp} {val}'.format(attr=attr, cmp = cmp, val=val[1:])
                if i != len(int_data)-1:
                    query_string += " AND "
                i += 1
            
            # Add sorting
            if order:
                query_string += " ORDER BY " + order[1]
                query_string += " DESC" if order[0] == "orderd" else ""
            
            # Do the query
            print(query_string)
            result = connection.execute(db.text(query_string))
            return self.data_as_dict(result)
        
    # Format the response to be in json
    def data_as_dict(self, query_result):
        output_list = []
        for row in query_result:
            output_list.append(dict(row._mapping))
        return output_list
        
        
    