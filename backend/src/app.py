from flask import Flask, make_response, request
from flask_cors import CORS
from flask_restful import Resource, Api
import re
import time

from tables import fire_table, facility_table, county_table

app = Flask(__name__)
api = Api(app)
CORS(app)

# Database objects used for reading data
facility_db = facility_table.FacilityDB()
fire_db = fire_table.FireDB()
county_db = county_table.CountyDB()

# Data sent on home page
class Home(Resource): 
    def get(self):
        # There is currently no data we are sending the home page
        return "API_HOME"

# Data sent on "facility" call
class Facility(Resource):
    def get(self):
        # Provide data for an individual facility in JSON format
        try:
            name = request.args.get("name").strip('"')
            attr = "name"
            response_dict = facility_db.read_subset(attr, name)[0]
            response_dict["nearby_fires"] = fire_db.nearby(response_dict["lat"], response_dict["lon"])
            adjust_date(response_dict["nearby_fires"])
            response_dict["nearby_counties"] = county_db.nearby(response_dict["lat"], response_dict["lon"])
            return response_dict
        except:
            return "Bad Request"

# Data sent on "wildfire" call
class Wildfire(Resource):
    def get(self):
        # Provide data for an individual facility in JSON format
        try: 
            name = request.args.get("name").strip('"')
            attr = "name"
            response_dict = fire_db.read_subset(attr, name)[0]
            response_dict["nearby_counties"] = county_db.nearby(response_dict["lat"], response_dict["lon"])
            response_dict["nearby_facilities"] = facility_db.nearby(response_dict["lat"], response_dict["lon"])
            adjust_date(response_dict)
            return response_dict
        except:
            return "Bad Request"


# Data sent on "county" call
class County(Resource):
    def get(self):
        # Provide data for an individual facility in JSON format
        try:
            name = request.args.get("name").strip('"')
            attr = "name"
            response_dict = county_db.read_subset(attr, name)[0]
            response_dict["nearby_fires"] = fire_db.nearby(response_dict["lat"], response_dict["lon"])
            adjust_date(response_dict["nearby_fires"])
            response_dict["nearby_facilities"] = facility_db.nearby(response_dict["lat"], response_dict["lon"])
            return response_dict
        except:
            return "Bad Request"


# Data sent on "allFacilities" call
class AllFacilities(Resource):
    def get(self):
        return query_all(facility_db, request.args)

# Data sent on "allWildfires" call
class AllWildfires(Resource):
    def get(self):
        query = query_all(fire_db, request.args)
        adjust_date(query)
        return query

# Data sent on "allCounties" call
class AllCounties(Resource):
    def get(self):
        return query_all(county_db, request.args)

# Data sent on "recentWildfire call"
class RecentWildfire(Resource):
    def get(self):
        # Provide data for the most recent wildfire
        out_dict = fire_db.most_recent()
        adjust_date(out_dict)
        return out_dict

# all* query setup, implements filtering and sorting
def query_all(db, args):
    int_data = []
    string_data = {}
    order = None
    for param, val in args.items():
        # Order by assignment
        if param in ["order", "orderd"] and val in db.order_args:
            if order:
                return "Bad Request, multiple order attributes provided"
            order = (param, val)
        
        # String attribute filter
        elif param in db.string_args:
            string_data[param] = val 
        
        # Integer attribute filter
        elif param in db.int_args:
            if val[0] not in [">","<", "="]:
                return "Bad Request, bad comparison operator, given: {val}".format(val=val[0])
            
            # Allow for multiple comparisons
            ints_given = re.split(">|<|=",val)[1:]
            cmp_given = re.findall(">|<|=",val)
            for i, int_given in enumerate(ints_given):
                try:
                    int(int_given)
                except:
                    return "Bad Request, non-int given for int comparison: {val}".format(val=int_given[1:])
                int_data.append((param,cmp_given[i] + int_given))
        
        # Bad request
        else:
            return "Bad Request, unknown arg '{param}'".format(param=param)

    return db.read_all(order, string_data, int_data)

# Adjust the int-given date to year/month/day/hour/minute
def adjust_date(out_con):
    if isinstance(out_con,dict):
        raw_time = out_con["discover_date"] // 1000
        out_con["discover_date"] = time.ctime(raw_time)
    elif isinstance(out_con,list):
        for out_dict in out_con:
            raw_time = out_dict["discover_date"] // 1000
            out_dict["discover_date"] = time.ctime(raw_time)


api.add_resource(Home, '/')
api.add_resource(Facility, '/facility')
api.add_resource(County, '/county')
api.add_resource(Wildfire, '/wildfire')

api.add_resource(AllFacilities,'/allFacilities')
api.add_resource(AllCounties,'/allCounties')
api.add_resource(AllWildfires,'/allWildfires')

api.add_resource(RecentWildfire,'/recentWildfire')


# When running locally, run on port 5000 with debug
if __name__ == "__main__":
    app.run(port=5000, debug=True)

    

