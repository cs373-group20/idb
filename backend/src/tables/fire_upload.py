import fire_table as fire_helper
import json

# URL for the database
url = "mysql://admin:adminpassword@californiawildfiredb.crnzrax60aum.us-east-2.rds.amazonaws.com:3306/californiawildfiredb"

fire_table = fire_helper.FireDB()

# Parse the Json File specifically for the Facility Dataset
def parseJson(file, attr_list):
    data = json.load(file)
    names_seen = set()
    all_instances = []
    for fire in data["features"]:
        fire_instance = {}
        attributes = fire["attributes"]
        if attributes["POOState"] != "US-CA":
            continue

        # Set up the name, if it is identical, make it unique by adding a number
        name = attributes["IncidentName"].strip().title()
        count = 0
        while name in names_seen:
            if count == 0:
                name += " 1"
            else:
                name = name[:-1] + str(count+1)
            count += 1
        fire_instance["IncidentName"] = name
        names_seen.add(name)

        # If this is an attribute we care about, then save it
        for attr in attr_list:
            if attr not in attributes:
                fire_instance[attr] = "null"
            else:
                fire_instance[attr] = attributes[attr]
        all_instances.append(fire_instance)
    return all_instances

# Upload parsed data to the SQL database
def upload():
    file = open("../raw_datasets/individualFires.json", encoding="utf8")
    
    # These match the attributes we have in postman
    attr_list = ["POOCity",
                "POOCounty",
                "POODispatchCenterID",
                "PrimaryFuelModel",
                "SecondaryFuelModel",
                "TotalIncidentPersonnel",
                "UniqueFireIdentifier",
                "EstimatedFinalCost",
                "OrganizationalAssessment",
                "PercentPerimeterToBeContained",
                "ContainmentDateTime",
                "IncidentSize",
                "DiscoveryAcres",
                "EstimatedCostToDate",
                "FinalAcres",
                "FireBehaviorGeneral",
                "FireBehaviorGeneral1",
                "FireBehaviorGeneral2",
                "FireBehaviorGeneral3",
                "FireCause",
                "FireDiscoveryDateTime",
                "IncidentTypeCategory",
                "IncidentTypeKind",
                "InitialLatitude",
                "InitialLongitude",
                "InitialResponseDateTime",
                "PercentContained"]

    all_fires = parseJson(file, attr_list)
    for fire in all_fires:
        fire_table.cache_row(fire)
    fire_table.upload_data()

# Delete the table
def delete_table():
    fire_table.delete_table()

# Print a small summary of the items in the table
def print_table():
    fire_table.print_data()
