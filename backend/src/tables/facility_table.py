import sys

sys.path.append('..')

import tables.facility_urls
import master_db 
import sqlalchemy as db

class FacilityDB(master_db.MasterDB):
    
    # This is a peculiar function, here's why it's organized like this:
    # self.base is an instance variable which the table definition has to inherit
    # However, simply creating a class within FacilityDB does not allow for the nested 
    # class to inherit an instance variable, so this function creates the required class
    # and then returns the class itself to be used later. 
    def create_facility(self):
        class Facility(self.base):
            __tablename__ =  "Facility"

            # Table Attributes
            name = db.Column(db.String(80), primary_key=True)
            facility_status = db.Column(db.String(80))
            status_note = db.Column(db.String(80))
            cad_name = db.Column(db.String(80))
            aka = db.Column(db.String(80))
            type = db.Column(db.String(80))
            unit = db.Column(db.String(80))
            cdf_unit = db.Column(db.String(80))
            county = db.Column(db.String(80))
            owner = db.Column(db.String(80))
            funding = db.Column(db.String(80))
            staffing = db.Column(db.String(80))
            address = db.Column(db.String(80))
            city = db.Column(db.String(80))
            zip_code = db.Column(db.String(80))
            phone_num = db.Column(db.String(80))
            lat = db.Column(db.Double)
            lon = db.Column(db.Double)
            img_url = db.Column(db.String(150))
        return Facility

    def __init__(self):
        super().__init__()
        self.table_name = "Facility"
        self.Facility = self.create_facility()
        self.string_args = ["county", "city", "facility_status", "owner", "type"]
        self.int_args = []
        self.order_args = ["name"]

        # We will search for facilities within 25 miles of the coordinates given
        self.DEGREE_DIFF = 0.3623
    
    # Batch uploading is faster, so we cache the rows we want to append until later
    def cache_row(self, data):
        row = self.Facility(
            name = data["NAME"],
            facility_status = data["FACILITY_STATUS"],
            status_note = data["STATUS_NOTE"],
            cad_name = data["CAD_NAME"],
            aka = data["AKA"],
            type = data["TYPE"],
            unit = data["UNIT"],
            cdf_unit = data["CDF_UNIT"],
            county = data["COUNTY"],
            owner = data["OWNER"],
            funding = data["FUNDING"],
            staffing = data["STAFFING"],
            address = data["ADDRESS"],
            city = data["CITY"],
            zip_code = data["ZIP"],
            phone_num = data["PHONE_NUM"],
            lat = data["LAT"],
            lon = data["LON"],
            img_url = tables.facility_urls.ret_img())
        self.cache.append(row)

    # Simply prints a sample of the data in the facility database
    def print_data(self):
        with self.engine.connect() as connection:
            result = connection.execute(db.text("SELECT * FROM " + self.table_name))
            print("\nRows of %s: " % self.table_name)
            for row in result:
                print("[name: %s, facility_status: %s]" % (row.name, row.facility_status))
        
        
    

