import sys

sys.path.append('..')

import master_db
import sqlalchemy as db
import tables.fire_img_urls
import tables.fire_urls

class FireDB(master_db.MasterDB):
    
    # This is a peculiar function, here's why it's organized like this:
    # self.base is an instance variable which the table definition has to inherit
    # However, simply creating a class within FireDB does not allow for the nested 
    # class to inherit an instance variable, so this function creates the required class
    # and then returns the class itself to be used later. 
    def create_fire(self):
        class Fire(self.base):
            __tablename__ =  "Fire"

            # Table Attributes
            name = db.Column(db.String(80), primary_key=True)
            city = db.Column(db.String(80))
            county = db.Column(db.String(80))
            dispatch_center = db.Column(db.String(80))
            primary_fuel = db.Column(db.String(80))
            secondary_fuel = db.Column(db.String(80))
            total_personnel = db.Column(db.String(80))
            identifier = db.Column(db.String(80))
            final_cost = db.Column(db.Integer)
            severity = db.Column(db.String(80))
            percent_to_be_contained = db.Column(db.Integer)
            containment_date = db.Column(db.String(80))
            size = db.Column(db.Integer)
            discovery_size = db.Column(db.Double)
            cost_to_date = db.Column(db.Integer)
            final_acres = db.Column(db.Double)
            behavior0 = db.Column(db.String(80))
            behavior1 = db.Column(db.String(80))
            behavior2 = db.Column(db.String(80))
            behavior3 = db.Column(db.String(80))
            cause = db.Column(db.String(80))
            discover_date = db.Column(db.BigInteger)
            category = db.Column(db.String(80))
            type = db.Column(db.String(80))
            response_date = db.Column(db.String(80))
            percent_contained = db.Column(db.Integer)
            lat = db.Column(db.Double)
            lon = db.Column(db.Double)
            link = db.Column(db.String(100))
            img_url = db.Column(db.String(400))
        return Fire

    def __init__(self):
        super().__init__()
        self.table_name = "Fire"
        self.Fire = self.create_fire()
        self.string_args = ["city", "county", "type"]
        self.int_args = ["cost_to_date", "size"]
        self.order_args = ["name", "cost_to_date", "discover_date"]

        # We will search for fires within 100 miles of the coordinates given
        self.DEGREE_DIFF = 1.449
    
    # Batch uploading is faster, so we cache the rows we want to append until later
    def cache_row(self, data):
        row = self.Fire(
            city = data["POOCity"],
            county = data["POOCounty"],
            dispatch_center = data["POODispatchCenterID"],
            primary_fuel = data["PrimaryFuelModel"],
            secondary_fuel = data["SecondaryFuelModel"],
            total_personnel = data["TotalIncidentPersonnel"],
            identifier = data["UniqueFireIdentifier"],
            final_cost = data["EstimatedFinalCost"],
            severity = data["OrganizationalAssessment"],
            percent_to_be_contained = data["PercentPerimeterToBeContained"],
            containment_date = data["ContainmentDateTime"],
            size = data["IncidentSize"],
            discovery_size = data["DiscoveryAcres"],
            cost_to_date = data["EstimatedCostToDate"],
            final_acres = data["FinalAcres"],
            behavior0 = data["FireBehaviorGeneral"],
            behavior1 = data["FireBehaviorGeneral1"],
            behavior2 = data["FireBehaviorGeneral2"],
            behavior3 = data["FireBehaviorGeneral3"],
            cause = data["FireCause"],
            discover_date = data["FireDiscoveryDateTime"],
            name = data["IncidentName"],
            category = data["IncidentTypeCategory"],
            type = data["IncidentTypeKind"],
            response_date = data["InitialResponseDateTime"],
            percent_contained = data["PercentContained"],
            lat = data["InitialLatitude"],
            lon = data["InitialLongitude"],
            link = tables.fire_urls.ret_urls(data["IncidentName"]),
            img_url = tables.fire_img_urls.ret_img())
        self.cache.append(row)
    
    def most_recent(self):
        with self.engine.connect() as connection:
            max_date_query = connection.execute(db.text("SELECT MAX(discover_date) as max_date FROM "+ self.table_name))
            max_date = 0
            for row in max_date_query:
                max_date = row.max_date

            query_string = "SELECT * FROM %s WHERE discover_date = %i" % (self.table_name, max_date)
            result = connection.execute(db.text(query_string))
            return self.data_as_dict(result)

    # Simply prints a sample of the data in the fire database
    def print_data(self):
        with self.engine.connect() as connection:
            result = connection.execute(db.text("SELECT * FROM " + self.table_name))
            print("\nRows of %s: " % self.table_name)
            for row in result:
                print("[name: %s, county: %s]" % (row.name, row.county))
    

