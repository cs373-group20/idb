import county_table as county_helper
import json

MILE_TO_METER_CONVERSION = 2589988.110

county_table = county_helper.CountyDB()

# Parse the Json File specifically for the County Dataset
def parseJson(pop_file, area_file, region_file):
    counties = {}

    # County population data
    data = json.load(pop_file)
    for county_pop in data[1:]:
        name = county_pop[0].split(" County")[0]
        counties[name] = {"pop" : county_pop[1], "pop_density" : county_pop[2]}
    
    # County land area data
    data = json.load(area_file)
    for county_area in data["results"]:
        name = county_area["name"]
        counties[name]["lon"] = county_area["geo_point_2d"]["lon"]
        counties[name]["lat"] = county_area["geo_point_2d"]["lat"]

        meter_area = county_area["aland"] + county_area["awater"]
        mile_area = round(meter_area / MILE_TO_METER_CONVERSION)
        counties[name]["area"] = mile_area
    
    # Region and fireMAR count
    data = json.load(region_file)
    for county_region in data["features"]:
        attributes = county_region["attributes"]
        name = attributes["CountyName"]
        counties[name]["region"] = attributes["AdminRegion"]
        counties[name]["fireMAR"] = attributes["FireMAR"]

    return counties

# Upload parsed data to the SQL database
def upload():
    pop_file = open("../raw_datasets/countyPop.json", encoding="utf8")
    area_file = open("../raw_datasets/countyLandArea.json", encoding="utf8")
    region_file = open("../raw_datasets/countyFMA.json", encoding="utf8")

    all_counties = parseJson(pop_file, area_file, region_file)
    for county in all_counties:
        county_table.cache_row(county, all_counties[county])
    county_table.upload_data()

# Delete the table
def delete_table():
    county_table.delete_table()

# Print a small summary of the items in the table
def print_table():
    county_table.print_data()
