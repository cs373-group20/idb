import sys

sys.path.append('..')

import tables.county_img_urls
import master_db 
import sqlalchemy as db

class CountyDB(master_db.MasterDB):
    
    # This is a peculiar function, here's why it's organized like this:
    # self.base is an instance variable which the table definition has to inherit
    # However, simply creating a class within CountyDB does not allow for the nested 
    # class to inherit an instance variable, so this function creates the required class
    # and then returns the class itself to be used later. 
    def create_county(self):
        class County(self.base):
            __tablename__ =  self.table_name

            # Table Attributes
            name = db.Column(db.String(80), primary_key=True)
            pop = db.Column(db.Integer)
            pop_density = db.Column(db.Double)
            region = db.Column(db.String(80))
            fireMAR = db.Column(db.Integer)
            area = db.Column(db.Integer)
            lat = db.Column(db.Double)
            lon = db.Column(db.Double)
            link = db.Column(db.String(100))
            img_url = db.Column(db.String(250))
        return County

    def __init__(self):
        super().__init__()
        self.table_name = "County"
        self.County = self.create_county()
        self.string_args = ["region", "fireMAR"]
        self.int_args = ["pop", "pop_density", "area"]
        self.order_args = ["name", "pop", "pop_density", "area"]

        # We will search for counties within 100 miles of the coordinates given
        self.DEGREE_DIFF = 1.449
    
    # Batch uploading is faster, so we cache the rows we want to append until later
    def cache_row(self, name, data):
        row = self.County(
            name = name,
            pop = data["pop"],
            pop_density = data["pop_density"],
            region = data["region"],
            fireMAR = data["fireMAR"],
            lon = data["lon"],
            lat = data["lat"],
            area = data["area"],
            link = 'https://wikipedia.org/wiki/' + name.replace(" ", "_") + '_County' + ',_California',
            img_url = tables.county_img_urls.ret_img(name))
        self.cache.append(row)

    # Simply prints a sample of the data in the county database
    def print_data(self):
        with self.engine.connect() as connection:
            result = connection.execute(db.text("SELECT * FROM " + self.table_name))
            print("\nRows of %s: " % self.table_name)
            for row in result:
                print("[name: %s, pop: %s]" % (row.name, row.pop))