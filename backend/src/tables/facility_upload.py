import facility_table as facility_helper
import json

# URL for the database
url = "mysql://admin:adminpassword@californiawildfiredb.crnzrax60aum.us-east-2.rds.amazonaws.com:3306/californiawildfiredb"

facility_table = facility_helper.FacilityDB()

# Parse the Json File specifically for the Facility Dataset
def parseJson(file, attr_list):
    data = json.load(file)
    all_instances = []
    names_seen = set()
    for facility in data["features"]:
        fac_instance = {}
        attributes = facility["attributes"]
        
        # Set up the name, if it is identical, make it unique by adding a number
        name = attributes["NAME"].strip()
        count = 0
        while name in names_seen:
            if count == 0:
                name += " 1"
            else:
                name[-1] = str(count+1)
            count += 1
        fac_instance["NAME"] = name
        names_seen.add(name)

        # If this is an attribute we care about, then save it
        for attr in attr_list:
            fac_instance[attr] = attributes[attr]
        all_instances.append(fac_instance)
    return all_instances

# Upload parsed data to the SQL database
def upload():
    file = open("../raw_datasets/facilities.json", encoding="utf8")
    
    # These match the attributes we have in postman
    attr_list = ["FACILITY_STATUS", 
                 "STATUS_NOTE", 
                 "CAD_NAME",
                 "AKA", 
                 "TYPE",
                 "UNIT", 
                 "CDF_UNIT", 
                 "COUNTY", 
                 "OWNER",
                 "FUNDING",
                 "STAFFING",
                 "ADDRESS",
                 "CITY",
                 "ZIP",
                 "PHONE_NUM",
                 "LAT",
                 "LON"]

    all_facilities = parseJson(file, attr_list)
    for facility in all_facilities:
        facility_table.cache_row(facility)
    facility_table.upload_data()

# Delete the table
def delete_table():
    facility_table.delete_table()

# Print a small summary of the items in the table
def print_table():
    facility_table.print_data()
