# California Wildfires (Group 20)

### [CaliforniaWildfires.me](https://www.californiawildfires.me/)

# Introduction
California is known for its catastrophic wildfires every year. Being the most populous state in the US, these fires affect a large number of citizens.

Our website strives to bring attention to the fires in California and to provide information on wildland fire protection facilities in the region for those affected by these fires.

#### **CaliforniaWildfires.me** answers questions like:

- *What are the recent / ongoing fires in California?*
- *What facilities are near these fires and how do I contact them?*
- *What counties have more fires and what facilities are near to support them?*

## Team Members

| Name                    | EIDs    | GitLabID         |
|:----------------------- |:------- | :--------------- |
| Hunter Samra            | hgs567  | @hunter.samra981 |
| Daehyun Kyoung          | dk27359 | @danielkyoung    |
| Abdulrahman Alshahrani  | ama8347 | @Abdomash        |
| Imad Hussein            | ifh223  | @imad.hussein    |
| Jayden Chakranarayan    | jvc684  | @jvchak          |

### **Project Leader:** ***Abdulrahman Alshahrani***
**Responsibilites:**
- Dividing the work between the team members.
- Making sure everyone is on track and making progress.


## Data Sources

- #### [OpenDataSoft](https://public.opendatasoft.com/explore/dataset/us-county-boundaries/?flg=en-us)

- #### [The Census Bureau](https://www.census.gov/data/developers/data-sets/popest-popproj/popest.Vintage_2019.html#list-tab-2014455046)

- #### [ArcGIS](https://gis-calema.opendata.arcgis.com/datasets/CalEMA::california-county-boundaries/api) (California counties boundaries)

- #### [ArcGIS](https://data-nifc.opendata.arcgis.com/datasets/nifc::current-wildland-fire-incident-locations/explore?location=27.023722%2C-98.483843%2C4.94) (Wildfires locations)

- #### [California Natural Resources](https://gis.data.cnra.ca.gov/datasets/CALFIRE-Forestry::cal-fire-facilities-for-wildland-fire-protection/api)

*Data was scrapped programmatically via their RESTful API.*

## API Information

- #### [API Link](http://api.californiawildfires.me/)
- #### [API Documentation](https://documenter.getpostman.com/view/29941436/2s9YJZ34Ee)

## GitLab Repo Information

#### [**Gitlab Pipeline**](https://gitlab.com/cs373-group20/idb/-/pipelines)

#### **Git SHA:** 86d44cab36e2afe23405c44891a52cc51478a83d

#### **Comments:** None

## Completion Time

Below is the estimated completion time for the project versus the actual time it took in hours for each team member.

| Name                    | Estimated time | Actual time |
| :---------------------- | :------------: | :---------: |
| Hunter Samra            | 20 hrs         | 24 hrs      |
| Daehyun Kyoung          | 15 hrs         | 15 hrs      |
| Abdulrahman Alshahrani  | 18 hrs         | 22 hrs      |
| Imad Hussein            | 15 hrs         | 20 hrs      |
| Jayden Chakranarayan    | 15 hrs         | 20 hrs      |
