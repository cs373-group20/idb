import { render, screen } from '@testing-library/react'
import CountyModelPage from '../pages/CountyModelPage'
import countyData from '../data/countyData.json'

const mockUsedNavigate = jest.fn();
function mockLoader(){
    return countyData
}
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
 useNavigate: () => mockUsedNavigate,
 useLoaderData: mockLoader,
}));

test("CountyModelPage renders", ()=>{
  render(<CountyModelPage/>)
  expect(screen.getByText(/All the Counties/i)).toBeInTheDocument();
  expect(screen.getAllByText(/Coastal/i)[0]).toBeInTheDocument();
  expect(screen.getByText(/135558/i)).toBeInTheDocument();
})