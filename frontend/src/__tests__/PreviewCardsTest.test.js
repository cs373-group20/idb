import { render, screen } from '@testing-library/react'
import WildfirePreviewBox from '../cards/WildfirePreviewBox'
import CountyPreviewBox from '../cards/CountyPreviewBox'
import FPAPreviewBox from '../cards/FPAPreviewBox'
const mockUsedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
   ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockUsedNavigate,
}));

test("WildfirePreviewBox renders", ()=>{
    render(<WildfirePreviewBox 
        WildfireName={"firename"}
        CoordinatesLon={56.78}/>)
    expect(screen.getByText(/firename/i)).toBeInTheDocument();
    expect(screen.getByText(/56.78/i)).toBeInTheDocument();
})

test("CountyPreviewBox renders", ()=>{
    render(<CountyPreviewBox 
        Population={1234}
        CoordinatesLon={56.78}/>)
    expect(screen.getByText(/1234/i)).toBeInTheDocument();
    expect(screen.getByText(/56.78/i)).toBeInTheDocument();
})

test("FPAPreviewBox renders", ()=>{
    render(<FPAPreviewBox 
        FPAName={"fpaname"}
        CoordinatesLon={56.78}/>)
    expect(screen.getByText(/fpaname/i)).toBeInTheDocument();
    expect(screen.getByText(/56.78/i)).toBeInTheDocument();
})
