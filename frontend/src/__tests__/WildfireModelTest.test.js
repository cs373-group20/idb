import { render, screen } from '@testing-library/react'
import WildfireModelPage from '../pages/WildfireModelPage'
import fireData from '../data/fireData.json'

const mockUsedNavigate = jest.fn();
function mockWildfireLoader(){
    return fireData
}
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
 useNavigate: () => mockUsedNavigate,
 useLoaderData: mockWildfireLoader,
}));

test("WildfireModelPage renders", ()=>{
  render(<WildfireModelPage/>)
  expect(screen.getAllByText(/Date Discovered:/i)[0]).toBeInTheDocument();
  expect(screen.getAllByText(/Learn More/i)[0]).toBeInTheDocument();
  expect(screen.getAllByText(/Undetermined/i)[0]).toBeInTheDocument();
})