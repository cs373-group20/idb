import os
import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

URL = 'https://www.californiawildfires.me/'

def wait_for_url_change(driver, new_url):
    try:
        wait = WebDriverWait(driver, 10)
        wait.until(EC.url_to_be(new_url))
        return True
    except TimeoutException:
        return False

class SeleniumTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Inspired by https://gitlab.com/rparappuram/cs373-idb-12/-/blob/main/frontend/tests/common/driver.py.
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("window-size=2560x1440")

        if os.environ.get("CI") == "true":
            cls.driver = webdriver.Remote(
                command_executor="http://selenium__standalone-chrome:4444/wd/hub",
                options=chrome_options,
            )
        else:
            cls.driver = webdriver.Chrome()

    # test 1
    def test_Home_Page_Intro(self):
        driver = self.driver
        driver.get(URL)
        name_tag = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[1]/p[1]')
        description_tag = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[1]/p[2]')

        self.assertEqual(name_tag.text, 'California Wildfires')
        self.assertEqual(description_tag.text, 'Your best way to find information about the wildfires in California.')

    # test 2
    def test_NavBar_Text(self):
        driver = self.driver
        driver.get(URL)
        home_btn      = driver.find_element(By.XPATH, '/html/body/div/div[1]/div[1]/button[1]')
        about_btn     = driver.find_element(By.XPATH, '/html/body/div/div[1]/div[1]/button[2]')
        wildfires_btn = driver.find_element(By.XPATH, '/html/body/div/div[1]/div[1]/button[3]')
        counties_btn  = driver.find_element(By.XPATH, '/html/body/div/div[1]/div[1]/button[4]')
        fpa_btn       = driver.find_element(By.XPATH, '/html/body/div/div[1]/div[1]/button[5]')
        self.assertEqual(home_btn.text, 'Home')
        self.assertEqual(about_btn.text, 'About')
        self.assertEqual(wildfires_btn.text, 'Wildfires')
        self.assertEqual(counties_btn.text, 'Counties')
        self.assertEqual(fpa_btn.text, 'Fire Protection Agencies')

    # test 3
    def test_NavBar_Links(self):
        driver = self.driver
        driver.get(URL)
        home_btn      = driver.find_element(By.XPATH, '/html/body/div/div[1]/div[1]/button[1]')
        about_btn     = driver.find_element(By.XPATH, '/html/body/div/div[1]/div[1]/button[2]')
        wildfires_btn = driver.find_element(By.XPATH, '/html/body/div/div[1]/div[1]/button[3]')
        counties_btn  = driver.find_element(By.XPATH, '/html/body/div/div[1]/div[1]/button[4]')
        fpa_btn       = driver.find_element(By.XPATH, '/html/body/div/div[1]/div[1]/button[5]')
        
        home_btn.click()
        self.assertTrue(wait_for_url_change(driver, URL + ''))
        
        about_btn.click()
        self.assertTrue(wait_for_url_change(driver, URL + 'about'))
        
        wildfires_btn.click()
        self.assertTrue(wait_for_url_change(driver, URL + 'wildfires'))
        
        counties_btn.click()
        self.assertTrue(wait_for_url_change(driver, URL + 'counties'))
        
        fpa_btn.click()
        self.assertTrue(wait_for_url_change(driver, URL + 'facilities'))

    # test4
    def test_Background_Pic(self):
        driver = self.driver
        driver.get(URL)
        
        background_tag = driver.find_element(By.XPATH, '/html/body/div/div[2]')
        bg_tag_style   = background_tag.get_attribute('style') 
        
        self.assertTrue("background-image" in bg_tag_style,
                        "Background image src attribute is empty.")
    
    # test 5
    def test_About_Page_Intro(self):
        driver = self.driver
        driver.get(URL + 'about')
        
        name_tag = driver.find_element(By.XPATH, '/html/body/div/div[2]/p[1]')
        description_tag = driver.find_element(By.XPATH, '/html/body/div/div[2]/p[2]')
        
        self.assertEqual(name_tag.text, 'California Wildfires')
        self.assertEqual(len(description_tag.text), 331)
    
    # test 6
    def test_DevCard_names(self):
        driver = self.driver
        driver.get(URL + 'about')

        dev1_name = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[1]/div[1]/p')
        dev2_name = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[1]/div[2]/p')
        dev3_name = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[1]/div[3]/p')
        dev4_name = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[1]/div[4]/p')
        dev5_name = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[1]/div[5]/p')

        self.assertEqual(dev1_name.text, "Abdulrahman Alshahrani")
        self.assertEqual(dev2_name.text, "Daehyun Kyoung")
        self.assertEqual(dev3_name.text, "Imad Hussein")
        self.assertEqual(dev4_name.text, "Hunter Samra")
        self.assertEqual(dev5_name.text, "Jayden Chakranarayan")

    # test 7
    def test_DevCard_pics(self):
        driver = self.driver
        driver.get(URL + 'about')

        dev1_pic = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[1]/div[1]/img').get_attribute('src')
        dev2_pic = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[1]/div[2]/img').get_attribute('src')
        dev3_pic = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[1]/div[3]/img').get_attribute('src')
        dev4_pic = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[1]/div[4]/img').get_attribute('src')
        dev5_pic = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[1]/div[5]/img').get_attribute('src')

        self.assertIsNotNone(dev1_pic, "DevCard1 image is missing or empty.")
        self.assertIsNotNone(dev2_pic, "DevCard2 image is missing or empty.")
        self.assertIsNotNone(dev3_pic, "DevCard3 image is missing or empty.")
        self.assertIsNotNone(dev4_pic, "DevCard4 image is missing or empty.")
        self.assertIsNotNone(dev5_pic, "DevCard5 image is missing or empty.")

    # test 8
    def test_tools_text(self):
        driver = self.driver
        driver.get(URL + 'about')

        tool1_name = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[1]/button').text
        tool1_bio  = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[1]/div').text
        self.assertEqual(tool1_name, 'React')
        self.assertIsNotNone(tool1_bio, "Tool 1 bio is missing or empty.")


        tool2_name = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[2]/button').text
        tool2_bio  = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[2]/div').text
        self.assertEqual(tool2_name, 'GitLab')
        self.assertIsNotNone(tool2_bio, "Tool 2 bio is missing or empty.")

        
        tool3_name = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[3]/button').text
        tool3_bio  = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[3]/div').text
        self.assertEqual(tool3_name, 'Figma')
        self.assertIsNotNone(tool3_bio, "Tool 3 bio is missing or empty.")

        
        tool4_name = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[4]/button').text
        tool4_bio  = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[4]/div').text
        self.assertEqual(tool4_name, 'AWS Amplify Studio')
        self.assertIsNotNone(tool4_bio, "Tool 4 bio is missing or empty.")
        
        tool5_name = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[5]/button').text
        tool5_bio  = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[5]/div').text
        self.assertEqual(tool5_name, 'Postman')
        self.assertIsNotNone(tool5_bio, "Tool 5 bio is missing or empty.")

        tool6_name = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[6]/button').text
        tool6_bio  = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[6]/div').text
        self.assertEqual(tool6_name, 'NPM')
        self.assertIsNotNone(tool6_bio, "Tool 6 bio is missing or empty.")

        tool7_name = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[7]/button').text
        tool7_bio  = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[7]/div').text
        self.assertEqual(tool7_name, 'Namecheap')
        self.assertIsNotNone(tool7_bio, "Tool 7 bio is missing or empty.")
    
    # test 9
    def test_tools_links(self):
        driver = self.driver
        driver.get(URL + 'about')
        orig_url = driver.current_url
        main_window = driver.current_window_handle

        def check_other_tab_exists ():
            exists = False
            for handle in driver.window_handles:
                if handle != main_window:
                    exists = True
                    driver.switch_to.window(handle)
                    self.assertNotEqual(driver.current_window_handle, main_window)
                    driver.close()
                    driver.switch_to.window(main_window)
                    driver.get(orig_url)
            self.assertEqual(exists, True)

        tool1_btn = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[1]/button')
        tool1_btn.click()
        check_other_tab_exists()

        tool2_btn = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[2]/button')
        tool2_btn.click()
        check_other_tab_exists()

        tool3_btn = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[3]/button')
        tool3_btn.click()
        check_other_tab_exists()
        
        tool4_btn = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[4]/button')
        tool4_btn.click()
        check_other_tab_exists()
        
        tool5_btn = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[5]/button')
        tool5_btn.click()
        check_other_tab_exists()
        
        tool6_btn = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[6]/button')
        tool6_btn.click()
        check_other_tab_exists()
        
        tool7_btn = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[3]/div[7]/button')
        tool7_btn.click()
        check_other_tab_exists()
    
    # test 10
    def test_sources_links(self):
        driver = self.driver
        driver.get(URL + 'about')
        orig_url = driver.current_url
        main_window = driver.current_window_handle

        def check_other_tab_exists ():
            exists = False
            for handle in driver.window_handles:
                if handle != main_window:
                    exists = True
                    driver.switch_to.window(handle)
                    self.assertNotEqual(driver.current_window_handle, main_window)
                    driver.close()
                    driver.switch_to.window(main_window)
                    driver.get(orig_url)
            self.assertEqual(exists, True)

        src1_btn = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[4]/button/p')
        src1_btn.click()
        check_other_tab_exists()

        src2_btn = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[5]/button[1]/p')
        src2_btn.click()
        check_other_tab_exists()

        src3_btn = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[5]/button[2]/p')
        src3_btn.click()
        check_other_tab_exists()
        
        src4_btn = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[5]/button[3]/p')
        src4_btn.click()
        check_other_tab_exists()
        
        src5_btn = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[6]/button/p')
        src5_btn.click()
        check_other_tab_exists()

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == '__main__':
    unittest.main()
