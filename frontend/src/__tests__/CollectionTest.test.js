import { render, screen } from '@testing-library/react'
import WildfirePreviewBoxGrid from '../collections/WildfirePreviewBoxGrid'
import CountyPreviewBoxGrid from '../collections/CountyPreviewBoxGrid'
import FPAPreviewBoxGrid from '../collections/FPAPreviewBoxGrid'

const mockUsedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
   ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockUsedNavigate,
}));

const wildfireitems = [
    {
        id:"uniqueid",
        WildfireName:"somename",
        CoordinatesLat:1234,
        CoordinatesLon:5678,
        DateDiscovered:"01 123 123",
        EstimatedCost:10,
        Size:0.0001,
        Cause:"abc",
        Complexity:null,
    },
    {
        id:"uniqueid2",
        Cause:"efasd",
        EstimatedCost:101010
    }
]

test("WildfirePreviewBoxGrid renders", ()=>{
    render(<WildfirePreviewBoxGrid 
        items={wildfireitems}/>)
    expect(screen.getByText(/0.0001/i)).toBeInTheDocument();
    expect(screen.getByText(/01 123 123/i)).toBeInTheDocument();
    expect(screen.getByText(/efasd/i)).toBeInTheDocument();
    expect(screen.getByText(/101010/i)).toBeInTheDocument();
})

const countyitems = [
    {
        id:"uniqueid",
        CoordinatesLat:621,
    },
    {
        id:"uniqueid2",
        Region:"bravo"
    },
    {
        id:"uniqueid3",
        Area:9
    }
]
test("CountyPreviewBoxGrid renders", ()=>{
    render(<CountyPreviewBoxGrid 
        items={countyitems}/>)
    expect(screen.getByText(/621/i)).toBeInTheDocument();
    expect(screen.getByText(/bravo/i)).toBeInTheDocument();
    expect(screen.getByText(/9/i)).toBeInTheDocument();
})

const fpaitems = [
    {
        id:"uniqueid",
        FPAName:"somename",
    },
    {
        id:"uniqueid2",
        Scope:"charlie"
    },
    {
        id:"uniqueid3",
        CoordinatesLon:9.5
    },
    {
        id:"uniqueid4",
        Address:"1234 some street"
    }
]
test("FPAPreviewBoxGrid renders", ()=>{
    render(<FPAPreviewBoxGrid
        items={fpaitems}/>)
    expect(screen.getByText(/somename/i)).toBeInTheDocument();
    expect(screen.getByText(/charlie/i)).toBeInTheDocument();
    expect(screen.getByText(/9.5/i)).toBeInTheDocument();
    expect(screen.getByText(/1234 some street/i)).toBeInTheDocument();
})