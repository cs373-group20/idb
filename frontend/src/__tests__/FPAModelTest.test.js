import { render, screen } from '@testing-library/react'
import FPAModelPage from '../pages/FPAModelPage'
import facilityData from '../data/facilityData.json'

const mockUsedNavigate = jest.fn();
function mockLoader(){
    return facilityData
}
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
 useNavigate: () => mockUsedNavigate,
 useLoaderData: mockLoader,
}));

test("FPAModelPage renders", ()=>{
  render(<FPAModelPage/>)
  expect(screen.getByText(/Number of elements/i)).toBeInTheDocument();
  expect(screen.getAllByText(/Learn More/i)[0]).toBeInTheDocument();
  expect(screen.getAllByText(/STATE/i)[0]).toBeInTheDocument();
})