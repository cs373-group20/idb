import { render, screen } from '@testing-library/react'
import NavBarActive from '../pages/NavBarActive'
const mockUsedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
   ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockUsedNavigate,
}));

test("NavBarActive renders", ()=>{
    render(<NavBarActive/>);
    const element1 = screen.getByText(/Home/i);
    const element2 = screen.getByText(/About/i);
    const element3 = screen.getByText(/Wildfires/i);
    const element4 = screen.getByText(/Counties/i);
    const element5 = screen.getByText(/Fire Protection Agencies/i);
    expect(element1).toBeInTheDocument();
    expect(element2).toBeInTheDocument();
    expect(element3).toBeInTheDocument();
    expect(element4).toBeInTheDocument();
    expect(element5).toBeInTheDocument();
})

