import React from 'react';
import { BrowserRouter as Router, Routes, Route }
    from 'react-router-dom';
import {createBrowserRouter, RouterProvider, Outlet} from 'react-router-dom'
import HomePageExtend from './pages/home'
import AboutPageExtend from './pages/about'
import GlobalSearchPage, {loader as searchLoader} from './pages/globalSearch'
import {ModelListPageExtend
}from './pages/modelList'
import ModelPageExtend from './pages/model'
import NavBarActive from './pages/NavBarActive'
import contents from './contents.json'
import WildfireModelPage, {loader as wildfireModelLoader} from './pages/WildfireModelPage'
import CountyModelPage, {loader as countyModelLoader} from './pages/CountyModelPage'
import FPAModelPage, {loader as fpaModelLoader} from './pages/FPAModelPage'
import WildfireInstance, {loader as wildfireLoader} from './pages/WildfireInstance'
import CountyInstance, {loader as countyLoader} from './pages/CountyInstance'
import FPAInstance, {loader as fpaLoader} from './pages/FPAInstance'
import Visualization from './pages/visualizations'

function Layout(){
  return(
    <>
      <NavBarActive/>
      <Outlet/>
    </>
  )
}

const router = createBrowserRouter([
  {
    element: <Layout/>,
    children: [
    {
      path: "/",
      element: <HomePageExtend name = {contents.homepage.name} imageurl={contents.homepage.imageurl}/>,
    },
    {
      path: "/about",
      element:<AboutPageExtend/>,
    },
    {
      path:"/wildfires",
      loader: wildfireModelLoader,
      element:<WildfireModelPage/>,
    },
    {
      path:"/facilities",
      loader: fpaModelLoader,
      element:<FPAModelPage/>,
    },
    {
      path:"/counties",
      loader: countyModelLoader,
      element:<CountyModelPage/>,
    },
    {
      path:"/wildfires/:wildfirename",
      loader: wildfireLoader,
      element:<WildfireInstance/>,
    },
    {
      path:"/counties/:countyname",
      loader: countyLoader,
      element:<CountyInstance/>,
    },
    {
      path:"/facilities/:FPAname",
      loader: fpaLoader,
      element:<FPAInstance/>,
    },
    {
      path:"/search/:searchValue",
      loader: searchLoader,
      element:<GlobalSearchPage/>,
    },
    {
      path:"/visualizations",
      element:<Visualization/>,
    }
    ]
  }
]);

function App() {
  return (
    <>
    <RouterProvider router={router} />
    </>
  );
}

export default App;
