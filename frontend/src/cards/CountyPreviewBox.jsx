/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import {
  getOverrideProps,
  getOverridesFromVariants,
  mergeVariantsAndOverrides,
} from "@aws-amplify/ui-react/internal";
import { Button, Flex, Image, Text, View } from "@aws-amplify/ui-react";
import EmbeddedMapComponent from "../pages/EmbeddedMap"
import HighlighterIfText from "../functions/highlighter";
export default function CountyPreviewBox(props) {
  const {
    Region,
    CoordinatesLat,
    Area,
    Population,
    FireMAR,
    PopulationDensity,
    CoordinatesLon,
    CountyName,
    LearnMoreButtonOnClick,
    ExternalUrl,
    SearchTerms,
    overrides: overridesProp,
    ...rest
  } = props;
  const variants = [
    {
      overrides: {
        Name: {},
        Line1Title: {},
        Line1Text: {},
        Line1: {},
        Line2Title: {},
        Line2Text: {},
        Line2: {},
        Line3Title: {},
        Line3Text: {},
        Line3: {},
        Line4Title: {},
        Line4Text: {},
        Line4: {},
        Line5Title: {},
        Line5Text: {},
        Line5: {},
        Line6Title: {},
        Line6Text: {},
        Line6: {},
        TextArea: {},
        Image: {},
        Button: {},
        CountyPreviewBox: {},
      },
      variantValues: { property1: "Default" },
    },
  ];
  const overrides = mergeVariantsAndOverrides(
    getOverridesFromVariants(variants, props),
    overridesProp || {}
  );
  return (
    <View
      width="300px"
      height="500px"
      display="block"
      gap="unset"
      alignItems="unset"
      justifyContent="unset"
      overflow="hidden"
      position="relative"
      padding="0px 0px 0px 0px"
      backgroundColor="rgba(174,179,183,1)"
      {...getOverrideProps(overrides, "CountyPreviewBox")}
      {...rest}
    >
      <Flex
        gap="0"
        direction="column"
        width="300px"
        height="300px"
        justifyContent="flex-start"
        alignItems="center"
        position="absolute"
        top="200px"
        left="0px"
        padding="10px 10px 10px 10px"
        display="flex"
        {...getOverrideProps(overrides, "TextArea")}
      >
        <Text
          fontFamily="Inter"
          fontSize="16px"
          fontWeight="600"
          color="rgba(0,0,0,1)"
          lineHeight="30px"
          textAlign="center"
          display="block"
          direction="column"
          justifyContent="unset"
          width="227px"
          height="60px"
          gap="unset"
          alignItems="unset"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          whiteSpace="pre-wrap"
          {...getOverrideProps(overrides, "Name")}
        >
          <HighlighterIfText
            searchWords={SearchTerms}
            textToHighlight={CountyName}
          />
        </Text>
        <Flex
          gap="5px"
          direction="row"
          width="246px"
          height="19px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          padding="0px 10px 0px 10px"
          display="flex"
          {...getOverrideProps(overrides, "Line1")}
        >
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="400"
            color="rgba(0,0,0,1)"
            lineHeight="21px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Area:"
            {...getOverrideProps(overrides, "Line1Title")}
          ></Text>
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="400"
            color="rgba(0,0,0,1)"
            lineHeight="21px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="179px"
            height="18px"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children={`${Area}${" sq. miles"}`}
            {...getOverrideProps(overrides, "Line1Text")}
          >
            <HighlighterIfText
              searchWords={SearchTerms}
              textToHighlight={`${Area}${" sq. miles"}`}
            />
          </Text>
        </Flex>
        <Flex
          gap="5px"
          direction="row"
          width="246px"
          height="19px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          padding="0px 10px 0px 10px"
          display="flex"
          {...getOverrideProps(overrides, "Line2")}
        >
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="400"
            color="rgba(0,0,0,1)"
            lineHeight="21px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Population:"
            {...getOverrideProps(overrides, "Line2Title")}
          ></Text>
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="400"
            color="rgba(0,0,0,1)"
            lineHeight="21px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children={Population}
            {...getOverrideProps(overrides, "Line2Text")}
          >
            <HighlighterIfText
              searchWords={SearchTerms}
              textToHighlight={Population}
            />
          </Text>
        </Flex>
        <Flex
          gap="5px"
          direction="row"
          width="246px"
          height="19px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          padding="0px 10px 0px 10px"
          display="flex"
          {...getOverrideProps(overrides, "Line3")}
        >
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="400"
            color="rgba(0,0,0,1)"
            lineHeight="21px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Population Density:"
            {...getOverrideProps(overrides, "Line3Title")}
          ></Text>
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="400"
            color="rgba(0,0,0,1)"
            lineHeight="21px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children={PopulationDensity}
            {...getOverrideProps(overrides, "Line3Text")}
          >
            <HighlighterIfText
              searchWords={SearchTerms}
              textToHighlight={PopulationDensity}
            />
          </Text>
        </Flex>
        <Flex
          gap="5px"
          direction="row"
          width="246px"
          height="unset"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          padding="0px 10px 0px 10px"
          display="flex"
          {...getOverrideProps(overrides, "Line4")}
        >
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="400"
            color="rgba(0,0,0,1)"
            lineHeight="21px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Region:"
            {...getOverrideProps(overrides, "Line4Title")}
          ></Text>
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="400"
            color="rgba(0,0,0,1)"
            lineHeight="21px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children={Region}
            {...getOverrideProps(overrides, "Line4Text")}
          >
            <HighlighterIfText
              searchWords={SearchTerms}
              textToHighlight={Region}
            />
          </Text>
        </Flex>
        <Flex
          gap="5px"
          direction="row"
          width="246px"
          height="unset"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          padding="0px 10px 0px 10px"
          display="flex"
          {...getOverrideProps(overrides, "Line5")}
        >
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="400"
            color="rgba(0,0,0,1)"
            lineHeight="21px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Fire MAR:"
            {...getOverrideProps(overrides, "Line5Title")}
          ></Text>
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="400"
            color="rgba(0,0,0,1)"
            lineHeight="21px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children={FireMAR}
            {...getOverrideProps(overrides, "Line5Text")}
          >
            <HighlighterIfText
              searchWords={SearchTerms}
              textToHighlight={FireMAR}
            />
          </Text>
        </Flex>
        <Flex
          gap="0"
          direction="column"
          width="246px"
          height="unset"
          justifyContent="center"
          alignItems="flex-start"
          shrink="0"
          position="relative"
          padding="0px 10px 0px 10px"
          display="flex"
          {...getOverrideProps(overrides, "Line6")}
        >
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="400"
            color="rgba(0,0,0,1)"
            lineHeight="21px"
            textAlign="right"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Coordinates:"
            {...getOverrideProps(overrides, "Line6Title")}
          ></Text>
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="400"
            color="rgba(0,0,0,1)"
            lineHeight="21px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="138px"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children={`${"("}${CoordinatesLat}${","}${CoordinatesLon}${")"}`}
            {...getOverrideProps(overrides, "Line6Text")}
          >
            <HighlighterIfText
              searchWords={SearchTerms}
              textToHighlight={`${"("}${CoordinatesLat}${","}${CoordinatesLon}${")"}`}
            />
          </Text>
        </Flex>
        <br />
        <Button
          width="200px"
          height="35px"
          shrink="0"
          size="default"
          isDisabled={false}
          variation="primary"
          children="Learn More"
          onClick={LearnMoreButtonOnClick}
          {...getOverrideProps(overrides, "LearnMoreButton")}
        ></Button>
      </Flex>
      <Image
          width="300px"
          height="200px"
          display="block"
          gap="unset"
          alignItems="unset"
          justifyContent="unset"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          objectFit="cover"
          src={ExternalUrl}
          {...getOverrideProps(overrides, "Image")}
        >
        </Image>
    </View>
  );
}
