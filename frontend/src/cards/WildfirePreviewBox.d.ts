/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { ButtonProps, FlexProps, ImageProps, TextProps, ViewProps } from "@aws-amplify/ui-react";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type WildfirePreviewBoxOverridesProps = {
    WildfirePreviewBox?: PrimitiveOverrideProps<ViewProps>;
    TextArea?: PrimitiveOverrideProps<FlexProps>;
    Name?: PrimitiveOverrideProps<TextProps>;
    Line1?: PrimitiveOverrideProps<FlexProps>;
    Line1Title?: PrimitiveOverrideProps<TextProps>;
    Line1Text?: PrimitiveOverrideProps<TextProps>;
    Line2?: PrimitiveOverrideProps<FlexProps>;
    Line2Title?: PrimitiveOverrideProps<TextProps>;
    Line2Text?: PrimitiveOverrideProps<TextProps>;
    Line3?: PrimitiveOverrideProps<FlexProps>;
    Line3Title?: PrimitiveOverrideProps<TextProps>;
    Line3Text?: PrimitiveOverrideProps<TextProps>;
    Line4?: PrimitiveOverrideProps<FlexProps>;
    Line4Title?: PrimitiveOverrideProps<TextProps>;
    Line4Text?: PrimitiveOverrideProps<TextProps>;
    Line5?: PrimitiveOverrideProps<FlexProps>;
    Line5Title?: PrimitiveOverrideProps<TextProps>;
    Line5Text?: PrimitiveOverrideProps<TextProps>;
    Line6?: PrimitiveOverrideProps<FlexProps>;
    Line6Title?: PrimitiveOverrideProps<TextProps>;
    Line6Text?: PrimitiveOverrideProps<TextProps>;
    Image?: PrimitiveOverrideProps<ImageProps>;
    LearnMoreButton?: PrimitiveOverrideProps<ButtonProps>;
    Button?: PrimitiveOverrideProps<ButtonProps>;
} & EscapeHatchProps;
export declare type WildfirePreviewBoxProps = React.PropsWithChildren<Partial<ViewProps> & {
    DateDiscovered?: String;
    EstimatedCost?: Number;
    Size?: Number;
    Cause?: String;
    Dispatch?: String;
    CoordinatesLat?: Number;
    CoordinatesLon?: Number;
    WildfireName?: String;
    LearnMoreButtonOnClick?: undefined;
    ExternalUrl?: String;
    SearchTerms?: any[];
} & {
    property1?: "Default";
} & {
    overrides?: WildfirePreviewBoxOverridesProps | undefined | null;
}>;
export default function WildfirePreviewBox(props: WildfirePreviewBoxProps): React.ReactElement;
