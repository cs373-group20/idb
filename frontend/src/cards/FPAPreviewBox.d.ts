/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { ButtonProps, FlexProps, ImageProps, TextProps, ViewProps } from "@aws-amplify/ui-react";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type FPAPreviewBoxOverridesProps = {
    FPAPreviewBox?: PrimitiveOverrideProps<ViewProps>;
    TextArea?: PrimitiveOverrideProps<FlexProps>;
    Name?: PrimitiveOverrideProps<TextProps>;
    Line1?: PrimitiveOverrideProps<FlexProps>;
    Line1Title?: PrimitiveOverrideProps<TextProps>;
    Line1Text?: PrimitiveOverrideProps<TextProps>;
    Line2?: PrimitiveOverrideProps<FlexProps>;
    Line2Title?: PrimitiveOverrideProps<TextProps>;
    Line2Text?: PrimitiveOverrideProps<TextProps>;
    Line3?: PrimitiveOverrideProps<FlexProps>;
    Line3Title?: PrimitiveOverrideProps<TextProps>;
    Line3Text?: PrimitiveOverrideProps<TextProps>;
    Line4?: PrimitiveOverrideProps<FlexProps>;
    Line4Title40991240?: PrimitiveOverrideProps<TextProps>;
    Line4Title40991241?: PrimitiveOverrideProps<TextProps>;
    Line5?: PrimitiveOverrideProps<FlexProps>;
    Line5Title?: PrimitiveOverrideProps<TextProps>;
    Line5Text?: PrimitiveOverrideProps<TextProps>;
    Line6?: PrimitiveOverrideProps<FlexProps>;
    Line6Title?: PrimitiveOverrideProps<TextProps>;
    Line6Text?: PrimitiveOverrideProps<TextProps>;
    Image?: PrimitiveOverrideProps<ImageProps>;
    LearnMoreButton?: PrimitiveOverrideProps<ButtonProps>;
} & EscapeHatchProps;
export declare type FPAPreviewBoxProps = React.PropsWithChildren<Partial<ViewProps> & {
    Scope?: String;
    Address?: String;
    PhoneNumber?: String;
    CoordinatesLon?: Number;
    CoordinatesLat?: Number;
    Status?: String;
    Type?: String;
    FPAName?: String;
    LearnMoreButtonOnClick?: undefined;
    ExternalUrl?: String;
    SearchTerms?: any[];
} & {
    property1?: "Default";
} & {
    overrides?: FPAPreviewBoxOverridesProps | undefined | null;
}>;
export default function FPAPreviewBox(props: FPAPreviewBoxProps): React.ReactElement;
