/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { FlexProps, ImageProps, TextProps } from "@aws-amplify/ui-react";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type CountyPageOverridesProps = {
    CountyPage?: PrimitiveOverrideProps<FlexProps>;
    ContentFrame?: PrimitiveOverrideProps<FlexProps>;
    Name?: PrimitiveOverrideProps<TextProps>;
    Image?: PrimitiveOverrideProps<ImageProps>;
    Area?: PrimitiveOverrideProps<TextProps>;
    Population?: PrimitiveOverrideProps<TextProps>;
    "Population Density"?: PrimitiveOverrideProps<TextProps>;
    Region?: PrimitiveOverrideProps<TextProps>;
    FireMAR?: PrimitiveOverrideProps<TextProps>;
    Coordinates?: PrimitiveOverrideProps<TextProps>;
} & EscapeHatchProps;
export declare type CountyPageProps = React.PropsWithChildren<Partial<FlexProps> & {
    CountyName?: String;
    Area?: Number;
    Population?: Number;
    PopulationDensity?: Number;
    Region?: String;
    FireMAR?: Number;
    CoordinatesLat?: Number;
    CoordinatesLon?: Number;
    ExternalUrl?: String;
} & {
    overrides?: CountyPageOverridesProps | undefined | null;
}>;
export default function CountyPage(props: CountyPageProps): React.ReactElement;
