/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { FlexProps, ImageProps, TextProps } from "@aws-amplify/ui-react";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type WildfirePageOverridesProps = {
    WildfirePage?: PrimitiveOverrideProps<FlexProps>;
    ContentFrame?: PrimitiveOverrideProps<FlexProps>;
    Name?: PrimitiveOverrideProps<TextProps>;
    Image?: PrimitiveOverrideProps<ImageProps>;
    County?: PrimitiveOverrideProps<TextProps>;
    Complexity?: PrimitiveOverrideProps<TextProps>;
    Size?: PrimitiveOverrideProps<TextProps>;
    Cost?: PrimitiveOverrideProps<TextProps>;
    Containment?: PrimitiveOverrideProps<TextProps>;
    Coordinates?: PrimitiveOverrideProps<TextProps>;
    Reported?: PrimitiveOverrideProps<TextProps>;
    Cause?: PrimitiveOverrideProps<TextProps>;
    Fuel?: PrimitiveOverrideProps<TextProps>;
} & EscapeHatchProps;
export declare type WildfirePageProps = React.PropsWithChildren<Partial<FlexProps> & {
    FireName?: String;
    County?: String;
    Dispatch?: String;
    DateDiscovered?: String;
    Cause?: String;
    Fuel?: String;
    CoordinatesLon?: Number;
    CoordinatesLat?: Number;
    Containment?: Number;
    Cost?: Number;
    Size?: Number;
    ExternalUrl?: String;
} & {
    overrides?: WildfirePageOverridesProps | undefined | null;
}>;
export default function WildfirePage(props: WildfirePageProps): React.ReactElement;
