/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { FlexProps, ImageProps, TextProps } from "@aws-amplify/ui-react";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type FPAPageOverridesProps = {
    FPAPage?: PrimitiveOverrideProps<FlexProps>;
    ContentFrame?: PrimitiveOverrideProps<FlexProps>;
    Name?: PrimitiveOverrideProps<TextProps>;
    Image?: PrimitiveOverrideProps<ImageProps>;
    County?: PrimitiveOverrideProps<TextProps>;
    Status?: PrimitiveOverrideProps<TextProps>;
    Type?: PrimitiveOverrideProps<TextProps>;
    Scope?: PrimitiveOverrideProps<TextProps>;
    Address?: PrimitiveOverrideProps<TextProps>;
    PhoneNumber?: PrimitiveOverrideProps<TextProps>;
    Coordinates?: PrimitiveOverrideProps<TextProps>;
} & EscapeHatchProps;
export declare type FPAPageProps = React.PropsWithChildren<Partial<FlexProps> & {
    FacilityName?: String;
    County?: String;
    Status?: String;
    Type?: String;
    Scope?: String;
    Address?: String;
    PhoneNumber?: String;
    CoordinatesLat?: Number;
    CoordinatesLon?: Number;
    ExternalUrl?: String;
} & {
    overrides?: FPAPageOverridesProps | undefined | null;
}>;
export default function FPAPage(props: FPAPageProps): React.ReactElement;
