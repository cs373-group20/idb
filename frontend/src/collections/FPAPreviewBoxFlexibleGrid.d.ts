/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { FPAPreviewBoxProps } from "../cards/FPAPreviewBox";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { CollectionProps } from "@aws-amplify/ui-react";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type FPAPreviewBoxFlexibleGridOverridesProps = {
    FPAPreviewBoxFlexibleGrid?: PrimitiveOverrideProps<CollectionProps>;
    FPAPreviewBox?: FPAPreviewBoxProps;
} & EscapeHatchProps;
export declare type FPAPreviewBoxFlexibleGridProps = React.PropsWithChildren<Partial<CollectionProps<any>> & {
    items?: any[];
    overrideItems?: (collectionItem: {
        item: any;
        index: number;
    }) => FPAPreviewBoxProps;
} & {
    overrides?: FPAPreviewBoxFlexibleGridOverridesProps | undefined | null;
}>;
export default function FPAPreviewBoxFlexibleGrid(props: FPAPreviewBoxFlexibleGridProps): React.ReactElement;
