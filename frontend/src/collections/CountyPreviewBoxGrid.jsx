/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import {useState, React} from "react";
import CountyPreviewBox from "../cards/CountyPreviewBox";
import { getOverrideProps } from "@aws-amplify/ui-react/internal";
import { Collection } from "@aws-amplify/ui-react";
export default function CountyPreviewBoxGrid(props) {
  const { items, overrideItems, overrides, SearchTerms, ...rest } = props;
  return (
    <Collection
      maxWidth="100%"
      type="list"
      direction="row"
      wrap="wrap"
      isPaginated={true}
      searchPlaceholder="Search..."
      itemsPerPage={20}
      columnEnd={3}
      autoFlow="row"
      alignItems="center"
      justifyContent="center"
      items={items || []}
      {...getOverrideProps(overrides, "CountyPreviewBoxGrid")}
      {...rest}
    >
      {(item, index) => (
        item.CoordinatesLat && <CountyPreviewBox
          margin="10px 10px 10px 10px"
          key={item.id}
          CountyName={item.CountyName}
          Region={item.Region}
          CoordinatesLat={item.CoordinatesLat}
          Area={item.Area}
          Population={item.Population}
          FireMAR={item.FireMAR}
          PopulationDensity={item.PopulationDensity}
          CoordinatesLon={item.CoordinatesLon}
          LearnMoreButtonOnClick={item.LearnMoreButtonOnClick}
          ExternalUrl={item.img_url}
          SearchTerms={SearchTerms}
          {...(overrideItems && overrideItems({ item, index }))}
        ></CountyPreviewBox>
      )}
    </Collection>
  );
}
