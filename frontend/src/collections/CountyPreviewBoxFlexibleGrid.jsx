/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import CountyPreviewBox from "../cards/CountyPreviewBox";
import { getOverrideProps } from "@aws-amplify/ui-react/internal";
import { Collection } from "@aws-amplify/ui-react";
export default function CountyPreviewBoxFlexibleGrid(props) {
  const { items, overrideItems, overrides, ...rest } = props;
  return (
    <Collection
      type="grid"
      searchPlaceholder="Search..."
      templateColumns="1fr 1fr 1fr"
      autoFlow="row"
      alignItems="stretch"
      justifyContent="stretch"
      items={items || []}
      {...getOverrideProps(overrides, "CountyPreviewBoxFlexibleGrid")}
      {...rest}
    >
      {(item, index) => (
        <CountyPreviewBox
          margin="10px 10px 10px 10px"
          key={item.id}
          CountyName={item.CountyName}
          Region={item.Region}
          CoordinatesLat={item.CoordinatesLat}
          Area={item.Area}
          Population={item.Population}
          FireMAR={item.FireMAR}
          PopulationDensity={item.PopulationDensity}
          CoordinatesLon={item.CoordinatesLon}
          ExternalUrl={item.img_url}
          {...(overrideItems && overrideItems({ item, index }))}
        ></CountyPreviewBox>
      )}
    </Collection>
  );
}
