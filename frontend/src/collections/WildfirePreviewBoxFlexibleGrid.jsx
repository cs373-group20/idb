/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import WildfirePreviewBox from "../cards/WildfirePreviewBox";
import { getOverrideProps } from "@aws-amplify/ui-react/internal";
import { Collection } from "@aws-amplify/ui-react";
export default function WildfirePreviewBoxFlexibleGrid(props) {
  const { items, overrideItems, overrides, ...rest } = props;
  return (
    <Collection
      type="grid"
      searchPlaceholder="Search..."
      templateColumns="1fr 1fr 1fr"
      autoFlow="row"
      alignItems="stretch"
      justifyContent="stretch"
      items={items || []}
      {...getOverrideProps(overrides, "WildfirePreviewBoxFlexibleGrid")}
      {...rest}
    >
      {(item, index) => (
        <WildfirePreviewBox
          margin="10px 10px 10px 10px"
          key={item.id}
          WildfireName={item.WildfireName}
          CoordinatesLat={item.CoordinatesLat}
          CoordinatesLon={item.CoordinatesLon}
          DateDiscovered={item.DateDiscovered}
          EstimatedCost={item.EstimatedCost}
          Size={item.Size}
          Cause={item.Cause}
          Complexity={item.Complexity}
          ExternalUrl={item.img_url}
          {...(overrideItems && overrideItems({ item, index }))}
        ></WildfirePreviewBox>
      )}
    </Collection>
  );
}
