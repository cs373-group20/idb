/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import FPAPreviewBox from "../cards/FPAPreviewBox";
import { getOverrideProps } from "@aws-amplify/ui-react/internal";
import { Collection } from "@aws-amplify/ui-react";
export default function FPAPreviewBoxGrid(props) {
  const { items, overrideItems, overrides, SearchTerms, ...rest } = props;
  return (
    <Collection
      maxWidth="100%"
      type="list"
      direction="row"
      wrap="wrap"
      isPaginated={true}
      searchPlaceholder="Search..."
      itemsPerPage={20}
      columnEnd={3}
      autoFlow="row"
      alignItems="center"
      justifyContent="center"
      items={items || []}
      {...getOverrideProps(overrides, "FPAPreviewBoxGrid")}
      {...rest}
    >
      {(item, index) => (
        item.CoordinatesLat && <FPAPreviewBox
          margin="10px 10px 10px 10px"
          key={item.id}
          FPAName={item.FPAName}
          Scope={item.Scope}
          Address={item.Address}
          PhoneNumber={item.PhoneNumber}
          CoordinatesLon={item.CoordinatesLon}
          CoordinatesLat={item.CoordinatesLat}
          Status={item.Status}
          Type={item.Type}
          LearnMoreButtonOnClick={item.LearnMoreButtonOnClick}
          ExternalUrl={item.img_url}
          SearchTerms={SearchTerms}
          {...(overrideItems && overrideItems({ item, index }))}
        ></FPAPreviewBox>
      )}
    </Collection>
  );
}
