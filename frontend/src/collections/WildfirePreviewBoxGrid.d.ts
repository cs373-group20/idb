/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { WildfirePreviewBoxProps } from "../cards/WildfirePreviewBox";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { CollectionProps } from "@aws-amplify/ui-react";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type WildfirePreviewBoxGridOverridesProps = {
    WildfirePreviewBoxGrid?: PrimitiveOverrideProps<CollectionProps>;
    WildfirePreviewBox?: WildfirePreviewBoxProps;
} & EscapeHatchProps;
export declare type WildfirePreviewBoxGridProps = React.PropsWithChildren<Partial<CollectionProps<any>> & {
    items?: any[];
    SearchTerms?: any[];
    overrideItems?: (collectionItem: {
        item: any;
        index: number;
    }) => WildfirePreviewBoxProps;
} & {
    overrides?: WildfirePreviewBoxGridOverridesProps | undefined | null;
}>;
export default function WildfirePreviewBoxGrid(props: WildfirePreviewBoxGridProps): React.ReactElement;
