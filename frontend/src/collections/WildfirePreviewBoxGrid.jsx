/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import WildfirePreviewBox from "../cards/WildfirePreviewBox";
import { getOverrideProps } from "@aws-amplify/ui-react/internal";
import { Flex, Collection } from "@aws-amplify/ui-react";
export default function WildfirePreviewBoxGrid(props) {
  const { items, overrideItems, overrides, SearchTerms, ...rest } = props;
  return (
    <Collection
      maxWidth="100%"
      type="list"
      direction="row"
      wrap="wrap"
      isPaginated={true}
      searchPlaceholder="Search..."
      itemsPerPage={20}
      columnEnd={3}
      autoFlow="row"
      alignItems="center"
      justifyContent="center"
      items={items || []}

      {...getOverrideProps(overrides, "WildfirePreviewBoxGrid")}
      {...rest}
    >
      {(item, index) => (
        item.CoordinatesLat && 
        <WildfirePreviewBox
          margin="10px 10px 10px 10px"
          key={item.id}
          WildfireName={item.WildfireName}
          CoordinatesLat={item.CoordinatesLat}
          CoordinatesLon={item.CoordinatesLon}
          DateDiscovered={item.DateDiscovered}
          EstimatedCost={item.EstimatedCost}
          Size={item.Size}
          Cause={item.Cause}
          Dispatch={item.Dispatch}
          LearnMoreButtonOnClick={item.LearnMoreButtonOnClick}
          ExternalUrl={item.img_url}
          SearchTerms={SearchTerms}
          {...(overrideItems && overrideItems({ item, index }))}
        ></WildfirePreviewBox>
      )}
    </Collection>
  );
}
