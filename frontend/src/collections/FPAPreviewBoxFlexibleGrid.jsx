/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import FPAPreviewBox from "../cards/FPAPreviewBox";
import { getOverrideProps } from "@aws-amplify/ui-react/internal";
import { Collection } from "@aws-amplify/ui-react";
export default function FPAPreviewBoxFlexibleGrid(props) {
  const { items, overrideItems, overrides, ...rest } = props;
  return (
    <Collection
      type="grid"
      searchPlaceholder="Search..."
      templateColumns="1fr 1fr 1fr"
      autoFlow="row"
      alignItems="stretch"
      justifyContent="stretch"
      items={items || []}
      {...getOverrideProps(overrides, "FPAPreviewBoxFlexibleGrid")}
      {...rest}
    >
      {(item, index) => (
        <FPAPreviewBox
          margin="10px 10px 10px 10px"
          key={item.id}
          FPAName={item.FPAName}
          Scope={item.Scope}
          Address={item.Address}
          PhoneNumber={item.PhoneNumber}
          CoordinatesLon={item.CoordinatesLon}
          CoordinatesLat={item.CoordinatesLat}
          Status={item.Status}
          Type={item.Type}
          ExternalUrl={item.img_url}
          {...(overrideItems && overrideItems({ item, index }))}
        ></FPAPreviewBox>
      )}
    </Collection>
  );
}
