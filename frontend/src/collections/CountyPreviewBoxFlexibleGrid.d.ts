/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { CountyPreviewBoxProps } from "../cards/CountyPreviewBox";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { CollectionProps } from "@aws-amplify/ui-react";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type CountyPreviewBoxFlexibleGridOverridesProps = {
    CountyPreviewBoxFlexibleGrid?: PrimitiveOverrideProps<CollectionProps>;
    CountyPreviewBox?: CountyPreviewBoxProps;
} & EscapeHatchProps;
export declare type CountyPreviewBoxFlexibleGridProps = React.PropsWithChildren<Partial<CollectionProps<any>> & {
    items?: any[];
    overrideItems?: (collectionItem: {
        item: any;
        index: number;
    }) => CountyPreviewBoxProps;
} & {
    overrides?: CountyPreviewBoxFlexibleGridOverridesProps | undefined | null;
}>;
export default function CountyPreviewBoxFlexibleGrid(props: CountyPreviewBoxFlexibleGridProps): React.ReactElement;
