export const dataSources = {
    counties:
    {
        a:"https://public.opendatasoft.com/api/explore/v2.1/catalog/datasets/us-county-boundaries/records?select=stusab%2C%20name%2C%20%20geo_point_2d%2C%20aland%2C%20awater&refine=stusab%3ACA",
        b:"https://services.arcgis.com/BLN4oKB0N1YSgvY8/arcgis/rest/services/Counties_in_California/FeatureServer/0/query?where=1%3D1&outFields=CountyName,AdminRegion,FireMAR&outSR=4326&f=json",
        c:"https://api.census.gov/data/2019/pep/population?get=NAME,POP,DENSITY&for=county:*&in=state:06&key=b48f14e98f0efbf0a1a7adaf5a29a6852de44d61"
    },

    wildfires:
    {
        a:"https://services3.arcgis.com/T4QMspbfLg3qTGWY/arcgis/rest/services/WFIGS_Incident_Locations_Current/FeatureServer/0/query?where=1%3D1&outFields=ContainmentDateTime,ControlDateTime,CreatedBySystem,IncidentSize,DiscoveryAcres,DispatchCenterID,EstimatedCostToDate,FinalAcres,FinalFireReportApprovedDate,FireBehaviorGeneral,FireBehaviorGeneral1,FireBehaviorGeneral2,FireBehaviorGeneral3,FireCause,FireDiscoveryDateTime,FireMgmtComplexity,FireOutDateTime,FireStrategyConfinePercent,FireStrategyFullSuppPercent,FireStrategyMonitorPercent,FireStrategyPointZonePercent,ICS209ReportDateTime,ICS209ReportForTimePeriodFrom,ICS209ReportForTimePeriodTo,ICS209ReportStatus,IncidentManagementOrganization,IncidentName,IncidentShortDescription,IncidentTypeCategory,IncidentTypeKind,InitialLatitude,InitialLongitude,InitialResponseAcres,InitialResponseDateTime,IsFireCauseInvestigated,IsFireCodeRequested,IsMultiJurisdictional,IsReimbursable,IsTrespass,IsUnifiedCommand,LocalIncidentIdentifier,ModifiedBySystem,PercentContained,PercentPerimeterToBeContained,PrimaryFuelModel,SecondaryFuelModel,TotalIncidentPersonnel,UniqueFireIdentifier,EstimatedFinalCost,CreatedOnDateTime_dt,ModifiedOnDateTime_dt,POOCity,POOFips,POODispatchCenterID,POOCounty,POOProtectingAgency,POOProtectingUnit,SourceOID,FireCode,IsFSAssisted,IrwinID&outSR=4326&f=json"
    },

    facilities:
    {
        a:"https://egis.fire.ca.gov/arcgis/rest/services/FRAP/Facilities/MapServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json"
    }
}