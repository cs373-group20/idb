export const toolsInfo = 
[
    {
    "name": 'React',
    "url": 'https://react.dev/',
    "img": 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1024px-React-icon.svg.png',
    "bio": "An Open Source Library to build the front-end user interfaces on Javascript.",
    },
    {
    "name": 'GitLab',
    "url": 'https://gitlab.com/',
    "img": 'https://about.gitlab.com/images/press/logo/png/gitlab-logo-500.png',
    "bio": "An Open-Core version control website to develop the project collaboratively.",
    },
    {
    "name": 'Figma',
    "url": 'https://www.figma.com/',
    "img": 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Figma-logo.svg/600px-Figma-logo.svg.png',
    "bio": "A collaborative application to design the UI interfaces for the project.",
    },
    {
    "name": 'AWS Amplify Studio',
    "url": 'https://aws.amazon.com/amplify/studio/',
    "img": 'https://pbs.twimg.com/profile_images/1114309924551417856/FKA4cm2x_400x400.png',
    "bio": "A full-stack platform to host the project and build the UI in React.",
    },
    {
    "name": 'Postman',
    "url": 'https://www.postman.com/',
    "img": 'https://voyager.postman.com/logo/postman-logo-orange-stacked.svg',
    "bio": "A platform to build and document the APIs for the project.",
    },
    {
    "name": 'NPM',
    "url": 'https://www.npmjs.com/',
    "img": 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Npm-logo.svg/810px-Npm-logo.svg.png',
    "bio": "A Javascript package manager to manage react and amplify projects for the project.",
    },
    {
    "name": 'Namecheap',
    "url": 'https://www.namecheap.com/',
    "img": 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Namecheap_Logo.svg/1920px-Namecheap_Logo.svg.png',
    "bio": "A domain name registrar to register the domain of the project.",
    }
];
