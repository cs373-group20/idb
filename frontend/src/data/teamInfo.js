export const teamInfo = 
[
    {
    "name": 'Abdulrahman Alshahrani',
    "gitlabNames": ['Abdomash', 'Abdulrahman Alshahrani'],
    "gitlabID": '15658777',
    "photo": 'https://miro.medium.com/v2/resize:fit:828/format:webp/0*-BqMuBGJCy2BSWLX.jpeg',
    "bio": "Hello! I'm a Junior Computer Science student at UT Austin. I enjoy cooking and playing video games in my free time.",
    "role": 'Frontend',
    "commits": 0,
    "issues": 0,
    "unittests": 10
    },
    {
    "name": 'Daehyun Kyoung',
    "gitlabNames": ['danielkyoung', 'Daehyun Kyoung'],
    "gitlabID": '7672395',
    "photo": 'https://miro.medium.com/v2/resize:fit:828/format:webp/0*tWzg2cZznMyvQBw3.jpeg',
    "bio": "I'm a 5th year student at UT. I work part time at Applied Research Laboratories. I do bit of running as hobby",
    "role": 'Frontend',
    "commits": 0,
    "issues": 0,
    "unittests": 10
    },
    {
    "name": 'Imad Hussein',
    "gitlabNames": ['Imad F Hussein', 'Imad Hussein'],
    "gitlabID": '15686235',
    "photo": 'https://miro.medium.com/v2/resize:fit:828/format:webp/1*6F2FYcIT3DEnB0x89dojKw.jpeg',
    "bio": "Hello, I'm a senior CS student at UT Austin. In my freetime I enjoy watching new shows and playing video games.",
    "role": 'Backend',
    "commits": 0,
    "issues": 0,
    "unittests": 0
    },
    {
    "name": 'Hunter Samra',
    "gitlabNames": ['Hunter S', 'hunter24', 'Hunter Samra'],
    "gitlabID": '15588191',
    "photo": 'https://miro.medium.com/v2/resize:fit:828/format:webp/0*SMKWWcxxAiQD776g.jpeg',
    "bio": "Hello, I'm a Senior Computer Science major at the University of Texas at Austin. In my free time I enjoy playing spikeball and ultamite frisbee.",
    "role": 'Backend',
    "commits": 0,
    "issues": 0,
    "unittests": 7
    },
    {
    "name": 'Jayden Chakranarayan',
    "gitlabNames": ['Jayden Chakranarayan'],
    "gitlabID": '12420438',
    "photo": 'https://miro.medium.com/v2/resize:fit:828/format:webp/0*D1aB4uJLXyk-oVou.png',
    "bio": "Hey! I'm currently a senior studying CS at UT Austin. I like to play video games and play basketball in my freetime",
    "role": 'Backend',
    "commits": 0,
    "issues": 0,
    "unittests": 0
    }
];
