import React, { useEffect, useState } from "react";
import { Chart, CategoryScale, LinearScale, Title, Legend, LineElement, LineController, PointElement, Tooltip } from 'chart.js'
import { Line } from 'react-chartjs-2';
import { getFromAllCounties, getFromAllFacilities, getFromAllWildfires } from "./dataFilter";
import { Loader } from "@aws-amplify/ui-react";

Chart.register(
    CategoryScale,
    LinearScale,
    LineController,
    LineElement,
    Legend,
    PointElement,
    Title,
    Tooltip
);

function createDataSet(counties, fires, facilities) {
    let fireCount = Array(counties.length).fill(0);
    fires.forEach(([countyName]) => {
        let i = counties.findIndex(props => props[0] === countyName);
        if(i !== -1){
            fireCount[i] += 1;
        }
    });

    let facilityCount = Array(counties.length).fill(0);
    facilities.forEach(([countyName]) => {
        let i = counties.findIndex(props => props[0] === countyName);
        if (i !== -1) {
            facilityCount[i] += 1;
        }
    })

    let data = {}
    data["labels"] = counties;
    data["datasets"] = [
        {
            label:"Number of Wildfires",
            data:fireCount,
            borderWidth:3,
            backgroundColor:'rgb(235, 64, 52)',
            pointRadius:5,
            tension:0.5
        },
        {
            label:"Number of Facilities",
            data:facilityCount,
            borderWidth:3,
            backgroundColor:'rgb(240, 185, 46)',
            pointRadius:5,
            tension:0.5
        }
    ];
    return data;
}

export default function Visualization1(props){
    const [dataSet, setDataSet] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const facilitiesData = await getFromAllFacilities(["county"]);
            const wildfiresData = await getFromAllWildfires(["county"]);
            const countiesData = await getFromAllCounties(["name"]);

            const newDataSet = createDataSet(countiesData, wildfiresData, facilitiesData);
            setDataSet(newDataSet);
        };

        fetchData();
    }, []);

    if (!dataSet) {
        // Wait until dataset gets loaded.
        return <Loader size="large"/>;
    }

    const options = {
        legend: {
            position:'top'
        },
        tooltips: {
            mode: 'index'
        },
        hover: {
            mode: 'index'
        }
    }
    return(
        <Line
            options = {options}
            data = {dataSet}
        />
    )
};


