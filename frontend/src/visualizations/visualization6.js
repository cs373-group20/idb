import React, { useEffect, useState } from "react";
import { Chart, Legend, LineElement, Tooltip } from 'chart.js'
import { Scatter } from 'react-chartjs-2';
import { Loader } from "@aws-amplify/ui-react";

const aidURL = "https://api.southwesthurricaneaid.me/api/aid_organizations?page=1&per_page=1000";
const hurricaneURL = "https://api.southwesthurricaneaid.me/api/hurricanes?page=1&per_page=100";

Chart.register(
    Tooltip,
    Legend,
    LineElement
);

async function createDataSet (aidData, hurricaneData) {

    const hurricanes = hurricaneData.hurricanes;
    const aids = aidData.aid_organizations;

    let aidCounties = new Map();
    aids.forEach(aid => {
        aidCounties.set(aid.county.name, (aidCounties.get(aid.county.name) || 0) + 1);
    })

    let hurrCounties = new Map();
    hurricanes.forEach(hurricane => {
        console.log("AID:", hurricane);
        hurricane.counties.forEach(county => {
            if (aidCounties.has(county.name)){
                hurrCounties.set(county.name, (hurrCounties.get(county.name) || 0) + 1);
            }
        })
    })

    let hurrCounts = [];
    let aidCounts = [];
    let counties = [];

    aidCounties.forEach((value, county) => {
        if (hurrCounties.has(county)){
            counties.push(county);
            hurrCounts.push(hurrCounties.get(county));
            aidCounts.push(value);
        }
    })

    let countyData = counties.map((county, index) => ({
        county,
        hurrCount: hurrCounts[index],
        aidCount: aidCounts[index],
        totalCount: hurrCounts[index] + aidCounts[index],
    }));
      
    countyData.sort((a, b) => b.totalCount - a.totalCount);
    
    hurrCounts = countyData.map(data => data.hurrCount);
    aidCounts = countyData.map(data => data.aidCount);
    counties = countyData.map(data => data.county);

    let countsData = [];
    counties.forEach((_, index) => {
        countsData.push({
            x: hurrCounts[index],
            y: aidCounts[index],
        });
    })
    

    let data = {}
    data["labels"] = counties;
    data["datasets"] = [{
        label:"Counties",
        data:countsData,
        backgroundColor:"red",
        borderColor:"red",
        borderWidth:2,
    }];

    return data;
}


export default function Visualization4(props){

    const [dataSet, setDataSet] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const aidData = await fetch(aidURL).then(response => response.json());
            const hurricaneData = await fetch(hurricaneURL).then(response => response.json());
            const newDataSet = await createDataSet(aidData, hurricaneData);
            setDataSet(newDataSet);
        }

        fetchData();
    }, []);

    if (!dataSet) {
        // Wait until dataset gets loaded.
        return <Loader size="large" />;
    }

    const options = {
        legend: {
            position:'top'
        },
        tooltips: {
            mode: 'index'
        },
        hover: {
            mode: 'index'
        },
        scales: {
            x: {
                title: {
                    display: true,
                    text: "Number of Hurricanes",
                },
            },
            y: {
                title: {
                    display: true,
                    text: "Number of Hurricane Aid Organizations",
                },
            },
        },
    }
    
    return(
        <Scatter
            options = {options}
            data = {dataSet}
        />
    )
};


