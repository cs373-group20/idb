import React, { useEffect, useState } from "react";
import { Chart, Tooltip } from 'chart.js'
import { Bar } from 'react-chartjs-2';
import { Loader } from "@aws-amplify/ui-react";

const apiURL = "https://api.southwesthurricaneaid.me/api/hurricanes?page=1&per_page=100";

Chart.register(
    Tooltip
  );

async function createDataSet (hurricaneData) {

    let hurricanes = hurricaneData.hurricanes;
    let categoryCounts = {};

    hurricanes.forEach(hurricane => {
        let category = hurricane.category;
        if (!(category in categoryCounts)) {
        categoryCounts[category] = [0, 0];
        }
        categoryCounts[category][0] += 1;
        categoryCounts[category][1] += hurricane.deaths_number;
    });

    let categories = Object.keys(categoryCounts).sort();
    let deathsList = categories.map(category => categoryCounts[category]);
    let avgDeaths = deathsList.map(pair => Math.max(pair[1] / pair[0], 0));

    let data = {}
    data["labels"] = categories.map(category => `Category: ${category}`);
    data["datasets"] = [{
        label:"Average deaths",
        data:avgDeaths,
        backgroundColor:'rgba(235, 64, 52, 0.6)',
        borderColor:'rgba(235, 64, 52, 1)',
        borderWidth:2,
    }];

    return data;
}


export default function Visualization4(props){

    const [dataSet, setDataSet] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const data = await fetch(apiURL).then(response => response.json());
            const newDataSet = await createDataSet(data);
            setDataSet(newDataSet);
        }

        fetchData();
    }, []);

    if (!dataSet) {
        // Wait until dataset gets loaded.
        return <Loader size="large" />;
    }

    const options = {
        legend: {
            position:'top'
        },
        tooltips: {
            mode: 'index'
        },
        hover: {
            mode: 'index'
        }
    }
    
    return(
        <Bar
            options = {options}
            data = {dataSet}
        />
    )
};


