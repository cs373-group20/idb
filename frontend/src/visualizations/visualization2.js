import React, { useEffect, useState } from "react";
import {Chart, CategoryScale, LinearScale, BarElement, Title,Legend, Tooltip} from 'chart.js'
import { Bar } from 'react-chartjs-2';
import { getFromAllWildfires } from "./dataFilter";
import { Loader } from "@aws-amplify/ui-react";
import { dateFormatter } from "../functions/parsers";

Chart.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Legend,
    Tooltip
);

function countDatesByWeek(dates) {
    const startDate = new Date(Math.min(...dates));
    const endDate = new Date(Math.max(...dates));

    const weekNames = [];
    const weekCounts = [];

    let currentDate = new Date(startDate);
    while (currentDate <= endDate) {
        const weekStart = new Date(currentDate);
        const weekEnd = new Date(currentDate);
        weekEnd.setDate(currentDate.getDate() + 6);

        const count = dates.filter(date => date >= weekStart && date <= weekEnd).length;

        weekNames.push(weekStart.toDateString());
        weekCounts.push(count);

        currentDate.setDate(currentDate.getDate() + 7);
    }

    return [weekNames, weekCounts];
}

function createDataSet(wildfires) {
    const dates = wildfires.map(([date]) => new Date(date));
    let [weekNames, weekValues] = countDatesByWeek(dates);
    weekNames = weekNames.map((dateStr) => {return `Week of ` + dateFormatter(dateStr);})

    let data = {}
    data["labels"] = weekNames;
    data["datasets"] = [{
        label:`Number of Wildfires`,
        data:weekValues,
        borderWidth:1,
        minBarLength:0.1,
        backgroundColor:'rgba(235, 64, 52, 1)'}];
    return data;
}

export default function Visualization2(props){
    const [dataSet, setDataSet] = useState(null);
    
    useEffect(() => {
        const fetchData = async () => {
            const wildfiresData = await getFromAllWildfires(["discover_date"]);
            
            const newDataSet = createDataSet(wildfiresData);
            setDataSet(newDataSet);
        };
        
        fetchData();
    }, []);
    
    if (!dataSet) {
        // Wait until dataset gets loaded.
        return <Loader size="large" />;
    }
    
    const options = {
        legend: {
            position:'top'
        },
        tooltips: {
            mode: 'index'
        },
        hover: {
            mode: 'index'
        }
    }
    return(
        <Bar
            options = {options}
            data = {dataSet}
        />
    )
};


