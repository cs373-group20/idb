const fireURL = "https://api.californiawildfires.me/allWildfires";
const facilitiesURL = "https://api.californiawildfires.me/allFacilities?order=name";
const countiesURL = "https://api.californiawildfires.me/allCounties?order=name";

const hurricanesURL = "https://api.southwesthurricaneaid.me/api/hurricanes?page=1&per_page=100"

let allWildfires;
let allCounties;
let allFacilities;

let allHurricanes;

async function fetchData(url) {
    const response = await fetch(url);
    const data = await response.json();
    return data;
}

export async function getFromAllWildfires(props) {
    if (!allWildfires) {
        allWildfires = await fetchData(fireURL);
    }

    let fires = [];
    allWildfires.forEach(elem => {
        let properties = [];
        props.forEach((property) => {
            properties.push(elem[property]);
        })
        fires.push(properties);
    })
    return fires;
}

export async function getFromAllFacilities(props) {
    if (!allFacilities) {
        allFacilities = await fetchData(facilitiesURL);
    }

    let facilities = [];
    allFacilities.forEach(elem => {
        let properties = [];
        props.forEach((property) => {
            properties.push(elem[property]);
        })
        facilities.push(properties);
    })
    return facilities;
}

export async function getFromAllCounties(props) {
    if (!allCounties) {
        allCounties = await fetchData(countiesURL);
    }

    let counties = [];
    allCounties.forEach(elem => {
        let properties = [];
        props.forEach((property) => {
            properties.push(elem[property]);
        })
        counties.push(properties);
    })
    return counties;
}

export async function getFromAllHurricanes(props) {
    if (!allHurricanes) {
        allHurricanes = await fetchData(hurricanesURL);
    }

    let hurricanes = [];
    allHurricanes.hurricanes.forEach(elem => {
        

        hurricanes.push(elem);
    })
    return hurricanes;
}

