import React, { useEffect, useState } from "react";
import { Chart, Tooltip, ArcElement } from 'chart.js'
import { Pie } from 'react-chartjs-2';
import { getFromAllFacilities } from "./dataFilter";
import { Loader } from "@aws-amplify/ui-react";

Chart.register(
    ArcElement,
    Tooltip
);

function countFacilitiesTypes (unpackedFacilities) {
    const typesMap = unpackedFacilities.reduce((occurrenceMap, value) => {
        occurrenceMap.set(value, (occurrenceMap.get(value) || 0) + 1);
        return occurrenceMap;
    }, new Map());

    let types  = [];
    let values = [];
    
    typesMap.forEach((value, key) => {
        types.push(key);
        values.push(value);
    });

    const sortedIndices = values.map((_, index) => index).sort((a, b) => values[b] - values[a]);
    types = sortedIndices.map(index => types[index]);
    values = sortedIndices.map(index => values[index]);

    return [types, values];
}

function createDataSet(facilities) {

    const unpackedFacilities = facilities.map(([facility]) => facility);
    const [types, values] = countFacilitiesTypes(unpackedFacilities);

    let pieColors = [
        "rgba(255, 99, 132, ",
        "rgba(255, 159, 64, ",
        "rgba(255, 205, 86, ",
        "rgba(75, 192, 192, ",
        "rgba(54, 162, 235, ",
        "rgba(153, 102, 255, ",
        "rgba(255, 77, 77, ",
        "rgba(255, 116, 78, ",
        "rgba(255, 153, 102, ",
        "rgba(75, 192, 192, ",
        "rgba(123, 239, 178, ",
        "rgba(159, 207, 96, ",
        "rgba(189, 197, 129, ",
        "rgba(255, 99, 132, ",
        "rgba(144, 255, 159, ",
        "rgba(255, 131, 131, ",
        "rgba(112, 128, 144, ",
        "rgba(203, 192, 255, ",
        "rgba(255, 186, 124, ",
        "rgba(107, 138, 247, ",
        "rgba(199, 236, 238, ",
        "rgba(255, 204, 92, ",
        "rgba(146, 208, 80, ",
        "rgba(255, 107, 107, ",
        "rgba(255, 180, 107, ",
        "rgba(131, 224, 170, ",
        "rgba(246, 229, 141, ",
        "rgba(196, 248, 20, ",
    ]

    const piecesColors = types.map((_, index) => {return pieColors[index] + "0.5)"});
    const borderColors = types.map((_, index) => {return pieColors[index] + "1)"});
    let data = {}
    data["labels"] = types;
    data["datasets"] = [{
        label:"Type of Facility",
        data:values,
        backgroundColor:piecesColors,
        borderColor:borderColors,
        borderWidth:0.5
    }];
    return data;
}

export default function Visualization3(props){

    const [dataSet, setDataSet] = useState(null);
    
    useEffect(() => {
        const fetchData = async () => {
            const facilitiesData = await getFromAllFacilities(["type"]);
            
            const newDataSet = createDataSet(facilitiesData);
            setDataSet(newDataSet);
        };
        
        fetchData();
    }, []);

    if (!dataSet) {
        // Wait until dataset gets loaded.
        return <Loader size="large" />;
    }

    const options = {
        legend: {
            position:'top'
        },
        tooltips: {
            mode: 'index'
        },
        hover: {
            mode: 'index'
        }
    }
    
    return(
        <Pie
            options = {options}
            data = {dataSet}
        />
    )
};


