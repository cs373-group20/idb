import React, { useEffect, useState } from "react";
import { Chart,  LinearScale, Title, Legend, PointElement, LineElement, Tooltip } from 'chart.js'
import { Scatter } from 'react-chartjs-2';
import { getFromAllHurricanes } from "./dataFilter";
import { Loader } from "@aws-amplify/ui-react";

 Chart.register(
     LinearScale,
     PointElement,
     LineElement,
     Tooltip,
     Legend
   );

function createDataSet(hurricanes) {
    let pressures_windspeed_data = [];
    let wind_speed = [];
    hurricanes.forEach((hurricane) => {
        let this_pressure = hurricane.lowest_pressure_mbar;
        if (this_pressure != 0){
            let pressure_windspeed = {};
            pressure_windspeed["x"] = this_pressure
            pressure_windspeed["y"] = hurricane.highest_winds_mph
            pressures_windspeed_data.push(pressure_windspeed);
        }
    });
    let data = {
        datasets: [
          {
            label: 'Wind Speed (mph)',
            data: pressures_windspeed_data,
            backgroundColor: 'rgba(255, 99, 132, 1)',
          },
        ],
      };
    return data;
}

export default function Visualization5(props){
    const [dataSet, setDataSet] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const hurricanesData = await getFromAllHurricanes(["lowest_pressure_mbar", "highest_wind_mph"]);

            const newDataSet = createDataSet(hurricanesData);
            setDataSet(newDataSet);
        };

        fetchData();
    }, []);

    const options = {
        legend: {
            position:'top'
        },
        tooltips: {
            mode: 'index'
        },
        hover: {
            mode: 'index'
        },
        scales: {
            y: {
                title: {
                    display: true,  
                    text: "maximum wind speed (mph)"
                }
            },
            x: {
              title: {
                display: true, 
                text: "lowest pressure (mbar)"
              }
            },
            },
    }

    
    if (!dataSet) {
        // Wait until dataset gets loaded.
        return <Loader size="large"/>;
    }

    
    return (
        <Scatter
        options = {options}
        data = {dataSet}
    />)
};


