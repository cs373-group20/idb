import Highlighter from "react-highlight-words";

export default function HighlightIfText(props){
    const {searchWords, textToHighlight} = props;
    let text = textToHighlight;
    if(text == null || text.length === 0){
        text = " ";
    }
    else if(typeof text != "string"){
        text = text.toString();
    }
    return (
        <Highlighter
            searchWords={searchWords != null ? searchWords : [""]}
            textToHighlight={text}
        />
    )
}