export function EpochToDateTime(epoch) {
    var d = new Date();
    d.setUTCSeconds(epoch / 100000);
    return(d.toDateString());
}

export function countyParser(county){
    const population = county.pop || "Unknown";
    const pop_density = county.pop_density || "Unknown";
    const region = county.region || "Unknown";
    const fireMAR = county.fireMAR || "Unknown";
    const area = county.area || "Unknown";

    return (
        {
            id:county.name,
            CountyName:county.name,
            Population:population,
            PopulationDensity:pop_density,
            Region:region,
            FireMAR:fireMAR,
            CoordinatesLat:county.lat,
            CoordinatesLon:county.lon,
            Area:area,
            img_url:county.img_url
        }
    )
}

export function dateFormatter(date) {
    const dateObject = new Date(date);
    
    const options = {
        month: 'short',
        day: 'numeric',
        year: 'numeric',
    };

    return new Intl.DateTimeFormat('en-US', options).format(dateObject);
}

export function fireParser(fire){
    const defaultImageUrl = "https://cdn.vectorstock.com/i/preview-1x/65/30/default-image-icon-missing-picture-page-vector-40546530.jpg";

    const cost        = fire.cost_to_date / 100 || fire.final_cost / 100 || "Unknown";
    const date        = dateFormatter(fire.discover_date) || "Unknown";
    const size        = fire.size || "Unknown";
    const imgurl      = fire.img_url || defaultImageUrl;
    const cause       = fire.cause || "Undetermined";
    const containment = fire.percent_contained || "Unknown";
    const fuel        = fire.primary_fuel || "Unknown";
    const dispatch    = fire.dispatch_center || "Unknown";
    
    return (
        {
            id:fire.name,
            WildfireName:fire.name,
            Dispatch:dispatch,
            DateDiscovered:date,
            CoordinatesLat:fire.lat,
            CoordinatesLon:fire.lon,
            Size:size,
            Cause:cause,
            Cost:cost,
            EstimatedCost:cost,
            Containment:containment,
            primary_fuel:fuel,
            img_url:imgurl
        }
    )
}

export function fpaParser(fpa){
    const phone = fpa.phone_num || "Unknown";
    const scope = fpa.owner || "Unknown"; 
    const address = fpa.address || "Unknown";
    const status = fpa.facility_status || "Unknown";
    const type = fpa.type || "Unknown";

    return (
        {
            id:fpa.name,
            Scope:scope,
            Address:address,
            PhoneNumber:phone,
            CoordinatesLon:fpa.lon,
            CoordinatesLat:fpa.lat,
            Status:status,
            Type:type,
            FPAName:fpa.name,
            img_url:fpa.img_url
        }
    )
}