export function fullWordFilter(cardArray, searchWord){
    return cardArray.filter((card) => {
        var found = false;

        for (const [key, word] of Object.entries(card)) {
            if(key != 'img_url'){
                if(typeof word == 'string'){
                    if(isNaN(searchWord) && word.toLowerCase().includes(searchWord.toLowerCase())){
                        found = true;
                    }
                }
                else if(word == Number(searchWord)){
                    found = true;
                }
            }
        }
        return found;
    });
}

export function multiWordFilter(cardArray, searchWordList){
    return cardArray.filter((card) => {
        var foundCount = 0;
        for (const [key, word] of Object.entries(card)) {
            if(key != 'img_url'){
                for(const search of searchWordList){
                    if(typeof word == 'string'){
                        if(isNaN(search) && word.toLowerCase().includes(search.toLowerCase())){
                            foundCount += 1;
                        }
                    }
                    else if(word == Number(search)){
                        foundCount += 1;
                    }
                }
            }
        }
        return foundCount > 1;
    });
}

export function singleWordFilter(cardArray, searchWordList){
    return cardArray.filter((card) => {
        var foundCount = 0;
        for (const [key, word] of Object.entries(card)) {
            if(key != 'img_url'){
                for(const search of searchWordList){
                    if(typeof word == 'string'){
                        if(isNaN(search) && word.toLowerCase().includes(search.toLowerCase())){
                            foundCount += 1;
                        }
                    }
                    else if(word == Number(search)){
                        foundCount += 1;
                    }
                }
            }
        }
        return foundCount > 0;
    });
}

export function generateSearchValueList(searchString){
    if(searchString.length == 0){
        return ([""]);
    }
    else{
        return (searchString.split(" ").filter((word) => word.length != 0));
    }
}