import React from "react";
import { useEffect, useState } from "react";
import { Flex, Text, SelectField, SearchField, ScrollView, CheckboxField, Loader } from "@aws-amplify/ui-react";
import FPAPreviewBoxGrid from '../collections/FPAPreviewBoxGrid'
import fpaData from '../data/facilityData.json'
import {fpaParser} from '../functions/parsers'
import { useNavigate } from 'react-router-dom'
import {fullWordFilter, multiWordFilter, singleWordFilter, generateSearchValueList} from '../functions/filters'

const url = "https://api.californiawildfires.me/allFacilities"
const types = ["FSA", "FSAB", "COM", "FSB", "FSL", "CC", "FSCC", "COM/LO", "HB", 
"ICC", "HQ", "LO", "CNA", "ADM", "SFM", "FCFFC", "TC", "OC", "ECC", 
"FCCCC", "EQF", "AAB", "DSA", "NUR", "VC", "YCC", "FSO", "RHQ"];
// const statuses = ["Active", "Decommissioned", "Temporarily Decommissioned"]

export function getFPAData(){
    return fpaData;
}

export async function loader(){    
    const fpa = await fetch(url)
           .then((response) => {
                console.log(response)
                return(response.json())})
           .catch((err) => {
              console.log(err.message);
              return fpaData
           });
     return fpa
}

function FPAModelPage() {
    document.title = "Fire Protection Agencies";
    const navigate = useNavigate()
    const [sortBy, setSortBy] = useState('?order=name');
    const [fpas, setFpas] = useState(0);
    const [facilitiesCards, setFacilitiesCards] = useState([]);
    const [facilityCheckboxValues, setFacilityCheckboxValues] = useState([]);
    
    useEffect(() => {
      const initialCheckboxValues = {};
      types.forEach(element => {
          initialCheckboxValues[element] = true;
      });
      setFacilityCheckboxValues(initialCheckboxValues);
    }, [])

    useEffect(() => {
      const fetchData = async () => {
          try {
              const response = await fetch(url + sortBy);
              const data = await response.json();
              return data;
          } catch (error) {
              console.error("Error fetching data:", error.message);
              return fpaData;
          }
      };

      fetchData().then(fpaData => {
        const values = new Set(fpaData.map(item => item.owner));
        console.log(values);
        setFpas(fpaData.filter(facility => {
          return facilityCheckboxValues[facility.type];
        }));
        setFacilitiesCards([]);
      });
    }, [sortBy, facilityCheckboxValues]);
    
    useEffect(() => {
      if (fpas) {
        const updatedFacilitiesCards = [];
    
        for (let i = 0; i < fpas.length; i++) {
          const fpa = fpaParser(fpas[i]);
          fpa['LearnMoreButtonOnClick'] = () => navigate(fpa.id);
          updatedFacilitiesCards.push(fpa);
        }
    
        setFacilitiesCards([...updatedFacilitiesCards]);
      }
        // eslint-disable-next-line
    }, [fpas]);

    const [searchValue, setSearchValue] = useState("");
    const [facilitiesCardsFiltered, setFacilitiesCardsFiltered] = useState([]);

    const onChange = async function(event){
      await setSearchValue(event.target.value);
    };

    const onClear = () => {
      setSearchValue([]);
      setFacilitiesCardsFiltered(facilitiesCards);
    };

    useEffect(function() {
      if(searchValue.length === 0){
        setFacilitiesCardsFiltered(facilitiesCards);
      }
      else{
        let searchValueList = searchValue.split(" ").filter((word) => word.length !== 0);
        if(searchValueList.length > 1){
          let fullMatchFiltered = fullWordFilter(facilitiesCards, searchValue);
          let facilitiesCardsFiltered = facilitiesCards.filter((card) => ! fullMatchFiltered.includes(card));
          let multiMatchFiltered = multiWordFilter(facilitiesCardsFiltered, searchValueList);
          facilitiesCardsFiltered = facilitiesCardsFiltered.filter((card) => ! multiMatchFiltered.includes(card));
          let singleMatchFiltered = singleWordFilter(facilitiesCardsFiltered, searchValueList);
          setFacilitiesCardsFiltered(fullMatchFiltered.concat(multiMatchFiltered, singleMatchFiltered));
        }
        else if(searchValueList.length > 0){
          let fullMatchFiltered = fullWordFilter(facilitiesCards, searchValue);
          let facilitiesCardsFiltered = facilitiesCards.filter((card) => !fullMatchFiltered.includes(card));
          let singleMatchFiltered = singleWordFilter(facilitiesCardsFiltered, searchValueList);
          setFacilitiesCardsFiltered(fullMatchFiltered.concat(singleMatchFiltered));
        }
      }
    }, [searchValue, facilitiesCards]);

    // Function to handle checkbox change
    const handleCheckboxChange = (event) => {
        const { name, checked } = event.target;
        setFacilityCheckboxValues((prevValues) => ({
            ...prevValues,
            [name]: checked,
        }));
    };

    // Generate checkboxes based on types
    const facilitiesCheckBoxes = types.map((element) => (
      <CheckboxField
          paddingLeft="15px"
          key={element}
          label={element}
          name={element}
          checked={facilityCheckboxValues[element] || false}
          onChange={handleCheckboxChange}
      />
    ));

    return(
        <Flex
        direction="column"
        justifyContent="flex-start"
        alignItems="center">
        <Flex
        gap="40px"
        direction="column"
        width="70%"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        overflow="hidden"
        position="relative"
        wrap="nowrap"
        padding="100px 0px 200px 0px"
        backgroundColor="rgba(255,255,255,1)"
        >
        <Text fontSize={30}>All the Fire Protection Agencies in California</Text>
        <Text fontSize={15}>Number of elements: {facilitiesCards.length}</Text>
        <SelectField
          width="unset"
          maxWidth="100%"
          label="Sort By"
          onChange={(e) => setSortBy(e.target.value)}
        >
          <option  value="?order=name">Name (A to Z)</option>
          <option value="?orderd=name">Name (Z to A)</option>
          <option  value="?order=name&owner=CITY">Show CITY Owned Facilites Only (A to Z)</option>
          <option value="?orderd=name&owner=CITY">Show CITY Owned Facilites Only (Z to A)</option>
          <option  value="?order=name&owner=STATE">Show STATE Owned Facilites Only (A to Z)</option>
          <option value="?orderd=name&owner=STATE">Show STATE Owned Facilites Only (Z to A)</option>
          <option  value="?order=name&owner=FEDERAL">Show FEDERAL Owned Facilites Only (A to Z)</option>
          <option value="?orderd=name&owner=FEDERAL">Show FEDERAL Owned Facilites Only (Z to A)</option>
          <option  value="?order=name&facility_status=Active">Show Active Facilities Only (A to Z)</option>
          <option value="?orderd=name&facility_status=Active">Show Active Facilities Only (Z to A)</option>
          <option  value="?order=name&facility_status=Decommissioned">Show Decommissioned Facilities Only (A to Z)</option>
          <option value="?orderd=name&facility_status=Decommissioned">Show Decommissioned Facilities Only (Z to A)</option>
          <option  value="?order=name&facility_status=Temporarily Decommissioned">Show Temporarily Decommissioned Facilities Only (A to Z)</option>
          <option value="?orderd=name&facility_status=Temporarily Decommissioned">Show Temporarily Decommissioned Facilities Only (Z to A)</option>

        </SelectField>
        <SearchField
          label = "search"
          onChange={onChange}
          onClear={onClear}
          value={searchValue}
        />
        <Flex
          gap="0px"
          direction="column"
          border="1px solid black"
          borderRadius="medium"
          boxShadow="medium"
          padding="5px 5px 0px 5px"
        >
            <Text
                padding="5px"
            >View Facilities of Type(s):</Text>
            <ScrollView
                height="150px"
                width="300px"
                maxWidth="100%"
                borderRadius="medium"
                backgroundColor="#f0f0f0"
            >
                {facilitiesCheckBoxes}
            </ScrollView>
        </Flex>
        {(facilitiesCardsFiltered.length !== 0 || searchValue.length !== 0) && <FPAPreviewBoxGrid 
          SearchTerms = {generateSearchValueList(searchValue)}
          items = {facilitiesCardsFiltered}></FPAPreviewBoxGrid>}
        {(facilitiesCardsFiltered.length === 0 && searchValue.length === 0) && <Loader size="large">Loading...</Loader>}
        </Flex>
        </Flex>
    )
}

export default FPAModelPage