import React, { useEffect, useState } from "react";
import {
  Flex,
  Text,
  SelectField,
  SearchField,
  SliderField,
  CheckboxField,
  Loader,
} from "@aws-amplify/ui-react";
import CountyPreviewBoxGrid from "../collections/CountyPreviewBoxGrid";
import countyData from "../data/countyData.json";
import { countyParser } from "../functions/parsers";
import { useNavigate } from "react-router-dom";
import {
  fullWordFilter,
  multiWordFilter,
  singleWordFilter,
  generateSearchValueList,
} from "../functions/filters";

const url = "https://api.californiawildfires.me/allCounties";

export function getCountyData() {
  return countyData;
}

export async function testloader() {
  return countyData;
}

export async function loader() {
  const counties = await fetch(url)
    .then((response) => {
      return response.json();
    })
    .catch((err) => {
      console.log(err.message);
      return countyData;
    });
  return counties;
}

export default function CountyModelPage() {
  document.title = "Counties";
  const navigate = useNavigate();

  const [sortBy, setSortBy] = useState("?order=name");
  const [counties, setCounties] = useState([]);
  const [countiesCards, setCountiesCards] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [countiesCardsFiltered, setCountiesCardsFiltered] = useState([]);
  const [useFireMarFilter, setUseFireMarFilter] = useState(false);
  const [fireMarValue, setFireMarValue] = useState(1);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(url + sortBy);
        const data = await response.json();
        return data;
      } catch (error) {
        console.error("Error fetching data:", error.message);
        return countyData;
      }
    };

    fetchData().then((data) => {
      setCounties(data);
      setCountiesCards([]);
    });
  }, [sortBy]);

  useEffect(() => {
    if (counties) {
      const updatedCountiesCards = [];

      for (let i = 0; i < counties.length; i++) {
        const county = countyParser(counties[i]);
        county["LearnMoreButtonOnClick"] = () => navigate(county.CountyName);
        updatedCountiesCards.push(county);
      }

      setCountiesCards([...updatedCountiesCards]);
    }
    // eslint-disable-next-line
  }, [counties]);

  const onChange = async function (event) {
    await setSearchValue(event.target.value);
  };

  const onClear = () => {
    setSearchValue([]);
  };

  useEffect(function () {
    if (searchValue.length === 0) {
        setCountiesCardsFiltered(countiesCards)
    } else {
        let searchValueList = searchValue.split(" ").filter((word) => word.length !== 0);
        if (searchValueList.length > 1) {
            console.log("multi-word search")
            let fullMatchFiltered = fullWordFilter(countiesCards, searchValue);
            let countiesCardsFiltered = countiesCards.filter((card) => !fullMatchFiltered.includes(card));
            let multiMatchFiltered = multiWordFilter(countiesCardsFiltered, searchValueList);
            countiesCardsFiltered = countiesCardsFiltered.filter((card) => !multiMatchFiltered.includes(card));
            let singleMatchFiltered = singleWordFilter(countiesCardsFiltered, searchValueList);
            setCountiesCardsFiltered(fullMatchFiltered.concat(multiMatchFiltered, singleMatchFiltered));
        } else if (searchValueList.length > 0) {
            let fullMatchFiltered = fullWordFilter(countiesCards, searchValue);
            let countiesCardsFiltered = countiesCards.filter((card) => !fullMatchFiltered.includes(card));
            let singleMatchFiltered = singleWordFilter(countiesCardsFiltered, searchValueList);
            setCountiesCardsFiltered(fullMatchFiltered.concat(singleMatchFiltered));
        }
    }
}, [searchValue, countiesCards]);

  const handleCheckboxChange = () => {
    setUseFireMarFilter(!useFireMarFilter);

    if (useFireMarFilter) {
        setSortBy(sortBy.split("&")[0]);
    } else {
        setFireMarValue(1);
        setSortBy(sortBy + "&fireMAR=1")
    }
  };

  const handleSliderChange = (value) => {
    setFireMarValue(value);
    if (useFireMarFilter) {
      setSortBy(`?order=name&fireMAR=${value}`);
    }
  };

  const handleSelectChange = (value) => {
    if (useFireMarFilter) {
        setSortBy(value + `&fireMAR=${fireMarValue}`)
    } else {
        setSortBy(value);
    }
  }

  return (
    <Flex
      direction="column"
      justifyContent="flex-start"
      alignItems="center"
    >
      <Flex
        gap="40px"
        direction="column"
        width="70%"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        overflow="hidden"
        position="relative"
        wrap="nowrap"
        padding="100px 0px 200px 0px"
        backgroundColor="rgba(255,255,255,1)"
      >
        <Text fontSize={30}>All the Counties in California</Text>
        <Text fontSize={15}>
          Number of elements: {countiesCards.length}
        </Text>
        <Flex
          direction="column"
          wrap="wrap"
          alignItems="bottom"
          gap="20px"
        >
          <SelectField
            width="unset"
            maxWidth="100%"
            label="Sort By"
            onChange={(e) => handleSelectChange(e.target.value)}
          >
            <option value="?order=name">Name (A to Z)</option>
            <option value="?orderd=name">Name (Z to A)</option>
            <option value="?order=pop">Population (Low to High)</option>
            <option value="?orderd=pop">Population (High to Low)</option>
            <option value="?order=pop_density">
              Population Density (Low to High)
            </option>
            <option value="?orderd=pop_density">
              Population Density (High to Low)
            </option>
            <option value="?order=area">Area (Low to High)</option>
            <option value="?orderd=area">Area (High to Low)</option>
          </SelectField>
          <SearchField
            label="search"
            onChange={onChange}
            onClear={onClear}
            value={searchValue}
          />
          <CheckboxField
            label="Use Fire MAR filter"
            checked={useFireMarFilter}
            onChange={handleCheckboxChange}
          />
          {useFireMarFilter && (
            <SliderField
              label="Fire MAR scale (1-6): "
              min={1}
              max={6}
              step={1}
              onChange={(value) => handleSliderChange(value)}
              value={fireMarValue}
            />
          )}
        </Flex>
        {(countiesCardsFiltered !== 0 || searchValue.length !== 0) && <CountyPreviewBoxGrid
          SearchTerms={generateSearchValueList(searchValue)}
          items={countiesCardsFiltered}
        />}
        {(countiesCardsFiltered === 0 && searchValue.length === 0) && <Loader size="large"/>}
      </Flex>
    </Flex>
  );
}
