import React from "react";
import { useEffect, useState } from "react";
import {
    HomePage,
} from '../ui-components-editable';
import WildfirePreviewBox from "../cards/WildfirePreviewBox";
import backgroundImage from "../assets/home-bg.jpg"
import { useNavigate } from "react-router-dom";
import { fireParser } from "../functions/parsers";

function HomePageExtend(props) {
    document.title = props.name;
    const apiurl = 'https://api.californiawildfires.me/recentWildfire'
    const navigate = useNavigate();
    const [fire, setFire] = useState({});

    const fetchLastWildFiredata = async () => {
        await fetch(apiurl)
        .then(response => {
            return response.json()
        })
        .then((data) => {
          if (data) {
            setFire(fireParser(data[0]));
          }
        })
        .catch((err) => {
          console.error(err);
        }); 
    }

    useEffect(() => {
        fetchLastWildFiredata();
    }, []);

    let previewBox;
    if (Object.keys(fire).length !== 0) {
        // console.log(fire)
        previewBox = (
            <WildfirePreviewBox 
            key={fire.id}
            WildfireName={fire.WildfireName}
            CoordinatesLat={fire.CoordinatesLat}
            CoordinatesLon={fire.CoordinatesLon}
            DateDiscovered={fire.DateDiscovered}
            EstimatedCost={fire.EstimatedCost}
            Size={fire.Size}
            Cause={fire.Cause}
            Dispatch={fire.Dispatch}
            ExternalUrl={fire.img_url}
            LearnMoreButtonOnClick={()=>navigate(`wildfires/${fire.WildfireName}`)}
            />);
    } else {
        previewBox = (
            <WildfirePreviewBox
            LearnMoreButtonOnClick={()=>navigate(``)}
            />)
    }

    return (
        <HomePage
        wildfirePreviewBox={previewBox}
        overrides = {
            {
                'HomePage':{backgroundImage:`url(${backgroundImage})`},
                'WebsiteName':{children:props.name}
            }
        }
        />
    )
    
}

export default HomePageExtend