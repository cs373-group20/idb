import { useEffect, useState } from "react";
import { 
    Flex, Text, SelectField, SearchField, CheckboxField, ScrollView, Loader } from "@aws-amplify/ui-react";
import WildfirePreviewBoxGrid from '../collections/WildfirePreviewBoxGrid'
import fireData from '../data/fireData.json'
import {fireParser} from '../functions/parsers'
import { useNavigate } from 'react-router-dom'
import {fullWordFilter, multiWordFilter, singleWordFilter, generateSearchValueList} from '../functions/filters'

const url = "https://api.californiawildfires.me/allWildfires";
let countyNames = await getCountyListOfNames();

async function getCountyListOfNames() {
    const countyURL = "https://api.californiawildfires.me/allCounties?order=name";
    const counties = await fetch(countyURL)
        .then((response) => response.json())
        .catch((err) => console.log(err));
    let names = []
    counties.forEach(elem => {
        names.push(elem["name"]);
    });
    return names;
}

export function EpochToDateTime(epoch) {
    var d = new Date();
    d.setUTCSeconds(epoch / 100000);
    return (d.toDateString());
}

export function testloader() {
    return fireData;
}

export async function loader() {
    const fires = await fetch(url)
        .then((response) => response.json())
        .catch((err) => {
            console.log(err.message);
            return fireData;
        });
    return fires;
}

export function getFireData() {
    return fireData;
}

export default function WildfireModelPage() {
    document.title = "Wildfires";
    const [sortBy, setSortBy] = useState('?order=name');
    const navigate = useNavigate();
    const [firesCards, setFiresCards] = useState([]);
    const [fires, setFires] = useState([]);

    const [countyCheckboxValues, setCountyCheckboxValues] = useState([]);

    useEffect(() => {
        const initialCheckboxValues = {};
        countyNames.forEach(element => {
            initialCheckboxValues[element] = true;
        });
        setCountyCheckboxValues(initialCheckboxValues);
    }, [])

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(url + sortBy);
                const data = await response.json();
                return data;
            } catch (error) {
                console.error("Error fetching data:", error.message);
                return fireData;
            }
        };

        fetchData().then(firesData => {

            setFires(firesData.filter(fire => {
                return countyCheckboxValues[fire.county];
            }));
            setFiresCards([]);
        });

    }, [sortBy, countyCheckboxValues]);

    const handleCheckboxChange = (event) => {
        const { name, checked } = event.target;
        setCountyCheckboxValues(prevValues => ({
            ...prevValues,
            [name]: checked,
        }));
    };


    useEffect(() => {
        if (fires) {
            const updatedFiresCards = [];

            for (let i = 0; i < fires.length; i++) {
                const fire = fireParser(fires[i]);
                fire['LearnMoreButtonOnClick'] = () => navigate(fire.id);
                updatedFiresCards.push(fire);
            }
            setFiresCards([...updatedFiresCards]);
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [fires]);

    const [searchValue, setSearchValue] = useState("");
    const [firesCardsFiltered, setFiresCardsFiltered] = useState([]);

    const onChange = async function (event) {
        await setSearchValue(event.target.value);
    };

    const onClear = () => {
        setSearchValue("");
        setFiresCardsFiltered(firesCards);
    };

    useEffect(() => {
        if (searchValue.length === 0) {
            setFiresCardsFiltered(firesCards);
        } else {
            let searchValueList = searchValue.split(" ").filter((word) => word.length !== 0);
            if (searchValueList.length > 1) {
                console.log("mutli word search")
                let fullMatchFiltered = fullWordFilter(firesCards, searchValue);
                let firesCardsFiltered = firesCards.filter((card) => !fullMatchFiltered.includes(card));
                let multiMatchFiltered = multiWordFilter(firesCardsFiltered, searchValueList);
                firesCardsFiltered = firesCardsFiltered.filter((card) => !multiMatchFiltered.includes(card));
                let singleMatchFiltered = singleWordFilter(firesCardsFiltered, searchValueList);
                setFiresCardsFiltered(fullMatchFiltered.concat(multiMatchFiltered, singleMatchFiltered));
            } else if (searchValueList.length > 0) {
                let fullMatchFiltered = fullWordFilter(firesCards, searchValue);
                let firesCardsFiltered = firesCards.filter((card) => !fullMatchFiltered.includes(card));
                let singleMatchFiltered = singleWordFilter(firesCardsFiltered, searchValueList);
                setFiresCardsFiltered(fullMatchFiltered.concat(singleMatchFiltered));
            }
        }

    }, [searchValue, firesCards]);

    const countiesCheckBoxes = countyNames.map((element) => (
        <CheckboxField
            paddingLeft="15px"
            key={element}
            label={element}
            name={element}
            checked={countyCheckboxValues[element] || false}
            onChange={handleCheckboxChange}
        />
    ));

    return (
        <Flex
            direction="column"
            justifyContent="flex-start"
            alignItems="center">
            <Flex
                gap="40px"
                direction="column"
                width="70%"
                height="unset"
                justifyContent="flex-start"
                alignItems="center"
                overflow="hidden"
                position="relative"
                wrap="nowrap"
                padding="100px 0px 200px 0px"
                backgroundColor="rgba(255,255,255,1)"
            >
                <Text fontSize={30}>All the Wildfires in California</Text>
                <Text fontSize={15}>Number of elements: {firesCards.length}</Text>
                <Flex
                    direction="column"
                    wrap="wrap"
                    alignItems="bottom"
                    gap="20px"
                >
                    <SelectField
                        width="unset"
                        maxWidth="100%"
                        label="Sort By"
                        onChange={(e) => setSortBy(e.target.value)}
                    >
                        <option value="?order=name">Name (A to Z)</option>
                        <option value="?orderd=name">Name (Z to A)</option>
                        <option value="?order=discover_date">DateDiscovered (Old to New)</option>
                        <option value="?orderd=discover_date">DateDiscovered (New to Old)</option>
                        <option value="?order=cost_to_date">EstimatedCost (Low to High)</option>
                        <option value="?orderd=cost_to_date">EstimatedCost (High to Low)</option>
                        <option value="?order=size">Size (Small to Big)</option>
                        <option value="?orderd=size">Size (Big to Small)</option>
                    </SelectField>
                    <SearchField
                        label="search"
                        placeholder="Search..."
                        onChange={onChange}
                        onClear={onClear}
                        value={searchValue}
                    />
                    <Flex
                        gap="0px"
                        direction="column"
                        border="1px solid black"
                        borderRadius="medium"
                        boxShadow="medium"
                        padding="5px 5px 0px 5px"
                    >
                        <Text
                            padding="5px"
                        >View wildfires in:</Text>
                        <ScrollView
                            height="150px"
                            width="300px"
                            maxWidth="100%"
                            borderRadius="medium"
                            backgroundColor="#f0f0f0"
                        >
                            {countiesCheckBoxes}
                        </ScrollView>
                    </Flex>
                </Flex>
                {(firesCardsFiltered.length !== 0 || searchValue.length !== 0) && <WildfirePreviewBoxGrid
                    SearchTerms={generateSearchValueList(searchValue)}
                    isSearchable={false}
                    items={firesCardsFiltered}
                />}
                {(firesCardsFiltered.length === 0 && searchValue.length === 0) && <Loader size="large" />}
            </Flex>
        </Flex>
    )
}
