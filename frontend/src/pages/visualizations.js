import React from "react";
import { Flex, Text } from "@aws-amplify/ui-react";
import Visualization1 from "../visualizations/visualization1"
import Visualization2 from "../visualizations/visualization2"
import Visualization3 from "../visualizations/visualization3"
import Visualization4 from "../visualizations/visualization4"
import Visualization5 from "../visualizations/visualization5"
import Visualization6 from "../visualizations/visualization6"

export default function Visualization(props){
    return(
        <Flex
        direction="column"
        justifyContent="flex-start"
        alignItems="center">
        <Flex
        gap="40px"
        direction="column"
        width="70%"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        overflow="hidden"
        position="relative"
        wrap="nowrap"
        padding="100px 0px 200px 0px"
        backgroundColor="rgba(255,255,255,1)"
        >
        <Text fontSize={40}>Our Visualizations</Text>
        <Text fontSize={20}>Number of Wildfires vs Number of Facilities per County</Text>
        <Visualization1 /><br />
        <Text fontSize={20}>Number of Wildfires per Week</Text>
        <Visualization2 /><br />
        <Text fontSize={20}>The Distribution of the Different Types of Facilities</Text>
        <Flex width="50%"><Visualization3 /></Flex>
        <br /><br />
        <Text fontSize={40}>Developer Visualizations</Text>
        <Text fontSize={20}>Average Deaths by Hurricane Category</Text>
        <Visualization4 /><br />
        <Text fontSize={20}>Lowest Pressure Recorded vs Highest Wind Speed Recorded</Text>
        <Visualization5 /><br />
        <Text fontSize={20}>Number of Hurricane Aid Organizations vs Number of Hurricanes per County</Text>
        <Visualization6 />
        </Flex>
        </Flex>
    )
}