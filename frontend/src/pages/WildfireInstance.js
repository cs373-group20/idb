import * as React from "react";
import { Flex, Text } from "@aws-amplify/ui-react";
import CountyPreviewBoxGrid from '../collections/CountyPreviewBoxGrid'
import FPAPreviewBoxGrid from '../collections/FPAPreviewBoxGrid'
import WildfirePage from '../pagetemplates/WildfirePage'
import EmbeddedMapComponent from './EmbeddedMap'
import { useState, useEffect } from 'react';
import {
    useNavigate, 
    useLoaderData} from 'react-router-dom'
import {fpaParser, countyParser, fireParser} from '../functions/parsers'

const baseurl = "https://api.californiawildfires.me/wildfire?name="

export async function loader({params}){
    const url = baseurl + params.wildfirename
    const fire = await fetch(url)
           .then((response) => {
                return(response.json())})
           .catch((err) => {
              console.log(err.message);
           });

    let county_parse = [];
    try {
        county_parse = fire.nearby_counties.map((elem)=>{
            return countyParser(elem);
        });
    } catch {}

    let fpa_parse = []
    try {
        fpa_parse =  fire.nearby_facilities.map((elem)=>{
            return fpaParser(elem);
        });
    } catch {}
    return {
    name:params.wildfirename,
    data:fire,
    county:county_parse,
    fpa:fpa_parse,
    }
}

export async function loader_norecurse({params}){
    const url = baseurl + params.wildfirename
    const fire = await fetch(url)
           .then((response) => {
                return(response.json())})
           .catch((err) => {
              console.log(err.message);
           });
    return {
        name:params.wildfirename,
        data:fire,
    }
}

function WildfireInstance() {
    const navigate = useNavigate();
    const fireNameData = useLoaderData();
    console.log(fireNameData);
    const fire = fireParser(fireNameData.data);
    // console.log(fireNameData.county);
    const fireName = fireNameData.name;
    document.title = fireName;
    const [countyCards, setCountyCards] = useState([]);
    const [fpaCards, setFpaCards] = useState([]);
    const elementsPerLoad = 5;
    const [currCountyI, setCurrCountyI] = useState(0);
    const [currFpaI, setCurrFpaI] = useState(0);
    
    useEffect(() => {
      if (fireNameData.county && currCountyI < fireNameData.county.length) {
        const updatedCountyCards = [];
    
        for (let i = currCountyI; i < currCountyI + elementsPerLoad && i < fireNameData.county.length; i++) {
          const county = fireNameData.county[i];
          county['LearnMoreButtonOnClick'] = () => navigate('/counties/' + county.CountyName);
          updatedCountyCards.push(county);
        }
    
        setCurrCountyI(currCountyI + elementsPerLoad);
        setCountyCards([...countyCards, ...updatedCountyCards]);
      }
        // eslint-disable-next-line
    }, [fireNameData.county, currCountyI, countyCards]);
    
    useEffect(() => {
      if (fireNameData.fpa && currFpaI < fireNameData.fpa.length) {
        const updatedFpaCards = [];
    
        for (let i = currFpaI; i < currFpaI + elementsPerLoad && i < fireNameData.fpa.length; i++) {
          const fpa = fireNameData.fpa[i];
          fpa['LearnMoreButtonOnClick'] = () => navigate('/facilities/' + fpa.FPAName);
          updatedFpaCards.push(fpa);
        }
    
        setCurrFpaI(currFpaI + elementsPerLoad);
        setFpaCards([...fpaCards, ...updatedFpaCards]);
      }
        // eslint-disable-next-line
    }, [fireNameData.fpa, currFpaI, fpaCards]);

    let mapEnabled = fire.CoordinatesLat ? true : 0;

    if (fireNameData.data === "Bad Request") {
        setTimeout(() => {
            window.history.go(-1);
        }, 3000)

        return (
            <Text fontSize={20}>
                Sorry, this is an invalid page.
                Redirecting you to the previous page...
            </Text>
        );
    }

    return(
        <div>
        <WildfirePage
            FireName={fire.WildfireName}
            County={fireNameData.data.county}
            Dispatch={fire.Dispatch}
            DateDiscovered={fire.DateDiscovered}
            CoordinatesLat={fire.CoordinatesLat}
            CoordinatesLon={fire.CoordinatesLon}
            Size={fire.Size}
            Cause={fire.Cause}
            Fuel={fire.primary_fuel}
            Cost={fire.Cost}
            Containment={fire.Containment}
            ExternalUrl={fire.img_url}
        />
        <EmbeddedMapComponent
            lat = {fire.CoordinatesLat ? fire.CoordinatesLat : 0}
            lon = {fire.CoordinatesLon ? fire.CoordinatesLon : 0}
            enabled = {mapEnabled}
            zoom = {8}
        />
        <Flex
        direction="column"
        justifyContent="flex-start"
        alignItems="center"
        gap="40px">
        <Flex
        direction="column"
        width="70%"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        overflow="hidden"
        position="relative"
        wrap="nowrap"
        padding="100px 10px 200px 10px"
        backgroundColor="rgba(255,255,255,1)"
        >
        <Text
        fontFamily="Inter"
        fontSize="36px"
        fontWeight="300"
        color="rgba(0,0,0,1)"
        lineHeight="60px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="860px"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="Counties within 100 mile radius:"
        ></Text>
        <Text fontSize={15}>Number of counties: {countyCards.length}</Text>
        <CountyPreviewBoxGrid 
            items = {countyCards}
            overrides = {{'CountyPreviewBoxGrid':{isPaginated:false}}}
        >
        </CountyPreviewBoxGrid>
        </Flex>
        <Flex
        direction="column"
        width="70%"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        overflow="hidden"
        position="relative"
        wrap="nowrap"
        padding="100px 10px 200px 10px"
        backgroundColor="rgba(255,255,255,1)"
        >
        <Text
        fontFamily="Inter"
        fontSize="36px"
        fontWeight="300"
        color="rgba(0,0,0,1)"
        lineHeight="60px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="860px"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="Facilities within 25 mile radius:"
        ></Text>
        <Text fontSize={15}>Number of Facilities: {fpaCards.length}</Text>
        <FPAPreviewBoxGrid 
            items = {fpaCards}
            overrides = {{'FPAPreviewBoxGrid':{isPaginated:false}}}
        >
        </FPAPreviewBoxGrid>
        </Flex>
        </Flex>
        </div>
        )

}

export default WildfireInstance