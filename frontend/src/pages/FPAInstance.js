import * as React from "react";
import { Flex, Text } from "@aws-amplify/ui-react";
import WildfirePreviewBoxGrid from '../collections/WildfirePreviewBoxGrid'
import CountyPreviewBoxGrid from '../collections/CountyPreviewBoxGrid'
import FPAPage from '../pagetemplates/FPAPage'
import {fireParser, countyParser} from '../functions/parsers'
import { useState, useEffect } from 'react';
import EmbeddedMapComponent from './EmbeddedMap'
import {
    useNavigate, 
    useLoaderData} from 'react-router-dom'

const baseurl = "https://api.californiawildfires.me/facility?name="

export async function loader({params}){
    const url = baseurl + params.FPAname
    const fpa = await fetch(url)
            .then((response) => {
                return(response.json())})
            .catch((err) => {
                console.log(err.message);
            });
    
    let wildfire_parse = [];
    try {
        wildfire_parse = fpa.nearby_fires.map((wildfire_elem)=>{
            return fireParser(wildfire_elem);
        });
    } catch {}

    let county_parse = []
    try {
        county_parse = fpa.nearby_counties.map((county_elem)=>{
            return countyParser(county_elem['data']);
        });
    } catch {}

    return {
    name:params.countyname,
    data:fpa,
    wildfire:wildfire_parse,
    county:county_parse,
    }
}

export async function loader_norecurse({params}){
    const url = baseurl + params.FPAname
    console.log(url);
    const fpas = await fetch(url)
            .then((response) => {
                // console.log(response)
                return(response.json())})
            .catch((err) => {
                console.log(err.message);
            });
    return {
    name:params.FPAname,
    data:fpas,
    }
}

function FPAInstance() {
    const navigate = useNavigate();
    const FPANameData = useLoaderData();
    const FPA = FPANameData.data;
    const FPAName = FPANameData.name;
    document.title = FPAName;
    const [fireCards, setFireCards] = useState([]);
    const [countyCards, setCountyCards] = useState([]);
    const elementsPerLoad = 5;
    const [currFireI, setCurrFireI] = useState(0);
    const [currCountyI, setCurrCountyI] = useState(0);

    useEffect(() => {
    if (FPANameData.wildfire && currFireI < FPANameData.wildfire.length) {
        const updatedFireCards = [];

        for (let i = currFireI; i < currFireI + elementsPerLoad && i < FPANameData.wildfire.length; i++) {
            const fire = FPANameData.wildfire[i];
            fire['LearnMoreButtonOnClick'] = () => navigate('/wildfires/' + fire.WildfireName);
            updatedFireCards.push(fire);
        }

        setCurrFireI(currFireI + elementsPerLoad);
        setFireCards([...fireCards, ...updatedFireCards]);
    }
        // eslint-disable-next-line
    }, [FPANameData.wildfire, currFireI, fireCards]);

    useEffect(() => {
        if (FPANameData.county && currCountyI < FPANameData.county.length) {
            const updatedCountyCards = [];

            for (let i = currCountyI; i < currCountyI + elementsPerLoad && i < FPANameData.county.length; i++) {
                const countyCard = FPANameData.county[i];
                countyCard['LearnMoreButtonOnClick'] = () => navigate('/counties/' + countyCard.CountyName);
                updatedCountyCards.push(countyCard);
            }

            setCurrCountyI(currCountyI + elementsPerLoad);
            setCountyCards([...countyCards, ...updatedCountyCards]);
        }
        // eslint-disable-next-line
    }, [FPANameData.county, currCountyI, countyCards]);

    if (FPANameData.data === "Bad Request") {
        setTimeout(() => {
            window.history.go(-1);
        }, 3000)

        return (
            <Text fontSize={20}>
                Sorry, this is an invalid page.
                Redirecting you to the previous page...
            </Text>
        );
    }

    return(
        <div>
        <FPAPage
            FacilityName={FPA.name}
            County={FPA.county}
            Status={FPA.facility_status}
            Type={FPA.type}
            Scope={FPA.owner}
            Address={FPA.address}
            PhoneNumber={FPA.phone_num}
            CoordinatesLat={FPA.lat}
            CoordinatesLon={FPA.lon}
            ExternalUrl={FPA.img_url}
        />
        <EmbeddedMapComponent
            lat = {FPA.lat}
            lon = {FPA.lon}
            enabled = {FPA.lat ? true : false}
            zoom = {14}
        />
        <Flex
        direction="column"
        justifyContent="flex-start"
        alignItems="center"
        gap="40px">
        <Flex
        direction="column"
        width="70%"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        overflow="hidden"
        position="relative"
        wrap="nowrap"
        padding="100px 10px 200px 10px"
        backgroundColor="rgba(255,255,255,1)"
        >
        <Text
        fontFamily="Inter"
        fontSize="36px"
        fontWeight="300"
        color="rgba(0,0,0,1)"
        lineHeight="60px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="860px"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="Wildfires within 100 mile radius:"
        ></Text>
        <Text fontSize={15}>Number of Wildfires: {fireCards.length}</Text>
        <WildfirePreviewBoxGrid 
            items = {fireCards}
            overrides = {{'WildfirePreviewBoxGrid':{isPaginated:false}}}
        >
        </WildfirePreviewBoxGrid>
        </Flex>
        <Flex
        direction="column"
        width="70%"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        overflow="hidden"
        position="relative"
        wrap="nowrap"
        padding="100px 10px 200px 10px"
        backgroundColor="rgba(255,255,255,1)"
        >
        <Text
        fontFamily="Inter"
        fontSize="36px"
        fontWeight="300"
        color="rgba(0,0,0,1)"
        lineHeight="60px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="860px"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="Counties within 100 mile radius:"
        ></Text>
        <Text fontSize={15}>Number of Counties: {countyCards.length}</Text>
        <CountyPreviewBoxGrid 
            items = {countyCards}
            overrides = {{'CountyPreviewBoxGrid':{isPaginated:false}}}
        >
        </CountyPreviewBoxGrid>
        </Flex>
        </Flex>
        </div>
    )
}

export default FPAInstance