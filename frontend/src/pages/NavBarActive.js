import React from 'react';
import { useState, useEffect } from 'react';
import NavBar from '../ui-components-editable/NavBarEditable'
import { 
    useNavigate 
  } from "react-router-dom";

function NavBarActive() {
    const navigate = useNavigate();
    
    const [isMobile, setIsMobile] = useState(false);
    useEffect(() => {
      const handleResize = () => {
        setIsMobile(window.innerWidth <= 800); // TODO: Verify width
      };
      handleResize();
      window.addEventListener('resize', handleResize);
  
      return () => {
        window.removeEventListener('resize', handleResize);
      };
    }, []);

    return (
        <NavBar
            isMobile={isMobile}
            overrides = {
                {
                    'Home':
                    {   
                        onClick :()=>{
                            navigate('/')
                        }
                        
                    },
                    'About':
                    {   
                        onClick :()=>{
                            navigate('/about')
                        }
                        
                    },
                    'Wildfires':
                    {   
                        onClick :()=>{
                            navigate('/wildfires')
                        }
                        
                    },
                    'Counties':
                    {   
                        onClick :()=>{
                            navigate('/counties')
                        }
                        
                    },
                    'Fire Protection Facilites':
                    {   
                        onClick :()=>{
                            navigate('/facilities')
                        }
                        
                    },
                    'SearchField':
                    {
                        onSubmit :(value)=>{
                            if (value) {
                                navigate('/search/'+value)
                            }
                        }
                    }

                }  
            }
        />
    )
}

export default NavBarActive