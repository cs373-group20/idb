import React, { Component } from "react";
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
import NoCoordinatesImage from "../assets/no_coordinates.png"

function EmbeddedMapComponent(props){
    var style_inner = {
        width:"50%",
        aspectRatio:"1.5",
    }
    if(props.inner){
        style_inner.width = props.inner.width
        style_inner.height = props.inner.height
        style_inner.aspectRatio = props.inner.aspectRatio
    }

    var style_outer = {
        width:"50%",
        aspectRatio:"1.5",
        margin:"auto",
    }
    if(props.outer){
        style_outer.width = props.outer.width
        style_outer.height = props.outer.height
        style_outer.aspectRatio = props.outer.aspectRatio
    }
    const defaultMapProps = {
        google:props.google,
        style:style_inner,
        zoom:props.zoom,
        initialCenter:{
        lat: props.lat,
        lng: props.lon
        }
    }
    const mapProps = {
        ...defaultMapProps,
        ...props.googleProps,
    }
    
    console.log(props.enabled)
    if(props.enabled === true){
        return(
            <div style={style_outer}
            >
                <Map 
                {...mapProps}
                >
                <Marker />
                </Map>
            </div>
            
        )
    }
    if(props.enabled === false){
        return(
            <div stype={style_outer}>
                <img 
                src={NoCoordinatesImage}
                alt="No coordinates"
                />
            </div>
        )
    }
}

export default GoogleApiWrapper({
    apiKey: "AIzaSyAdHnJpA-ixS8bZhGoKIkI-BiK8XIiyvhU"
})(EmbeddedMapComponent)
