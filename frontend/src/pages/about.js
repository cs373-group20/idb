import React, { useEffect, useState } from "react";
import { teamInfo } from "../data/teamInfo";
import AboutPage from "../ui-components-editable/AboutPageEditable";
import { toolsInfo } from "../data/toolsInfo";
import { dataSources } from "../data/dataSources";

const gitLabAPI = "https://gitlab.com/api/v4/projects/50368680";
const gitLabURL = "https://gitlab.com/cs373-group20/idb";
const aboutDescription = "California is known for its catastrophic wildfires every year. Being the most populous state in the US, these fires affect a large number of citizens. Our website strives to bring attention to the fires in California and to provide information on wildland fire protection facilities in the region for those affected by these fires.";
const postmanURL = "https://documenter.getpostman.com/view/29941436/2s9YJZ34Ee";

function AboutPageExtend() {
    const [teamCommits, setTeamCommits] = useState(0);
    const [teamIssues, setTeamIssues] = useState(0);
    let teamTests = 0;
    teamInfo.forEach(member => {
        teamTests += member.unittests;
    })
    
    const fetchGitLabTeamCommits = async () => {
        await fetch(gitLabAPI + "/repository/contributors")
          .then(response => {
            setTeamCommits(0);
            teamInfo.forEach(member => {
                member.commits = 0;
            })
            return response.json()
          })
          .then(data => {
            data.forEach(element => {
                teamInfo.forEach(member => {
                    member.gitlabNames.forEach(name => {
                        if (name === element.name){
                            member.commits += element.commits;
                        }
                    })
                })
            });;
          });

        let totalCommits = 0;
        teamInfo.forEach(member => {
            totalCommits += member.commits;
        })
        setTeamCommits(totalCommits);
    }

    const fetchGitLabIssues = async () => {
        setTeamIssues(0);
        teamInfo.forEach(async member => {
            await fetch (gitLabAPI + "/issues_statistics?author_id=" + member.gitlabID)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    member.issues = data.statistics.counts.all;
                });
        });
        await fetch(gitLabAPI + "/issues_statistics?scope=all")
            .then(response => {
                return response.json();
            })
            .then(data => {
                setTeamIssues(data.statistics.counts.all);
            })
    }

    useEffect(() => {
        fetchGitLabTeamCommits();
        fetchGitLabIssues();
        // eslint-disable-next-line
    }, [])

    let PageInfoOverrides = {
        About:{
            children:   "California Wildfires"
        },

        Description: {
            children:   aboutDescription,
        }
    };

    let DevCardOverrides = {};
    for (let index = 0; index < teamInfo.length; index++){
        const element = teamInfo[index];
        let DevCardI = "DevCard" + (index + 1);
        DevCardOverrides[DevCardI] = 
            {
                name:       element.name, 
                bio:        element.bio, 
                numCommits: element.commits, 
                numIssues:  element.issues,
                numTests:   element.unittests, 
                image:      element.photo, 
                role:       element.role 
            }
    }

    let RepoCardOverrides = {
        RepoCard: {
        numCommits: teamCommits,
        numIssues: teamIssues,
        numTests: teamTests,
        repoLinkURL: gitLabURL,
        repoDocuURL: postmanURL
        }
    };

    let ToolsInfoOverrides = {};
    for (let index = 0; index < toolsInfo.length; index++) {
        const element = toolsInfo[index];
        let toolCardI = "ToolCard" + (index + 1);
        ToolsInfoOverrides[toolCardI] = 
            {
                toolName: element.name,
                toolImg: element.img,
                toolBio: element.bio,
                toolURL: element.url
            };
    }

    const overrides = Object.assign(
        PageInfoOverrides, 
        DevCardOverrides, 
        RepoCardOverrides, 
        ToolsInfoOverrides
    );
    document.title = "About";

    return (
        <AboutPage
        overrides={overrides}
        counties1={dataSources.counties.a}
        counties2={dataSources.counties.b}
        counties3={dataSources.counties.c}
        wildfires1={dataSources.wildfires.a}
        facilities1={dataSources.facilities.a}
        />
    )
}

export default AboutPageExtend