import React from "react";
import { useEffect, useState } from "react";
import { Flex, Text, SelectField, SearchField } from "@aws-amplify/ui-react";
import {countyParser, fireParser, fpaParser} from '../functions/parsers'
import CountyPreviewBoxGrid from '../collections/CountyPreviewBoxGrid'
import WildfirePreviewBoxGrid from '../collections/WildfirePreviewBoxGrid'
import FPAPreviewBoxGrid from '../collections/FPAPreviewBoxGrid'
import {
    useNavigate, 
    useLoaderData} from 'react-router-dom'
import {fullWordFilter, multiWordFilter, singleWordFilter, generateSearchValueList} from '../functions/filters'

const url_counties = "https://api.californiawildfires.me/allCounties"
const url_wildfires = "https://api.californiawildfires.me/allWildfires"
const url_facilities = "https://api.californiawildfires.me/allFacilities"

export async function loader({params}){
    let result = new Object();
    result.searchValue = params.searchValue
    const fires = await fetch(url_wildfires)
           .then((response) => {
                console.log(response)
                result['fires'] = response.json()})
           .catch((err) => {
              console.log(err.message);
              result['fires'] = [];
           });
    const counties = await fetch(url_counties)
           .then((response) => {
                console.log(response)
                result['counties'] = response.json()})
           .catch((err) => {
              console.log(err.message);
              result['counties'] = [];
           });
    const fpa = await fetch(url_facilities)
           .then((response) => {
                console.log(response)
                result['facilities'] = response.json()})
           .catch((err) => {
              console.log(err.message);
              result['facilities'] = [];
           });
    return result;
}

export default function GlobalSearchPage(props){
    const navigate = useNavigate();
    const queryResult = useLoaderData();
    const [firesCards, setFiresCards] = useState([]);
    const elementsPerLoad = 1500;

    const [firesCurrI, setFiresCurrI] = useState(0);
    useEffect(() => {
      queryResult.fires.then(function(fires){
        if (fires && firesCurrI < fires.length) {
          const updatedFiresCards = [];
    
          for (let i = firesCurrI; i < firesCurrI + elementsPerLoad && i < fires.length; i++) {
            const fire = fireParser(fires[i]);
            fire['LearnMoreButtonOnClick'] = ()=>navigate('/wildfires/'+fire.id);
            updatedFiresCards.push(fire);
          }
    
          setFiresCurrI(firesCurrI + elementsPerLoad);
          setFiresCards([...firesCards, ...updatedFiresCards]);
        }
      }
      )

        // eslint-disable-next-line
    }, [queryResult, firesCurrI, firesCards]);
    const [countiesCards, setCountiesCards] = useState([]);
    const [countiesCurrI, setCountiesCurrI] = useState(0);
    useEffect(() => {
        queryResult.counties.then(function(counties){
            if (counties && countiesCurrI < counties.length) {
                const updatedCountiesCards = [];
            
                for (let i = countiesCurrI; i < countiesCurrI + elementsPerLoad && i < counties.length; i++) {
                const county = countyParser(counties[i]);
                county['LearnMoreButtonOnClick'] = () => navigate('/counties/'+county.CountyName);
                updatedCountiesCards.push(county);
                }
                
                setCountiesCurrI(countiesCurrI + elementsPerLoad);
                setCountiesCards([...countiesCards, ...updatedCountiesCards]);
                
            }
                // eslint-disable-next-line
            })}, [queryResult, countiesCurrI, countiesCards]);
    const [facilitiesCards, setFacilitiesCards] = useState([]);
    const [facilitiesCurrI, setFacilitiesCurrI] = useState(0);
    useEffect(() => {
      queryResult.facilities.then(function(facilities){
        if (facilities && facilitiesCurrI < facilities.length) {
          const updatedFacilitiesCards = [];
      
          for (let i = facilitiesCurrI; i < facilitiesCurrI + elementsPerLoad && i < facilities.length; i++) {
            const facility = fpaParser(facilities[i]);
            facility['LearnMoreButtonOnClick'] = () => navigate('/facilities/'+facility.id);
            updatedFacilitiesCards.push(facility);
          }
          
          setFacilitiesCurrI(facilitiesCurrI + elementsPerLoad);
          setFacilitiesCards([...facilitiesCards, ...updatedFacilitiesCards]);
          
        }
      }
      )
        // eslint-disable-next-line
    }, [queryResult, countiesCurrI, countiesCards]);
    
    const [searchValue, setSearchValue] = useState("");
    useEffect(() => {
      setSearchValue(queryResult.searchValue);
    }, [queryResult]);


    const [facilitiesCardsFiltered, setFacilitiesCardsFiltered] = useState([]);
    const [firesCardsFiltered, setFiresCardsFiltered] = useState([]);
    const [countiesCardsFiltered, setCountiesCardsFiltered] = useState([]);

    const onChange = async function(event){
        await setSearchValue(event.target.value);
    };
    const onClear = () => {
        setSearchValue([]);
    };
    useEffect(function() {
      if(searchValue.length == 0){
        setFiresCardsFiltered(firesCards);
        setCountiesCardsFiltered(countiesCards);
        setFacilitiesCardsFiltered(facilitiesCards);
        
      }
      else{
        let searchValueList = searchValue.split(" ").filter((word) => word.length != 0);
        if(searchValueList.length > 1){
          let facilitiesFullMatchFiltered = fullWordFilter(facilitiesCards, searchValue);
          let countiesFullMatchFiltered = fullWordFilter(countiesCards, searchValue);
          let firesFullMatchFiltered = fullWordFilter(firesCards, searchValue);

          let facilitiesCardsFiltered = facilitiesCards.filter((card) => ! facilitiesFullMatchFiltered.includes(card));
          let countiesCardsFiltered = countiesCards.filter((card) => ! countiesFullMatchFiltered.includes(card));
          let firesCardsFiltered = firesCards.filter((card) => ! firesFullMatchFiltered.includes(card));

          let facilitiesMultiMatchFiltered = multiWordFilter(facilitiesCardsFiltered, searchValueList);
          let countiesMultiMatchFiltered = multiWordFilter(countiesCardsFiltered, searchValueList);
          let firesMultiMatchFiltered = multiWordFilter(firesCardsFiltered, searchValueList);

          facilitiesCardsFiltered = facilitiesCardsFiltered.filter((card) => ! facilitiesMultiMatchFiltered.includes(card));
          countiesCardsFiltered = countiesCardsFiltered.filter((card) => ! countiesMultiMatchFiltered.includes(card));
          firesCardsFiltered = firesCardsFiltered.filter((card) => ! firesMultiMatchFiltered.includes(card));

          let facilitiesSingleMatchFiltered = singleWordFilter(facilitiesCardsFiltered, searchValueList);
          let countiesSingleMatchFiltered = singleWordFilter(countiesCardsFiltered, searchValueList);
          let firesSingleMatchFiltered = singleWordFilter(firesCardsFiltered, searchValueList);

          setFacilitiesCardsFiltered(facilitiesFullMatchFiltered.concat(facilitiesMultiMatchFiltered, facilitiesSingleMatchFiltered));
          setCountiesCardsFiltered(countiesFullMatchFiltered.concat(countiesMultiMatchFiltered, countiesSingleMatchFiltered));
          setFiresCardsFiltered(firesFullMatchFiltered.concat(firesMultiMatchFiltered, firesSingleMatchFiltered));
        }
        else if(searchValueList.length > 0){
          let facilitiesFullMatchFiltered = fullWordFilter(facilitiesCards, searchValue);
          let countiesFullMatchFiltered = fullWordFilter(countiesCards, searchValue);
          let firesFullMatchFiltered = fullWordFilter(firesCards, searchValue);

          let facilitiesCardsFiltered = facilitiesCards.filter((card) => ! facilitiesFullMatchFiltered.includes(card));
          let countiesCardsFiltered = countiesCards.filter((card) => ! countiesFullMatchFiltered.includes(card));
          let firesCardsFiltered = firesCards.filter((card) => ! firesFullMatchFiltered.includes(card));

          let facilitiesSingleMatchFiltered = singleWordFilter(facilitiesCardsFiltered, searchValueList);
          let countiesSingleMatchFiltered = singleWordFilter(countiesCardsFiltered, searchValueList);
          let firesSingleMatchFiltered = singleWordFilter(firesCardsFiltered, searchValueList);

          setFacilitiesCardsFiltered(facilitiesFullMatchFiltered.concat(facilitiesSingleMatchFiltered));
          setCountiesCardsFiltered(countiesFullMatchFiltered.concat(countiesSingleMatchFiltered));
          setFiresCardsFiltered(firesFullMatchFiltered.concat(firesSingleMatchFiltered));
        }
      }
    }, [searchValue, countiesCards, firesCards, facilitiesCards]);

    return(
        <Flex
        direction="column"
        justifyContent="flex-start"
        alignItems="center">
        <Flex
        gap="40px"
        direction="column"
        width="70%"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        overflow="hidden"
        position="relative"
        wrap="nowrap"
        padding="100px 10px 200px 10px"
        backgroundColor="rgba(255,255,255,1)"
        >
          <Text fontSize={30}>
        Results from Wildfires   
        </Text>
        <WildfirePreviewBoxGrid 
            SearchTerms = {generateSearchValueList(searchValue)}
            items = {firesCardsFiltered}>
        </WildfirePreviewBoxGrid>
        <Text fontSize={30}>
        Results from Counties   
        </Text>
        <CountyPreviewBoxGrid 
            SearchTerms = {generateSearchValueList(searchValue)}
            items = {countiesCardsFiltered}>
        </CountyPreviewBoxGrid>
        <Text fontSize={30}>
        Results from Facilities  
        </Text>
        <FPAPreviewBoxGrid 
            SearchTerms = {generateSearchValueList(searchValue)}
            items = {facilitiesCardsFiltered}>
        </FPAPreviewBoxGrid>
        </Flex>
        </Flex>
    )
}