import React from "react";
import {
    ModelPage,
} from '../ui-components-editable';
import { 
    useNavigate 
  } from "react-router-dom";

function ModelPageExtend(props){
    var navigate = useNavigate()
    document.title = props.name;
    return <ModelPage
        overrides = {
            {
                'Name':{
                    children:props.name
                    },
                'Description':{
                    children:props.description
                    },
                'Image':{
                    src:props.imageurl
                },
                'Relation1':{
                    children:props.relation1name
                },
                'Relation1PreviewBox1':{
                    hidden:!props.relation1box1show, 
                    boxname: props.relation1box1name,
                    boxcontent: props.relation1box1content,
                    imageurl: props.relation1box1image,
                    onClick: ()=>{navigate(props.relation1box1link)}
                },
                'Relation1PreviewBox2':{
                    hidden:!props.relation1box2show, 
                    boxname: props.relation1box2name,
                    boxcontent: props.relation1box2content,
                    imageurl: props.relation1box2image,
                    onClick: ()=>{navigate(props.relation1box2link)}
                },
                'Relation1PreviewBox3':{
                    hidden:!props.relation1box3show, 
                    boxname: props.relation1box3name,
                    boxcontent: props.relation1box3content,
                    imageurl: props.relation1box3image,
                    onClick: ()=>{navigate(props.relation1box3link)}
                },
                'Relation2':{
                    children:props.relation2name
                },
                'Relation2PreviewBox1':{
                    hidden:!props.relation2box1show, 
                    boxname: props.relation2box1name,
                    boxcontent: props.relation2box1content,
                    imageurl: props.relation2box1image,
                    onClick: ()=>{navigate(props.relation2box1link)}
                },
                'Relation2PreviewBox2':{
                    hidden:!props.relation2box2show, 
                    boxname: props.relation2box2name,
                    boxcontent: props.relation2box2content,
                    imageurl: props.relation2box2image,
                    onClick: ()=>{navigate(props.relation2box2link)}
                },
                'Relation2PreviewBox3':{
                    hidden:!props.relation2box3show, 
                    boxname: props.relation2box3name,
                    boxcontent: props.relation2box3content,
                    imageurl: props.relation2box3image,
                    onClick: ()=>{navigate(props.relation2box3link)}
                }
            }
        }
    />
}
export default ModelPageExtend