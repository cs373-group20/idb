import * as React from "react";
import { Flex, Text } from "@aws-amplify/ui-react";
import WildfirePreviewBoxGrid from '../collections/WildfirePreviewBoxGrid'
import FPAPreviewBoxGrid from '../collections/FPAPreviewBoxGrid'
import CountyPage from '../pagetemplates/CountyPage'
import {fpaParser, fireParser} from '../functions/parsers'
import { useState, useEffect } from 'react';
import EmbeddedMapComponent from './EmbeddedMap'
import {
    useNavigate, 
    useLoaderData} from 'react-router-dom'

const baseurl = "https://api.californiawildfires.me/county?name="

export async function loader({params}){
    const url = baseurl + params.countyname
    const county = await fetch(url)
            .then((response) => {
                return(response.json())})
            .catch((err) => {
                console.log(err.message);
            });

    let wildfire_parse = []
    try {
        wildfire_parse = county.nearby_fires.map((wildfire_elem)=>{
            return fireParser(wildfire_elem);
        });
    } catch {}

    let fpa_parse = []
    try {
        fpa_parse =  county.nearby_facilities.map((fpa_elem)=>{
            return fpaParser(fpa_elem);
        });
    } catch {}

    return {
    name:params.countyname,
    data:county,
    wildfire:wildfire_parse,
    fpa:fpa_parse,
    }
}

export async function loader_norecurse({params}){
    const url = baseurl + params.countyname
    const county = await fetch(url)
            .then((response) => {
                return(response.json())})
            .catch((err) => {
                console.log(err.message);
            });
    return {
    name:params.countyname,
    data:county,
    }
}

function CountyInstance() {
    const navigate = useNavigate();
    const countyNameData = useLoaderData();
    const county = countyNameData.data;
    const countyName = countyNameData.name;
    document.title = countyName;
    const [fireCards, setFireCards] = useState([]);
    const [fpaCards, setFpaCards] = useState([]);
    const elementsPerLoad = 5;
    const [currFireI, setCurrFireI] = useState(0);
    const [currFpaI, setCurrFpaI] = useState(0);
    
    useEffect(() => {
      if (countyNameData.wildfire && currFireI < countyNameData.wildfire.length) {
        const updatedFireCards = [];
        const endIndex = Math.min(currFireI + elementsPerLoad, countyNameData.wildfire.length);
    
        for (let i = currFireI; i < endIndex; i++) {
          const fire = countyNameData.wildfire[i];
          fire['LearnMoreButtonOnClick'] = () => navigate('/wildfires/' + fire.WildfireName);
          updatedFireCards.push(fire);
        }
    
        setCurrFireI(endIndex);
        setFireCards([...fireCards, ...updatedFireCards]);
      }
      // eslint-disable-next-line
    }, [countyNameData.wildfire, currFireI, fireCards]);
    
    useEffect(() => {
      if (countyNameData.fpa && currFpaI < countyNameData.fpa.length) {
        const updatedFpaCards = [];
        const endIndex = Math.min(currFpaI + elementsPerLoad, countyNameData.fpa.length);
    
        for (let i = currFpaI; i < endIndex; i++) {
          const fpa = countyNameData.fpa[i];
          fpa['LearnMoreButtonOnClick'] = () => navigate('/facilities/' + fpa.FPAName);
          updatedFpaCards.push(fpa);
        }
    
        setCurrFpaI(endIndex);
        setFpaCards([...fpaCards, ...updatedFpaCards]);
      }
        // eslint-disable-next-line
    }, [countyNameData.fpa, currFpaI, fpaCards]);

    if (countyNameData.data === "Bad Request") {
        setTimeout(() => {
            window.history.go(-1);
        }, 3000)

        return (
            <Text fontSize={20}>
                Sorry, this is an invalid page.
                Redirecting you to the previous page...
            </Text>
        );
    }

    return(
        <div>
        <CountyPage
            CountyName={county.name}
            Area={county.area}
            Population={county.pop}
            PopulationDensity={county.pop_density}
            Region={county.region}
            FireMAR={county.fireMAR}
            CoordinatesLat={county.lat}
            CoordinatesLon={county.lon}
            ExternalUrl={county.img_url}
        />
        <EmbeddedMapComponent
            lat = {county.lat}
            lon = {county.lon}
            enabled = {county.lat ? true : false}
            zoom = {8}
        />
        <Flex
        direction="column"
        justifyContent="flex-start"
        alignItems="center"
        gap="40px">
        <Flex
        direction="column"
        width="70%"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        overflow="hidden"
        position="relative"
        wrap="nowrap"
        padding="100px 10px 200px 10px"
        backgroundColor="rgba(255,255,255,1)"
        >
        <Text
        fontFamily="Inter"
        fontSize="36px"
        fontWeight="300"
        color="rgba(0,0,0,1)"
        lineHeight="60px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="860px"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="Wildfires within 100 mile radius:"
        ></Text>
        <Text fontSize={15}>Number of Wildfires: {fireCards.length}</Text>
        <WildfirePreviewBoxGrid 
            items = {fireCards}
            overrides = {{'WildfirePreviewBoxGrid':{isPaginated:false}}}
        >
        </WildfirePreviewBoxGrid>
        </Flex>
        <Flex
        direction="column"
        width="70%"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        overflow="hidden"
        position="relative"
        wrap="nowrap"
        padding="100px 10px 200px 10px"
        backgroundColor="rgba(255,255,255,1)"
        >
        <Text
        fontFamily="Inter"
        fontSize="36px"
        fontWeight="300"
        color="rgba(0,0,0,1)"
        lineHeight="60px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="860px"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="Facilities within 25 mile radius:"
        ></Text>
        <Text fontSize={15}>Number of Facilities: {fpaCards.length}</Text>
        <FPAPreviewBoxGrid 
            items = {fpaCards}
            overrides = {{'FPAPreviewBoxGrid':{isPaginated:false}}}
        >
        </FPAPreviewBoxGrid>
        </Flex>
        </Flex>
        </div>
    )
}

export default CountyInstance