import React from "react";
import {
    ModelListPage
} from '../ui-components-editable';
import { 
    useNavigate 
  } from "react-router-dom";


function ModelListPageExtend(props) {
    var navigate = useNavigate()
    document.title = props.name;
    return (
        <ModelListPage
        overrides = {
            {
                'ModelName':{children:props.name},
                'PreviewBox1':
                {
                    onClick: ()=>{navigate(props.link1)},
                    boxname: props.elem1name,
                    boxcontent: props.elem1content,
                    imageurl: props.elem1imageurl
                },
                'PreviewBox2':
                {
                    onClick: ()=>{navigate(props.link2)},
                    boxname: props.elem2name,
                    boxcontent: props.elem2content,
                    imageurl: props.elem2imageurl
                },
                'PreviewBox3':
                {
                    onClick: ()=>{navigate(props.link3)},
                    boxname: props.elem3name,
                    boxcontent: props.elem3content,
                    imageurl: props.elem3imageurl
                }
            }
        }
        />
    )
}

export {ModelListPageExtend}
