/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import { getOverrideProps } from "@aws-amplify/ui-react/internal";
import { Flex, Image, Text, View } from "@aws-amplify/ui-react";
import PreviewBox from "./PreviewBoxEditable";
export default function ModelPage(props) {
  const { overrides, ...rest } = props;
  return (
    <Flex
      width="100%"
      height="2300px"
      display="block"
      gap="unset"
      alignItems="unset"
      justifyContent="unset"
      overflow="hidden"
      position="relative"
      padding="0px 0px 0px 0px"
      backgroundColor="rgba(255,255,255,1)"
      {...getOverrideProps(overrides, "ModelPage")}
      {...rest}
    >
      <Flex
        gap="36px"
        direction="column"
        width="unset"
        height="951px"
        justifyContent="flex-start"
        alignItems="center"
        position="relative"
        top="88px"
        padding="0px 0px 0px 0px"
        {...getOverrideProps(overrides, "ContentFrame")}
      >
        <Text
          fontFamily="Inter"
          fontSize="48px"
          fontWeight="300"
          color="rgba(0,0,0,1)"
          lineHeight="60px"
          textAlign="center"
          display="block"
          direction="column"
          justifyContent="unset"
          width="860px"
          height="unset"
          gap="unset"
          alignItems="unset"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          whiteSpace="pre-wrap"
          children="Name"
          {...getOverrideProps(overrides, "Name")}
        ></Text>
        <Image
          width="unset"
          height="687px"
          display="block"
          gap="unset"
          alignItems="unset"
          justifyContent="unset"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          objectFit="cover"
          {...getOverrideProps(overrides, "Image")}
        ></Image>
        <Text
          fontFamily="Inter"
          fontSize="20px"
          fontWeight="400"
          color="rgba(0,0,0,1)"
          lineHeight="30px"
          textAlign="center"
          display="block"
          direction="column"
          justifyContent="unset"
          width="1294px"
          height="unset"
          gap="unset"
          alignItems="unset"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          whiteSpace="pre-wrap"
          children="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
          {...getOverrideProps(overrides, "Description")}
        ></Text>
        <Text
          fontFamily="Inter"
          fontSize="32px"
          fontWeight="500"
          color="rgba(0,0,0,1)"
          lineHeight="40px"
          textAlign="center"
          display="block"
          direction="column"
          justifyContent="unset"
          width="856px"
          height="unset"
          gap="unset"
          alignItems="unset"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          whiteSpace="pre-wrap"
          children="Relation1"
          {...getOverrideProps(overrides, "Relation1")}
        ></Text>
        <Flex
          gap="34px"
          direction="row"
          width="unset"
          height="unset"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          {...getOverrideProps(overrides, "Relation1Preview")}
        >
          <PreviewBox
            width="248px"
            height="400px"
            display="block"
            gap="unset"
            alignItems="unset"
            justifyContent="unset"
            overflow="hidden"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            backgroundColor="rgba(174,179,183,1)"
            {...getOverrideProps(overrides, "Relation1PreviewBox1")}
          ></PreviewBox>
          <PreviewBox
            width="248px"
            height="400px"
            display="block"
            gap="unset"
            alignItems="unset"
            justifyContent="unset"
            overflow="hidden"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            backgroundColor="rgba(174,179,183,1)"
            {...getOverrideProps(overrides, "Relation1PreviewBox2")}
          ></PreviewBox>
          <PreviewBox
            width="248px"
            height="400px"
            display="block"
            gap="unset"
            alignItems="unset"
            justifyContent="unset"
            overflow="hidden"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            backgroundColor="rgba(174,179,183,1)"
            {...getOverrideProps(overrides, "Relation1PreviewBox3")}
          ></PreviewBox>
        </Flex>
        <Text
          fontFamily="Inter"
          fontSize="32px"
          fontWeight="500"
          color="rgba(0,0,0,1)"
          lineHeight="40px"
          textAlign="center"
          display="block"
          direction="column"
          justifyContent="unset"
          width="856px"
          height="unset"
          gap="unset"
          alignItems="unset"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          whiteSpace="pre-wrap"
          children="Relation2"
          {...getOverrideProps(overrides, "Relation2")}
        ></Text>
        <Flex
          gap="34px"
          direction="row"
          width="unset"
          height="unset"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          {...getOverrideProps(overrides, "Relation2Preview")}
        >
          <PreviewBox
            width="248px"
            height="400px"
            display="block"
            gap="unset"
            alignItems="unset"
            justifyContent="unset"
            overflow="hidden"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            backgroundColor="rgba(174,179,183,1)"
            {...getOverrideProps(overrides, "Relation2PreviewBox1")}
          ></PreviewBox>
          <PreviewBox
            width="248px"
            height="400px"
            display="block"
            gap="unset"
            alignItems="unset"
            justifyContent="unset"
            overflow="hidden"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            backgroundColor="rgba(174,179,183,1)"
            {...getOverrideProps(overrides, "Relation2PreviewBox2")}
          ></PreviewBox>
          <PreviewBox
            width="248px"
            height="400px"
            display="block"
            gap="unset"
            alignItems="unset"
            justifyContent="unset"
            overflow="hidden"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            backgroundColor="rgba(174,179,183,1)"
            {...getOverrideProps(overrides, "Relation2PreviewBox3")}
          ></PreviewBox>
        </Flex>
      </Flex>
    </Flex>
  );
}
