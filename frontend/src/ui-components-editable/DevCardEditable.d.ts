/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { FlexProps, IconProps, ImageProps, TextProps } from "@aws-amplify/ui-react";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type DevCardOverridesProps = {
    DevCard?: PrimitiveOverrideProps<FlexProps>;
    Image?: PrimitiveOverrideProps<ImageProps>;
    Name?: PrimitiveOverrideProps<TextProps>;
    "Line 1"?: PrimitiveOverrideProps<IconProps>;
    "Frame 3"?: PrimitiveOverrideProps<FlexProps>;
    Bio?: PrimitiveOverrideProps<TextProps>;
    "Frame 2"?: PrimitiveOverrideProps<FlexProps>;
    Role?: PrimitiveOverrideProps<FlexProps>;
    label?: PrimitiveOverrideProps<TextProps>;
    Commits?: PrimitiveOverrideProps<FlexProps>;
    commits?: PrimitiveOverrideProps<TextProps>;
    numCommits?: PrimitiveOverrideProps<TextProps>;
    Issues?: PrimitiveOverrideProps<FlexProps>;
    issues?: PrimitiveOverrideProps<TextProps>;
    numIssues?: PrimitiveOverrideProps<TextProps>;
    "Unit Tests"?: PrimitiveOverrideProps<FlexProps>;
    unittests?: PrimitiveOverrideProps<TextProps>;
    numTests?: PrimitiveOverrideProps<TextProps>;
} & EscapeHatchProps;
export declare type DevCardProps = React.PropsWithChildren<Partial<FlexProps> & {
    name?: String;
    image?: String;
    numCommits?: Number;
    numIssues?: Number;
    numTests?: Number;
    bio?: String;
    role?: String;
} & {
    overrides?: DevCardOverridesProps | undefined | null;
}>;
export default function DevCard(props: DevCardProps): React.ReactElement;
