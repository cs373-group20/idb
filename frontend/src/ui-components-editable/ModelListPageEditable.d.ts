/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { FlexProps, TextProps, ViewProps } from "@aws-amplify/ui-react";
import { PreviewBoxProps } from "./PreviewBoxEditable";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type ModelListPageOverridesProps = {
    ModelListPage?: PrimitiveOverrideProps<ViewProps>;
    ModelName?: PrimitiveOverrideProps<TextProps>;
    PreviewGrid?: PrimitiveOverrideProps<FlexProps>;
    PreviewBox1?: PreviewBoxProps;
    PreviewBox2?: PreviewBoxProps;
    PreviewBox3?: PreviewBoxProps;
} & EscapeHatchProps;
export declare type ModelListPageProps = React.PropsWithChildren<Partial<ViewProps> & {
    overrides?: ModelListPageOverridesProps | undefined | null;
}>;
export default function ModelListPage(props: ModelListPageProps): React.ReactElement;
