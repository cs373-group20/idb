/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { FlexProps, IconProps, ImageProps, TextProps } from "@aws-amplify/ui-react";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type ToolCardOverridesProps = {
    ToolCard?: PrimitiveOverrideProps<FlexProps>;
    Image?: PrimitiveOverrideProps<ImageProps>;
    toolName?: PrimitiveOverrideProps<TextProps>;
    "Line 1"?: PrimitiveOverrideProps<IconProps>;
    "Frame 3"?: PrimitiveOverrideProps<FlexProps>;
    toolBio?: PrimitiveOverrideProps<TextProps>;
} & EscapeHatchProps;
export declare type ToolCardProps = React.PropsWithChildren<Partial<FlexProps> & {
    toolBio?: String;
    toolName?: String;
    toolImg?: String;
    toolURL?: String;
} & {
    overrides?: ToolCardOverridesProps | undefined | null;
}>;
export default function ToolCard(props: ToolCardProps): React.ReactElement;
