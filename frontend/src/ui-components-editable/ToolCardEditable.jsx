/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import {
  getOverrideProps,
  useNavigateAction,
} from "@aws-amplify/ui-react/internal";
import { Flex, Icon, Image, Text, Button } from "@aws-amplify/ui-react";
export default function ToolCard(props) {
  const { toolBio, toolName, toolImg, toolURL, overrides, ...rest } = props;
  const toolNameOnClick = useNavigateAction({ type: "url", url: toolURL, target: "_blank" });
  return (
    <Flex
      gap="7px"
      direction="column"
      width="264px"
      height="446px"
      justifyContent="flex-start"
      alignItems="center"
      position="relative"
      borderRadius="10px"
      padding="4px 4px 20px 4px"
      backgroundColor="rgba(174,179,183,1)"
      {...getOverrideProps(overrides, "ToolCard")}
      {...rest}
    >
      <Image
        width="256px"
        height="256px"
        display="block"
        gap="unset"
        alignItems="unset"
        justifyContent="unset"
        shrink="0"
        position="relative"
        boxShadow="0px 4px 4px rgba(0, 0, 0, 0.25)"
        borderRadius="10px"
        padding="10px 10px 10px 10px"
        objectFit="contain"
        backgroundColor="rgba(255,255,255,1)"
        src={toolImg}
        {...getOverrideProps(overrides, "Image")}
      ></Image>
      <Button
        fontFamily="Inter"
        fontSize="24px"
        fontWeight="600"
        border="none"
        color="rgba(0,0,0,1)"
        lineHeight="30px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="fit-content"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        position="relative"
        padding="0px 10px 0px 10px"
        whiteSpace="pre-wrap"
        children={toolName}
        onClick={() => {
          toolNameOnClick();
        }}
        {...getOverrideProps(overrides, "toolName")}
      ></Button>
      <Icon
        width="208px"
        height="0px"
        viewBox={{ minX: 0, minY: 0, width: 208, height: 1 }}
        paths={[
          {
            d: "M0 0L208 0L208 -2L0 -2L0 0Z",
            stroke: "rgba(174,179,183,1)",
            fillRule: "nonzero",
            strokeWidth: 2,
          },
        ]}
        display="block"
        gap="unset"
        alignItems="unset"
        justifyContent="unset"
        shrink="0"
        position="relative"
        {...getOverrideProps(overrides, "Line 1")}
      ></Icon>
      <Flex
        gap="10px"
        direction="row"
        width="251px"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        shrink="0"
        position="relative"
        padding="0px 10px 0px 10px"
        {...getOverrideProps(overrides, "Frame 3")}
      >
        <Text
          fontFamily="Inter"
          fontSize="14px"
          fontWeight="400"
          color="rgba(0,0,0,1)"
          lineHeight="24px"
          textAlign="center"
          display="block"
          direction="column"
          justifyContent="unset"
          width="unset"
          height="unset"
          gap="unset"
          alignItems="unset"
          grow="1"
          shrink="1"
          basis="0"
          position="relative"
          padding="0px 0px 0px 0px"
          whiteSpace="pre-wrap"
          children={toolBio}
          {...getOverrideProps(overrides, "toolBio")}
        ></Text>
      </Flex>
    </Flex>
  );
}
