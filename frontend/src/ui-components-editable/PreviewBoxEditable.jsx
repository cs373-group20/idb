/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import {
  getOverrideProps,
  useNavigateAction,
} from "@aws-amplify/ui-react/internal";
import { Button, Flex, Image, Text, View } from "@aws-amplify/ui-react";
export default function PreviewBox(props) {
  if (props.hidden) return null;
  const { overrides, ...rest } = props;
  const buttonOnClick = useNavigateAction({ type: "url", url: "" });
  return (
    <View
      width="248px"
      height="400px"
      display="block"
      gap="unset"
      alignItems="unset"
      justifyContent="unset"
      overflow="hidden"
      position="relative"
      padding="0px 0px 0px 0px"
      backgroundColor="rgba(174,179,183,1)"
      {...getOverrideProps(overrides, "PreviewBox")}
      {...rest}
    >
      <Flex
        gap="5px"
        direction="column"
        width="248px"
        height="179px"
        justifyContent="flex-start"
        alignItems="center"
        position="absolute"
        top="145px"
        left="0px"
        padding="10px 10px 10px 10px"
        {...getOverrideProps(overrides, "TextArea")}
      >
        <Text
          fontFamily="Inter"
          fontSize="24px"
          fontWeight="600"
          color="rgba(0,0,0,1)"
          lineHeight="30px"
          textAlign="center"
          display="block"
          direction="column"
          justifyContent="unset"
          width="208px"
          height="unset"
          gap="unset"
          alignItems="unset"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          whiteSpace="pre-wrap"
          children={props.boxname}
          {...getOverrideProps(overrides, "Name")}
        ></Text>
        <Text
          fontFamily="Inter"
          fontSize="14px"
          fontWeight="400"
          color="rgba(0,0,0,1)"
          lineHeight="21px"
          textAlign="left"
          display="block"
          direction="column"
          justifyContent="unset"
          width="225px"
          height="133px"
          gap="unset"
          alignItems="unset"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          whiteSpace="pre-wrap"
          children={props.boxcontent}
          {...getOverrideProps(overrides, "Description")}
        ></Text>
      </Flex>
      <Image
        width="248px"
        height="145px"
        display="block"
        gap="unset"
        alignItems="unset"
        justifyContent="unset"
        position="absolute"
        top="0px"
        left="0px"
        padding="0px 0px 0px 0px"
        objectFit="cover"
        src={props.imageurl}
        {...getOverrideProps(overrides, "Image")}
      ></Image>
      <Button
        width="250px"
        height="400px"
        position="absolute"
        padding="0px 0px 0px 0px"
        top="-1px"
        left="-1px"
        backgroundColor="rgba(255,255,255,0)"
        size="default"
        isDisabled={false}
        variation="default"
        onClick={() => {
          buttonOnClick();
        }}
        {...getOverrideProps(overrides, "Button")}
      ></Button>
    </View>
  );
}
