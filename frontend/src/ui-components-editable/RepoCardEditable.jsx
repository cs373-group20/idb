/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import {
  getOverrideProps,
  useNavigateAction,
} from "@aws-amplify/ui-react/internal";
import { Flex, Icon, Text, Button } from "@aws-amplify/ui-react";
export default function RepoCard(props) {
  const {
    numCommits,
    numIssues,
    numTests,
    repoLinkURL,
    repoDocuURL,
    overrides,
    ...rest
  } = props;
  const repoLinkOnClick = useNavigateAction({
    target: "_blank",
    type: "url",
    url: repoLinkURL,
  });
  const repoDocuOnClick = useNavigateAction({
    target: "_blank",
    type: "url",
    url: repoDocuURL,
  });
  return (
    <Flex
      gap="7px"
      direction="column"
      width="1033px"
      height="488px"
      justifyContent="flex-start"
      alignItems="center"
      position="relative"
      borderRadius="10px"
      padding="4px 4px 20px 4px"
      backgroundColor="rgba(174,179,183,1)"
      {...getOverrideProps(overrides, "RepoCard")}
      {...rest}
    >
      <Text
        fontFamily="Inter"
        fontSize="48px"
        fontWeight="700"
        color="rgba(0,0,0,1)"
        lineHeight="60px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="unset"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        alignSelf="stretch"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="Repo Information"
        {...getOverrideProps(overrides, "RepoInfo")}
      ></Text>
      <Icon
        width="208px"
        height="0px"
        viewBox={{ minX: 0, minY: 0, width: 208, height: 1 }}
        paths={[
          {
            d: "M0 0L208 0L208 -2L0 -2L0 0Z",
            stroke: "rgba(174,179,183,1)",
            fillRule: "nonzero",
            strokeWidth: 2,
          },
        ]}
        display="block"
        gap="unset"
        alignItems="unset"
        justifyContent="unset"
        shrink="0"
        position="relative"
        {...getOverrideProps(overrides, "Line 14267678")}
      ></Icon>
      <Flex
        gap="7px"
        direction="column"
        width="unset"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        shrink="0"
        alignSelf="stretch"
        position="relative"
        padding="0px 0px 0px 0px"
        {...getOverrideProps(overrides, "Frame 2")}
      >
        <Button
          gap="10px"
          direction="row"
          width="unset"
          height="36px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="32px"
          padding="8px 12px 8px 12px"
          backgroundColor="rgba(239,240,240,1)"
          onClick={() => {
            repoLinkOnClick();
          }}
          {...getOverrideProps(overrides, "RepoLink")}
        >
          <Text
            fontFamily="Inter"
            fontSize="16px"
            fontWeight="800"
            color="rgba(4,125,149,1)"
            lineHeight="20px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="GitLab Repo"
            {...getOverrideProps(overrides, "repoLink")}
          ></Text>
          <Flex
            padding="0px 0px 0px 0px"
            width="13.67px"
            height="11.96px"
            display="block"
            gap="unset"
            alignItems="unset"
            justifyContent="unset"
            shrink="0"
            position="relative"
            {...getOverrideProps(overrides, "icon42943001")}
          >
            <Icon
              width="13.67px"
              height="11.96px"
              viewBox={{
                minX: 0,
                minY: 0,
                width: 13.670654296875,
                height: 11.9617919921875,
              }}
              paths={[
                {
                  d: "M8.54414 0L8.54414 3.41765C1.70883 3.41765 0 6.92075 0 11.9618C0.88859 8.57831 3.41765 6.83531 6.83531 6.83531L8.54414 6.83531L8.54414 10.253L13.6706 4.85307L8.54414 0Z",
                  fill: "rgba(4,125,149,1)",
                  fillRule: "nonzero",
                },
              ]}
              display="block"
              gap="unset"
              alignItems="unset"
              justifyContent="unset"
              position="absolute"
              top="0%"
              bottom="0%"
              left="0%"
              right="0%"
              {...getOverrideProps(overrides, "Vector42943002")}
            ></Icon>
          </Flex>
        </Button>
        <Button
          gap="10px"
          direction="row"
          width="unset"
          height="36px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="32px"
          padding="8px 12px 8px 12px"
          backgroundColor="rgba(239,240,240,1)"
          onClick={() => {
            repoDocuOnClick();
          }}
          {...getOverrideProps(overrides, "RepoDocu")}
        >
          <Text
            fontFamily="Inter"
            fontSize="16px"
            fontWeight="800"
            color="rgba(4,125,149,1)"
            lineHeight="20px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Documentation"
            {...getOverrideProps(overrides, "repoDocu")}
          ></Text>
          <Flex
            padding="0px 0px 0px 0px"
            width="13.67px"
            height="11.96px"
            display="block"
            gap="unset"
            alignItems="unset"
            justifyContent="unset"
            shrink="0"
            position="relative"
            {...getOverrideProps(overrides, "icon42943024")}
          >
            <Icon
              width="13.67px"
              height="11.96px"
              viewBox={{
                minX: 0,
                minY: 0,
                width: 13.670654296875,
                height: 11.9617919921875,
              }}
              paths={[
                {
                  d: "M8.54414 0L8.54414 3.41765C1.70883 3.41765 0 6.92075 0 11.9618C0.88859 8.57831 3.41765 6.83531 6.83531 6.83531L8.54414 6.83531L8.54414 10.253L13.6706 4.85307L8.54414 0Z",
                  fill: "rgba(4,125,149,1)",
                  fillRule: "nonzero",
                },
              ]}
              display="block"
              gap="unset"
              alignItems="unset"
              justifyContent="unset"
              position="absolute"
              top="0%"
              bottom="0%"
              left="0%"
              right="0%"
              {...getOverrideProps(overrides, "Vector42943025")}
            ></Icon>
          </Flex>
        </Button>
        <Icon
          width="208px"
          height="0px"
          viewBox={{ minX: 0, minY: 0, width: 208, height: 1 }}
          paths={[
            {
              d: "M0 0L208 0L208 -2L0 -2L0 0Z",
              stroke: "rgba(174,179,183,1)",
              fillRule: "nonzero",
              strokeWidth: 2,
            },
          ]}
          display="block"
          gap="unset"
          alignItems="unset"
          justifyContent="unset"
          shrink="0"
          position="relative"
          {...getOverrideProps(overrides, "Line 14268870")}
        ></Icon>
        <Text
          fontFamily="Inter"
          fontSize="40px"
          fontWeight="700"
          color="rgba(0,0,0,1)"
          lineHeight="50px"
          textAlign="center"
          display="block"
          direction="column"
          justifyContent="unset"
          width="246px"
          height="unset"
          gap="unset"
          alignItems="unset"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          whiteSpace="pre-wrap"
          children="Statistics"
          {...getOverrideProps(overrides, "RepoStats")}
        ></Text>
        <Flex
          gap="10px"
          direction="row"
          width="447px"
          height="72px"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="32px"
          padding="8px 30px 8px 30px"
          backgroundColor="rgba(67,168,84,1)"
          {...getOverrideProps(overrides, "Commits")}
        >
          <Text
            fontFamily="Inter"
            fontSize="24px"
            fontWeight="600"
            color="rgba(13,26,38,1)"
            lineHeight="30px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            grow="1"
            shrink="1"
            basis="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Total Commits"
            {...getOverrideProps(overrides, "commits")}
          ></Text>
          <Text
            fontFamily="Inter"
            fontSize="24px"
            fontWeight="600"
            color="rgba(13,26,38,1)"
            lineHeight="30px"
            textAlign="right"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children={numCommits}
            {...getOverrideProps(overrides, "numCommits")}
          ></Text>
        </Flex>
        <Flex
          gap="10px"
          direction="row"
          width="447px"
          height="71px"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="32px"
          padding="8px 30px 8px 30px"
          backgroundColor="rgba(191,64,64,1)"
          {...getOverrideProps(overrides, "Issues")}
        >
          <Text
            fontFamily="Inter"
            fontSize="24px"
            fontWeight="600"
            color="rgba(13,26,38,1)"
            lineHeight="30px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            grow="1"
            shrink="1"
            basis="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Total Issues"
            {...getOverrideProps(overrides, "issues")}
          ></Text>
          <Text
            fontFamily="Inter"
            fontSize="24px"
            fontWeight="600"
            color="rgba(13,26,38,1)"
            lineHeight="30px"
            textAlign="right"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children={numIssues}
            {...getOverrideProps(overrides, "numIssues")}
          ></Text>
        </Flex>
        <Flex
          gap="10px"
          direction="row"
          width="447px"
          height="72px"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="32px"
          padding="8px 30px 8px 30px"
          backgroundColor="rgba(125,214,232,1)"
          {...getOverrideProps(overrides, "Unit Tests")}
        >
          <Text
            fontFamily="Inter"
            fontSize="24px"
            fontWeight="600"
            color="rgba(13,26,38,1)"
            lineHeight="30px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            grow="1"
            shrink="1"
            basis="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Total Unit Tests"
            {...getOverrideProps(overrides, "unittests")}
          ></Text>
          <Text
            fontFamily="Inter"
            fontSize="24px"
            fontWeight="600"
            color="rgba(13,26,38,1)"
            lineHeight="30px"
            textAlign="right"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children={numTests}
            {...getOverrideProps(overrides, "numTests")}
          ></Text>
        </Flex>
      </Flex>
    </Flex>
  );
}
