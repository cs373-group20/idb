/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { FlexProps, IconProps, TextProps } from "@aws-amplify/ui-react";
import { DevCardProps } from "./DevCard";
import { RepoCardProps } from "./RepoCard";
import { ToolCardProps } from "./ToolCard";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type AboutPageOverridesProps = {
    AboutPage?: PrimitiveOverrideProps<FlexProps>;
    About?: PrimitiveOverrideProps<TextProps>;
    Description?: PrimitiveOverrideProps<TextProps>;
    ourTeam?: PrimitiveOverrideProps<TextProps>;
    "Frame 4"?: PrimitiveOverrideProps<FlexProps>;
    DevCard1?: DevCardProps;
    DevCard2?: DevCardProps;
    DevCard3?: DevCardProps;
    DevCard4?: DevCardProps;
    DevCard5?: DevCardProps;
    Repo?: PrimitiveOverrideProps<TextProps>;
    RepoCard?: RepoCardProps;
    toolsUsed?: PrimitiveOverrideProps<TextProps>;
    ToolsLine1?: PrimitiveOverrideProps<FlexProps>;
    ToolCard1?: ToolCardProps;
    ToolCard2?: ToolCardProps;
    ToolCard3?: ToolCardProps;
    ToolCard4?: ToolCardProps;
    ToolsLine2?: PrimitiveOverrideProps<FlexProps>;
    ToolCard5?: ToolCardProps;
    ToolCard6?: ToolCardProps;
    ToolCard7?: ToolCardProps;
    dataSourcesUsed43051693?: PrimitiveOverrideProps<TextProps>;
    dataSourcesUsed4323714?: PrimitiveOverrideProps<TextProps>;
    wildfires?: PrimitiveOverrideProps<FlexProps>;
    name4322670?: PrimitiveOverrideProps<TextProps>;
    "Line 14322671"?: PrimitiveOverrideProps<IconProps>;
    Link_14322672?: PrimitiveOverrideProps<FlexProps>;
    link14322673?: PrimitiveOverrideProps<TextProps>;
    icon4322674?: PrimitiveOverrideProps<FlexProps>;
    Vector4322675?: PrimitiveOverrideProps<IconProps>;
    counties?: PrimitiveOverrideProps<FlexProps>;
    name4322685?: PrimitiveOverrideProps<TextProps>;
    "Line 14322686"?: PrimitiveOverrideProps<IconProps>;
    Link_14322687?: PrimitiveOverrideProps<FlexProps>;
    link14322688?: PrimitiveOverrideProps<TextProps>;
    icon4322689?: PrimitiveOverrideProps<FlexProps>;
    Vector4322690?: PrimitiveOverrideProps<IconProps>;
    Link_2?: PrimitiveOverrideProps<FlexProps>;
    link2?: PrimitiveOverrideProps<TextProps>;
    icon4322693?: PrimitiveOverrideProps<FlexProps>;
    Vector4322694?: PrimitiveOverrideProps<IconProps>;
    Link_3?: PrimitiveOverrideProps<FlexProps>;
    link3?: PrimitiveOverrideProps<TextProps>;
    icon4322697?: PrimitiveOverrideProps<FlexProps>;
    Vector4322698?: PrimitiveOverrideProps<IconProps>;
    agencies?: PrimitiveOverrideProps<FlexProps>;
    name4322700?: PrimitiveOverrideProps<TextProps>;
    "Line 14322701"?: PrimitiveOverrideProps<IconProps>;
    Link_14322702?: PrimitiveOverrideProps<FlexProps>;
    link14322703?: PrimitiveOverrideProps<TextProps>;
    icon4322704?: PrimitiveOverrideProps<FlexProps>;
    Vector4322705?: PrimitiveOverrideProps<IconProps>;
} & EscapeHatchProps;
export declare type AboutPageProps = React.PropsWithChildren<Partial<FlexProps> & {
    About?: String;
    Description?: String;
    wildfires1?: String;
    counties1?: String;
    counties2?: String;
    counties3?: String;
    facilities1?: String;
} & {
    overrides?: AboutPageOverridesProps | undefined | null;
}>;
export default function AboutPage(props: AboutPageProps): React.ReactElement;
