/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { FlexProps, ImageProps, TextProps, ViewProps } from "@aws-amplify/ui-react";
import { PreviewBoxProps } from "./PreviewBoxEditable";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type ModelPageOverridesProps = {
    ModelPage?: PrimitiveOverrideProps<ViewProps>;
    ContentFrame?: PrimitiveOverrideProps<FlexProps>;
    Name?: PrimitiveOverrideProps<TextProps>;
    Image?: PrimitiveOverrideProps<ImageProps>;
    Description?: PrimitiveOverrideProps<TextProps>;
    Relation1?: PrimitiveOverrideProps<TextProps>;
    Relation1Preview?: PrimitiveOverrideProps<FlexProps>;
    Relation1PreviewBox1?: PreviewBoxProps;
    Relation1PreviewBox2?: PreviewBoxProps;
    Relation1PreviewBox3?: PreviewBoxProps;
    Relation2?: PrimitiveOverrideProps<TextProps>;
    Relation2Preview?: PrimitiveOverrideProps<FlexProps>;
    Relation2PreviewBox1?: PreviewBoxProps;
    Relation2PreviewBox2?: PreviewBoxProps;
    Relation2PreviewBox3?: PreviewBoxProps;
} & EscapeHatchProps;
export declare type ModelPageProps = React.PropsWithChildren<Partial<ViewProps> & {
    overrides?: ModelPageOverridesProps | undefined | null;
}>;
export default function ModelPage(props: ModelPageProps): React.ReactElement;
