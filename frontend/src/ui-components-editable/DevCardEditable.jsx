/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import { getOverrideProps } from "@aws-amplify/ui-react/internal";
import { Flex, Icon, Image, Text } from "@aws-amplify/ui-react";
export default function DevCard(props) {
  const {
    name,
    image,
    numCommits = 0,
    numIssues = 0,
    numTests = 0,
    bio,
    role,
    overrides,
    ...rest
  } = props;
  return (
    <Flex
      gap="7px"
      direction="column"
      width="264px"
      height="758px"
      justifyContent="flex-start"
      alignItems="center"
      position="relative"
      borderRadius="10px"
      padding="4px 4px 20px 4px"
      backgroundColor="rgba(174,179,183,1)"
      {...getOverrideProps(overrides, "DevCard")}
      {...rest}
    >
      <Image
        width="256px"
        height="358px"
        display="block"
        gap="unset"
        alignItems="unset"
        justifyContent="unset"
        shrink="0"
        position="relative"
        boxShadow="0px 4px 4px rgba(0, 0, 0, 0.25)"
        borderRadius="10px"
        padding="0px 0px 0px 0px"
        objectFit="cover"
        src={image}
        {...getOverrideProps(overrides, "Image")}
      ></Image>
      <Text
        fontFamily="Inter"
        fontSize="24px"
        fontWeight="600"
        color="rgba(0,0,0,1)"
        lineHeight="30px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="246px"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children={name}
        {...getOverrideProps(overrides, "Name")}
      ></Text>
      <Icon
        width="208px"
        height="0px"
        viewBox={{ minX: 0, minY: 0, width: 208, height: 1 }}
        paths={[
          {
            d: "M0 0L208 0L208 -2L0 -2L0 0Z",
            stroke: "rgba(174,179,183,1)",
            fillRule: "nonzero",
            strokeWidth: 2,
          },
        ]}
        display="block"
        gap="unset"
        alignItems="unset"
        justifyContent="unset"
        shrink="0"
        position="relative"
        {...getOverrideProps(overrides, "Line 1")}
      ></Icon>
      <Flex
        gap="10px"
        direction="row"
        width="251px"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        shrink="0"
        position="relative"
        padding="0px 10px 0px 10px"
        {...getOverrideProps(overrides, "Frame 3")}
      >
        <Text
          fontFamily="Inter"
          fontSize="14px"
          fontWeight="400"
          color="rgba(0,0,0,1)"
          lineHeight="24px"
          textAlign="center"
          display="block"
          direction="column"
          justifyContent="unset"
          width="unset"
          height="unset"
          gap="unset"
          alignItems="unset"
          grow="1"
          shrink="1"
          basis="0"
          position="relative"
          padding="0px 0px 0px 0px"
          whiteSpace="pre-wrap"
          children={bio}
          {...getOverrideProps(overrides, "Bio")}
        ></Text>
      </Flex>
      <Flex
        gap="7px"
        direction="column"
        width="unset"
        height="unset"
        justifyContent="flex-end"
        alignItems="center"
        grow="1"
        shrink="1"
        basis="0"
        position="relative"
        padding="0px 0px 0px 0px"
        {...getOverrideProps(overrides, "Frame 2")}
      >
        <Flex
          gap="10px"
          direction="row"
          width="208px"
          height="36px"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="32px"
          padding="8px 12px 8px 12px"
          backgroundColor="rgba(239,240,240,1)"
          {...getOverrideProps(overrides, "Role")}
        >
          <Text
            fontFamily="Inter"
            fontSize="16px"
            fontWeight="800"
            color="rgba(13,26,38,1)"
            lineHeight="20px"
            textAlign="center"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            grow="1"
            shrink="1"
            basis="0"
            alignSelf="stretch"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children={role}
            {...getOverrideProps(overrides, "label")}
          ></Text>
        </Flex>
        <Flex
          gap="10px"
          direction="row"
          width="203px"
          height="32px"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="32px"
          padding="8px 12px 8px 12px"
          backgroundColor="rgba(67,168,84,1)"
          {...getOverrideProps(overrides, "Commits")}
        >
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="600"
            color="rgba(13,26,38,1)"
            lineHeight="14px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            grow="1"
            shrink="1"
            basis="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Commits"
            {...getOverrideProps(overrides, "commits")}
          ></Text>
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="600"
            color="rgba(13,26,38,1)"
            lineHeight="14px"
            textAlign="right"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children={numCommits}
            {...getOverrideProps(overrides, "numCommits")}
          ></Text>
        </Flex>
        <Flex
          gap="10px"
          direction="row"
          width="203px"
          height="32px"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="32px"
          padding="8px 12px 8px 12px"
          backgroundColor="rgba(191,64,64,1)"
          {...getOverrideProps(overrides, "Issues")}
        >
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="600"
            color="rgba(13,26,38,1)"
            lineHeight="14px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            grow="1"
            shrink="1"
            basis="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Issues"
            {...getOverrideProps(overrides, "issues")}
          ></Text>
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="600"
            color="rgba(13,26,38,1)"
            lineHeight="14px"
            textAlign="right"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children={numIssues}
            {...getOverrideProps(overrides, "numIssues")}
          ></Text>
        </Flex>
        <Flex
          gap="10px"
          direction="row"
          width="203px"
          height="32px"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="32px"
          padding="8px 12px 8px 12px"
          backgroundColor="rgba(125,214,232,1)"
          {...getOverrideProps(overrides, "Unit Tests")}
        >
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="600"
            color="rgba(13,26,38,1)"
            lineHeight="14px"
            textAlign="left"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            grow="1"
            shrink="1"
            basis="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Unit Tests"
            {...getOverrideProps(overrides, "unittests")}
          ></Text>
          <Text
            fontFamily="Inter"
            fontSize="14px"
            fontWeight="600"
            color="rgba(13,26,38,1)"
            lineHeight="14px"
            textAlign="right"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children={numTests}
            {...getOverrideProps(overrides, "numTests")}
          ></Text>
        </Flex>
      </Flex>
    </Flex>
  );
}
