/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import {
  getOverrideProps,
  useNavigateAction,
} from "@aws-amplify/ui-react/internal";
import { Flex, Icon, Text, Button } from "@aws-amplify/ui-react";
import DevCard from "./DevCardEditable";
import RepoCard from "./RepoCardEditable";
import ToolCard from "./ToolCardEditable";
export default function AboutPage(props) {
  const {
    About = "About",
    Description,
    wildfires1,
    counties1,
    counties2,
    counties3,
    facilities1,
    overrides,
    ...rest
  } = props;
  const linkWildfires1Click = useNavigateAction({
    target: "_blank",
    type: "url",
    url: wildfires1,
  });
  const linkCounties1Click =
    useNavigateAction({ target: "_blank", type: "url", url: counties1 });
  const linkCounties2Click = useNavigateAction({
    target: "_blank",
    type: "url",
    url: counties2,
  });
  const linkCounties3Click = useNavigateAction({
    target: "_blank",
    type: "url",
    url: counties3,
  });
  const linkFPA1Click = useNavigateAction(
    { target: "_blank", type: "url", url: facilities1 }
  );
  return (
    <Flex
      gap="40px"
      direction="column"
      width="100%"
      height="unset"
      justifyContent="flex-start"
      alignItems="center"
      overflow="hidden"
      position="relative"
      padding="100px 10px 200px 10px"
      backgroundColor="rgba(255,255,255,1)"
      {...getOverrideProps(overrides, "AboutPage")}
      {...rest}
    >
      <Text
        fontFamily="Inter"
        fontSize="48px"
        fontWeight="300"
        color="rgba(0,0,0,1)"
        lineHeight="60px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="unset"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        alignSelf="stretch"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="About"
        {...getOverrideProps(overrides, "About")}
      ></Text>
      <Text
        fontFamily="Inter"
        fontSize="20px"
        fontWeight="400"
        color="rgba(0,0,0,1)"
        lineHeight="30px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="unset"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        alignSelf="stretch"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="About description here."
        {...getOverrideProps(overrides, "Description")}
      ></Text>
      <Text
        fontFamily="Inter"
        fontSize="40px"
        fontWeight="400"
        color="rgba(0,0,0,1)"
        lineHeight="50px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="unset"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        alignSelf="stretch"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="Our Team!"
        {...getOverrideProps(overrides, "ourTeam")}
      ></Text>
      <Flex
        gap="20px"
        direction="row"
        width="unset"
        height="unset"
        wrap="wrap"
        justifyContent="center"
        alignItems="flex-start"
        shrink="0"
        alignSelf="stretch"
        position="relative"
        padding="0px 0px 0px 0px"
        {...getOverrideProps(overrides, "Frame 4")}
      >
        <DevCard
          display="flex"
          gap="7px"
          direction="column"
          width="264px"
          height="727px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="10px"
          padding="4px 4px 20px 4px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "DevCard1")}
        ></DevCard>
        <DevCard
          display="flex"
          gap="7px"
          direction="column"
          width="264px"
          height="727px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="10px"
          padding="4px 4px 20px 4px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "DevCard2")}
        ></DevCard>
        <DevCard
          display="flex"
          gap="7px"
          direction="column"
          width="264px"
          height="727px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="10px"
          padding="4px 4px 20px 4px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "DevCard3")}
        ></DevCard>
        <DevCard
          display="flex"
          gap="7px"
          direction="column"
          width="264px"
          height="727px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="10px"
          padding="4px 4px 20px 4px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "DevCard4")}
        ></DevCard>
        <DevCard
          display="flex"
          gap="7px"
          direction="column"
          width="264px"
          height="727px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="10px"
          padding="4px 4px 20px 4px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "DevCard5")}
        ></DevCard>
      </Flex>
      <Text
        fontFamily="Inter"
        fontSize="40px"
        fontWeight="400"
        color="rgba(0,0,0,1)"
        lineHeight="50px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="1400px"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="Our Repository!"
        {...getOverrideProps(overrides, "Repo")}
      ></Text>
      <RepoCard
        display="flex"
        gap="7px"
        direction="column"
        width="unset"
        height="488px"
        justifyContent="flex-start"
        alignItems="center"
        shrink="0"
        position="relative"
        borderRadius="10px"
        padding="4px 200px 20px 200px"
        backgroundColor="rgba(174,179,183,1)"
        {...getOverrideProps(overrides, "RepoCard")}
      ></RepoCard>
      <Text
        fontFamily="Inter"
        fontSize="40px"
        fontWeight="400"
        color="rgba(0,0,0,1)"
        lineHeight="50px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="unset"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        alignSelf="stretch"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="Tools Used!"
        {...getOverrideProps(overrides, "toolsUsed")}
      ></Text>
      <Flex
        gap="40px"
        direction="row"
        width="unset"
        height="unset"
        wrap="wrap"
        justifyContent="center"
        alignItems="flex-start"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        {...getOverrideProps(overrides, "ToolsLine1")}
      >
        <ToolCard
          display="flex"
          gap="7px"
          direction="column"
          width="264px"
          height="446px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="10px"
          padding="4px 4px 20px 4px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "ToolCard1")}
        ></ToolCard>
        <ToolCard
          display="flex"
          gap="7px"
          direction="column"
          width="264px"
          height="446px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="10px"
          padding="4px 4px 20px 4px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "ToolCard2")}
        ></ToolCard>
        <ToolCard
          display="flex"
          gap="7px"
          direction="column"
          width="264px"
          height="446px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="10px"
          padding="4px 4px 20px 4px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "ToolCard3")}
        ></ToolCard>
        <ToolCard
          display="flex"
          gap="7px"
          direction="column"
          width="264px"
          height="446px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="10px"
          padding="4px 4px 20px 4px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "ToolCard4")}
        ></ToolCard>
        <ToolCard
          display="flex"
          gap="7px"
          direction="column"
          width="264px"
          height="446px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="10px"
          padding="4px 4px 20px 4px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "ToolCard5")}
        ></ToolCard>
        <ToolCard
          display="flex"
          gap="7px"
          direction="column"
          width="264px"
          height="446px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="10px"
          padding="4px 4px 20px 4px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "ToolCard6")}
        ></ToolCard>
        <ToolCard
          display="flex"
          gap="7px"
          direction="column"
          width="264px"
          height="446px"
          justifyContent="flex-start"
          alignItems="center"
          shrink="0"
          position="relative"
          borderRadius="10px"
          padding="4px 4px 20px 4px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "ToolCard7")}
        ></ToolCard>
      </Flex>
      <Text
        fontFamily="Inter"
        fontSize="40px"
        fontWeight="400"
        color="rgba(0,0,0,1)"
        lineHeight="50px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="unset"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        alignSelf="stretch"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="Data Sources Used!"
        {...getOverrideProps(overrides, "dataSourcesUsed43051693")}
      ></Text>
      <Text
        fontFamily="Inter"
        fontSize="20px"
        fontWeight="400"
        color="rgba(0,0,0,1)"
        lineHeight="30px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="unset"
        height="unset"
        gap="unset"
        alignItems="unset"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="All the data sources were scraped via the cURL command in Linux and redirected into a separate JSON file."
        {...getOverrideProps(overrides, "dataSourcesUsed4323714")}
      ></Text>
      <Flex
        gap="10px"
        direction="column"
        width="unset"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        {...getOverrideProps(overrides, "wildfires")}
      >
        <Text
          fontFamily="Inter"
          fontSize="32px"
          fontWeight="500"
          color="rgba(0,0,0,1)"
          lineHeight="40px"
          textAlign="center"
          display="block"
          direction="column"
          justifyContent="unset"
          width="unset"
          height="unset"
          gap="unset"
          alignItems="unset"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          whiteSpace="pre-wrap"
          children="Wildfires"
          {...getOverrideProps(overrides, "name4322670")}
        ></Text>
        <Icon
          width="463px"
          height="1px"
          viewBox={{ minX: 0, minY: 0, width: 463, height: 1 }}
          paths={[
            {
              d: "M0 0L463 0L463 -2L0 -2L0 0Z",
              stroke: "rgba(174,179,183,1)",
              fillRule: "nonzero",
              strokeWidth: 1,
            },
          ]}
          display="block"
          gap="unset"
          alignItems="unset"
          justifyContent="unset"
          shrink="0"
          position="relative"
          {...getOverrideProps(overrides, "Line 14322671")}
        ></Icon>
        <Button
          gap="10px"
          direction="row"
          width="unset"
          height="unset"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          padding="0px 2px 0px 2px"
          onClick={() => {
            linkWildfires1Click();
          }}
          {...getOverrideProps(overrides, "Link_14322672")}
        >
          <Text
            fontFamily="Inter"
            fontSize="20px"
            fontWeight="700"
            color="rgba(4,125,149,1)"
            lineHeight="25px"
            textAlign="center"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="ArcGIS"
            {...getOverrideProps(overrides, "link14322673")}
          ></Text>
          <Flex
            padding="0px 0px 0px 0px"
            width="13.67px"
            height="11.96px"
            display="block"
            gap="unset"
            alignItems="unset"
            justifyContent="unset"
            shrink="0"
            position="relative"
            {...getOverrideProps(overrides, "icon4322674")}
          >
            <Icon
              width="13.67px"
              height="11.96px"
              viewBox={{
                minX: 0,
                minY: 0,
                width: 13.670623779296875,
                height: 11.9619140625,
              }}
              paths={[
                {
                  d: "M8.54414 0L8.54414 3.41765C1.70883 3.41765 0 6.92075 0 11.9618C0.88859 8.57831 3.41765 6.83531 6.83531 6.83531L8.54414 6.83531L8.54414 10.253L13.6706 4.85307L8.54414 0Z",
                  fill: "rgba(4,125,149,1)",
                  fillRule: "nonzero",
                },
              ]}
              display="block"
              gap="unset"
              alignItems="unset"
              justifyContent="unset"
              position="absolute"
              top="0%"
              bottom="0%"
              left="0%"
              right="0%"
              {...getOverrideProps(overrides, "Vector4322675")}
            ></Icon>
          </Flex>
        </Button>
      </Flex>
      <Flex
        gap="10px"
        direction="column"
        width="unset"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        {...getOverrideProps(overrides, "counties")}
      >
        <Text
          fontFamily="Inter"
          fontSize="32px"
          fontWeight="500"
          color="rgba(0,0,0,1)"
          lineHeight="40px"
          textAlign="center"
          display="block"
          direction="column"
          justifyContent="unset"
          width="unset"
          height="unset"
          gap="unset"
          alignItems="unset"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          whiteSpace="pre-wrap"
          children="Counties"
          {...getOverrideProps(overrides, "name4322685")}
        ></Text>
        <Icon
          width="463px"
          height="1px"
          viewBox={{ minX: 0, minY: 0, width: 463, height: 1 }}
          paths={[
            {
              d: "M0 0L463 0L463 -2L0 -2L0 0Z",
              stroke: "rgba(174,179,183,1)",
              fillRule: "nonzero",
              strokeWidth: 1,
            },
          ]}
          display="block"
          gap="unset"
          alignItems="unset"
          justifyContent="unset"
          shrink="0"
          position="relative"
          {...getOverrideProps(overrides, "Line 14322686")}
        ></Icon>
        <Button
          gap="10px"
          direction="row"
          width="unset"
          height="unset"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          padding="0px 2px 0px 2px"
          onClick={() => {
            linkCounties1Click();
          }}
          {...getOverrideProps(overrides, "Link_14322687")}
        >
          <Text
            fontFamily="Inter"
            fontSize="20px"
            fontWeight="700"
            color="rgba(4,125,149,1)"
            lineHeight="25px"
            textAlign="center"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="opendatasoft"
            {...getOverrideProps(overrides, "link14322688")}
          ></Text>
          <Flex
            padding="0px 0px 0px 0px"
            width="13.67px"
            height="11.96px"
            display="block"
            gap="unset"
            alignItems="unset"
            justifyContent="unset"
            shrink="0"
            position="relative"
            {...getOverrideProps(overrides, "icon4322689")}
          >
            <Icon
              width="13.67px"
              height="11.96px"
              viewBox={{
                minX: 0,
                minY: 0,
                width: 13.670623779296875,
                height: 11.9619140625,
              }}
              paths={[
                {
                  d: "M8.54414 0L8.54414 3.41765C1.70883 3.41765 0 6.92075 0 11.9618C0.88859 8.57831 3.41765 6.83531 6.83531 6.83531L8.54414 6.83531L8.54414 10.253L13.6706 4.85307L8.54414 0Z",
                  fill: "rgba(4,125,149,1)",
                  fillRule: "nonzero",
                },
              ]}
              display="block"
              gap="unset"
              alignItems="unset"
              justifyContent="unset"
              position="absolute"
              top="0%"
              bottom="0%"
              left="0%"
              right="0%"
              {...getOverrideProps(overrides, "Vector4322690")}
            ></Icon>
          </Flex>
        </Button>
        <Button
          gap="10px"
          direction="row"
          width="unset"
          height="unset"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          padding="0px 2px 0px 2px"
          onClick={() => {
            linkCounties2Click();
          }}
          {...getOverrideProps(overrides, "Link_2")}
        >
          <Text
            fontFamily="Inter"
            fontSize="20px"
            fontWeight="700"
            color="rgba(4,125,149,1)"
            lineHeight="25px"
            textAlign="center"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="ArcGIS"
            {...getOverrideProps(overrides, "link2")}
          ></Text>
          <Flex
            padding="0px 0px 0px 0px"
            width="13.67px"
            height="11.96px"
            display="block"
            gap="unset"
            alignItems="unset"
            justifyContent="unset"
            shrink="0"
            position="relative"
            {...getOverrideProps(overrides, "icon4322693")}
          >
            <Icon
              width="13.67px"
              height="11.96px"
              viewBox={{
                minX: 0,
                minY: 0,
                width: 13.670623779296875,
                height: 11.9619140625,
              }}
              paths={[
                {
                  d: "M8.54414 0L8.54414 3.41765C1.70883 3.41765 0 6.92075 0 11.9618C0.88859 8.57831 3.41765 6.83531 6.83531 6.83531L8.54414 6.83531L8.54414 10.253L13.6706 4.85307L8.54414 0Z",
                  fill: "rgba(4,125,149,1)",
                  fillRule: "nonzero",
                },
              ]}
              display="block"
              gap="unset"
              alignItems="unset"
              justifyContent="unset"
              position="absolute"
              top="0%"
              bottom="0%"
              left="0%"
              right="0%"
              {...getOverrideProps(overrides, "Vector4322694")}
            ></Icon>
          </Flex>
        </Button>
        <Button
          gap="10px"
          direction="row"
          width="unset"
          height="unset"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          padding="0px 2px 0px 2px"
          onClick={() => {
            linkCounties3Click();
          }}
          {...getOverrideProps(overrides, "Link_3")}
        >
          <Text
            fontFamily="Inter"
            fontSize="20px"
            fontWeight="700"
            color="rgba(4,125,149,1)"
            lineHeight="25px"
            textAlign="center"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Census Bureau"
            {...getOverrideProps(overrides, "link3")}
          ></Text>
          <Flex
            padding="0px 0px 0px 0px"
            width="13.67px"
            height="11.96px"
            display="block"
            gap="unset"
            alignItems="unset"
            justifyContent="unset"
            shrink="0"
            position="relative"
            {...getOverrideProps(overrides, "icon4322697")}
          >
            <Icon
              width="13.67px"
              height="11.96px"
              viewBox={{
                minX: 0,
                minY: 0,
                width: 13.670623779296875,
                height: 11.9619140625,
              }}
              paths={[
                {
                  d: "M8.54414 0L8.54414 3.41765C1.70883 3.41765 0 6.92075 0 11.9618C0.88859 8.57831 3.41765 6.83531 6.83531 6.83531L8.54414 6.83531L8.54414 10.253L13.6706 4.85307L8.54414 0Z",
                  fill: "rgba(4,125,149,1)",
                  fillRule: "nonzero",
                },
              ]}
              display="block"
              gap="unset"
              alignItems="unset"
              justifyContent="unset"
              position="absolute"
              top="0%"
              bottom="0%"
              left="0%"
              right="0%"
              {...getOverrideProps(overrides, "Vector4322698")}
            ></Icon>
          </Flex>
        </Button>
      </Flex>
      <Flex
        gap="10px"
        direction="column"
        width="unset"
        height="unset"
        justifyContent="flex-start"
        alignItems="center"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        {...getOverrideProps(overrides, "agencies")}
      >
        <Text
          fontFamily="Inter"
          fontSize="32px"
          fontWeight="500"
          color="rgba(0,0,0,1)"
          lineHeight="40px"
          textAlign="center"
          display="block"
          direction="column"
          justifyContent="unset"
          width="unset"
          height="unset"
          gap="unset"
          alignItems="unset"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          whiteSpace="pre-wrap"
          children="Fire Protection Agencies"
          {...getOverrideProps(overrides, "name4322700")}
        ></Text>
        <Icon
          width="463px"
          height="1px"
          viewBox={{ minX: 0, minY: 0, width: 463, height: 1 }}
          paths={[
            {
              d: "M0 0L463 0L463 -2L0 -2L0 0Z",
              stroke: "rgba(174,179,183,1)",
              fillRule: "nonzero",
              strokeWidth: 1,
            },
          ]}
          display="block"
          gap="unset"
          alignItems="unset"
          justifyContent="unset"
          shrink="0"
          position="relative"
          {...getOverrideProps(overrides, "Line 14322701")}
        ></Icon>
        <Button
          gap="10px"
          direction="row"
          width="unset"
          height="unset"
          justifyContent="center"
          alignItems="center"
          shrink="0"
          position="relative"
          padding="0px 2px 0px 2px"
          onClick={() => {
            linkFPA1Click();
          }}
          {...getOverrideProps(overrides, "Link_14322702")}
        >
          <Text
            fontFamily="Inter"
            fontSize="20px"
            fontWeight="700"
            color="rgba(4,125,149,1)"
            lineHeight="25px"
            textAlign="center"
            display="block"
            direction="column"
            justifyContent="unset"
            width="unset"
            height="unset"
            gap="unset"
            alignItems="unset"
            shrink="0"
            position="relative"
            padding="0px 0px 0px 0px"
            whiteSpace="pre-wrap"
            children="Department of Forestry and Fire Protection"
            {...getOverrideProps(overrides, "link14322703")}
          ></Text>
          <Flex
            padding="0px 0px 0px 0px"
            width="13.67px"
            height="11.96px"
            display="block"
            gap="unset"
            alignItems="unset"
            justifyContent="unset"
            shrink="0"
            position="relative"
            {...getOverrideProps(overrides, "icon4322704")}
          >
            <Icon
              width="13.67px"
              height="11.96px"
              viewBox={{
                minX: 0,
                minY: 0,
                width: 13.670623779296875,
                height: 11.9619140625,
              }}
              paths={[
                {
                  d: "M8.54414 0L8.54414 3.41765C1.70883 3.41765 0 6.92075 0 11.9618C0.88859 8.57831 3.41765 6.83531 6.83531 6.83531L8.54414 6.83531L8.54414 10.253L13.6706 4.85307L8.54414 0Z",
                  fill: "rgba(4,125,149,1)",
                  fillRule: "nonzero",
                },
              ]}
              display="block"
              gap="unset"
              alignItems="unset"
              justifyContent="unset"
              position="absolute"
              top="0%"
              bottom="0%"
              left="0%"
              right="0%"
              {...getOverrideProps(overrides, "Vector4322705")}
            ></Icon>
          </Flex>
        </Button>
      </Flex>
    </Flex>
  );
}
