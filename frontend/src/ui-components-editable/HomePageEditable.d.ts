/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { FlexProps, TextProps, ViewProps } from "@aws-amplify/ui-react";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type HomePageOverridesProps = {
    HomePage?: PrimitiveOverrideProps<FlexProps>;
    "Frame 6"?: PrimitiveOverrideProps<FlexProps>;
    Name40632397?: PrimitiveOverrideProps<TextProps>;
    Bio4325715?: PrimitiveOverrideProps<TextProps>;
    "Frame 7"?: PrimitiveOverrideProps<FlexProps>;
    Name43991946?: PrimitiveOverrideProps<TextProps>;
    Bio43991947?: PrimitiveOverrideProps<TextProps>;
    "Frame 8"?: PrimitiveOverrideProps<FlexProps>;
    Name43991950?: PrimitiveOverrideProps<TextProps>;
    Bio43991951?: PrimitiveOverrideProps<TextProps>;
    "Frame 9"?: PrimitiveOverrideProps<FlexProps>;
    Name43991959?: PrimitiveOverrideProps<TextProps>;
    WildfirePreviewBox?: PrimitiveOverrideProps<ViewProps>;
} & EscapeHatchProps;
export declare type HomePageProps = React.PropsWithChildren<Partial<FlexProps> & {
    name?: String;
    bio?: String;
    img?: String;
    wildfirePreviewBox?: React.ReactNode;
} & {
    overrides?: HomePageOverridesProps | undefined | null;
}>;
export default function HomePage(props: HomePageProps): React.ReactElement;
