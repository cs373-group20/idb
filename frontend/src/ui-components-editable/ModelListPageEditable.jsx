/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import { getOverrideProps } from "@aws-amplify/ui-react/internal";
import { Flex, Text, View } from "@aws-amplify/ui-react";
import PreviewBox from "./PreviewBoxEditable";
export default function ModelListPage(props) {
  const { overrides, ...rest } = props;
  return (
    <Flex
      width="100%"
      height="100vh"
      display="block"
      gap="unset"
      alignItems="unset"
      justifyContent="unset"
      overflow="hidden"
      position="relative"
      padding="0px 0px 0px 0px"
      backgroundColor="rgba(255,255,255,1)"
      {...getOverrideProps(overrides, "ModelListPage")}
      {...rest}
    >
      <Text
        fontFamily="Inter"
        fontSize="48px"
        fontWeight="300"
        color="rgba(0,0,0,1)"
        lineHeight="60px"
        textAlign="center"
        display="block"
        direction="column"
        justifyContent="unset"
        width="856px"
        height="85px"
        gap="unset"
        alignItems="unset"
        position="absolute"
        top="75px"
        left="calc(50% - 428px - -2px)"
        padding="0px 0px 0px 0px"
        whiteSpace="pre-wrap"
        children="ModelName"
        {...getOverrideProps(overrides, "ModelName")}
      ></Text>
      <Flex
        gap="40px"
        direction="row"
        width="1300px"
        height="710px"
        justifyContent="center"
        alignItems="flex-start"
        position="absolute"
        top="160px"
        left="calc(50% - 650px - 0px)"
        padding="20px 20px 20px 20px"
        {...getOverrideProps(overrides, "PreviewGrid")}
      >
        <PreviewBox
          width="248px"
          height="400px"
          display="block"
          gap="unset"
          alignItems="unset"
          justifyContent="unset"
          overflow="hidden"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          backgroundColor="rgba(174,179,183,1)"
          boxname="defaultname"
          boxtext="defaulttext"
          {...getOverrideProps(overrides, "PreviewBox1")}
        ></PreviewBox>
        <PreviewBox
          width="248px"
          height="400px"
          display="block"
          gap="unset"
          alignItems="unset"
          justifyContent="unset"
          overflow="hidden"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "PreviewBox2")}
        ></PreviewBox>
        <PreviewBox
          width="248px"
          height="400px"
          display="block"
          gap="unset"
          alignItems="unset"
          justifyContent="unset"
          overflow="hidden"
          shrink="0"
          position="relative"
          padding="0px 0px 0px 0px"
          backgroundColor="rgba(174,179,183,1)"
          {...getOverrideProps(overrides, "PreviewBox3")}
        ></PreviewBox>
      </Flex>
    </Flex>
  );
}
