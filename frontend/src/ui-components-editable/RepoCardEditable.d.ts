/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

import * as React from "react";
import { EscapeHatchProps } from "@aws-amplify/ui-react/internal";
import { FlexProps, IconProps, TextProps } from "@aws-amplify/ui-react";
export declare type PrimitiveOverrideProps<T> = Partial<T> & React.DOMAttributes<HTMLDivElement>;
export declare type RepoCardOverridesProps = {
    RepoCard?: PrimitiveOverrideProps<FlexProps>;
    RepoInfo?: PrimitiveOverrideProps<TextProps>;
    "Line 14267678"?: PrimitiveOverrideProps<IconProps>;
    "Frame 2"?: PrimitiveOverrideProps<FlexProps>;
    RepoLink?: PrimitiveOverrideProps<FlexProps>;
    repoLink?: PrimitiveOverrideProps<TextProps>;
    icon42943001?: PrimitiveOverrideProps<FlexProps>;
    Vector42943002?: PrimitiveOverrideProps<IconProps>;
    RepoDocu?: PrimitiveOverrideProps<FlexProps>;
    repoDocu?: PrimitiveOverrideProps<TextProps>;
    icon42943024?: PrimitiveOverrideProps<FlexProps>;
    Vector42943025?: PrimitiveOverrideProps<IconProps>;
    "Line 14268870"?: PrimitiveOverrideProps<IconProps>;
    RepoStats?: PrimitiveOverrideProps<TextProps>;
    Commits?: PrimitiveOverrideProps<FlexProps>;
    commits?: PrimitiveOverrideProps<TextProps>;
    numCommits?: PrimitiveOverrideProps<TextProps>;
    Issues?: PrimitiveOverrideProps<FlexProps>;
    issues?: PrimitiveOverrideProps<TextProps>;
    numIssues?: PrimitiveOverrideProps<TextProps>;
    "Unit Tests"?: PrimitiveOverrideProps<FlexProps>;
    unittests?: PrimitiveOverrideProps<TextProps>;
    numTests?: PrimitiveOverrideProps<TextProps>;
} & EscapeHatchProps;
export declare type RepoCardProps = React.PropsWithChildren<Partial<FlexProps> & {
    numCommits?: Number;
    numIssues?: Number;
    numTests?: Number;
    repoLinkURL?: String;
    repoDocuURL?: String;
} & {
    overrides?: RepoCardOverridesProps | undefined | null;
}>;
export default function RepoCard(props: RepoCardProps): React.ReactElement;
