/***************************************************************************
 * The contents of this file were generated with Amplify Studio.           *
 * Please refrain from making any modifications to this file.              *
 * Any changes to this file will be overwritten when running amplify pull. *
 **************************************************************************/

/* eslint-disable */
import * as React from "react";
import {
  getOverrideProps
} from "@aws-amplify/ui-react/internal";
import { Button, Flex, SearchField, Image } from "@aws-amplify/ui-react";
import { 
  useNavigate 
} from "react-router-dom";

export default function NavBar(props) {
  const { modelC, isMobile, overrides, ...rest } = props;
  const navigate = useNavigate();
  return (
    <Flex
      gap="20px"
      direction={isMobile? "column" : "row"}
      wrap="wrap"
      width="unset"
      height="unset"
      justifyContent="center"
      alignItems="center"
      position="relative"
      padding="24px 32px 24px 32px"
      backgroundColor="rgba(255,255,255,1)"
      {...getOverrideProps(overrides, "NavBar")}
      {...rest}
    >
      <Button
        border="none"
        backgroundColor="unset"
        gap="2px"
        direction="row"
        width="unset"
        height="unset"
        justifyContent="center"
        alignItems="center"
        marginLeft="10px"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        onClick={()=>navigate()}
        {...getOverrideProps(overrides, "Logo29767073")}
      >
      <Image
      src="/logo-no-bg.svg"
      alt="California Wildfires Logo"
      maxHeight="50px"
      >
      </Image>
      </Button>
      <Flex
        gap="20px"
        direction={isMobile? "column" : "row"}
        width="unset"
        wrap="wrap"
        height="unset"
        marginLeft="10px"
        justifyContent="flex-start"
        alignItems="center"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        {...getOverrideProps(overrides, "NavBarFrame")}
      >
        <Button
          width="unset"
          height="unset"
          shrink="0"
          size="default"
          isDisabled={false}
          variation="link"
          children="Home"
          //onClick={()=>navigate('/home')}
          {...getOverrideProps(overrides, "Home")}
        ></Button>
        <Button
          width="unset"
          height="unset"
          shrink="0"
          size="default"
          isDisabled={false}
          variation="link"
          children="About"
          onClick=""
          {...getOverrideProps(overrides, "About")}
        ></Button>
        <Button
          width="unset"
          height="unset"
          shrink="0"
          size="default"
          isDisabled={false}
          variation="link"
          children="Wildfires"
          onClick={()=>navigate('/wildfires')}
          {...getOverrideProps(overrides, "Wildfires")}
        ></Button>
        <Button
          width="unset"
          height="unset"
          shrink="0"Fire Protection Agencies
          size="default"
          isDisabled={false}
          variation="link"
          children="Counties"
          onClick={()=>navigate('/counties')}
          {...getOverrideProps(overrides, "Counties")}
        ></Button>
        <Button
          width="unset"
          height="unset"
          shrink="0"
          size="default"
          isDisabled={false}
          variation="link"
          children="Fire Protection Agencies"
          onClick={()=>navigate('/facilities')}
          {...getOverrideProps(overrides, "Fire Protection Agencies")}
        ></Button>
        <Button
          width="unset"
          height="unset"
          shrink="0"
          size="default"
          isDisabled={false}
          variation="link"
          children="Visualizations"
          onClick={()=>navigate('/visualizations')}
          {...getOverrideProps(overrides, "Visualizations")}
        ></Button>
      </Flex>
      <Flex
        marginLeft="auto"
        direction="row"
        width="usnet"
        height="unset"
        justifyContent="flex-end"
        alignItems="center"
        shrink="0"
        position="relative"
        padding="0px 0px 0px 0px"
        {...getOverrideProps(overrides, "SearchFieldFrame")}
      >
        <SearchField
          width="unset"
          height="unset"
          placeholder="Search"
          shrink="0"
          size="default"
          isDisabled={false}
          labelHidden={true}
          variation="default"
          {...getOverrideProps(overrides, "SearchField")}
        ></SearchField>
      </Flex>
    </Flex>
  );
}
