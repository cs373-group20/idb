export type AmplifyDependentResourcesAttributes = {
  "api": {
    "idb": {
      "GraphQLAPIEndpointOutput": "string",
      "GraphQLAPIIdOutput": "string",
      "GraphQLAPIKeyOutput": "string"
    }
  }
}